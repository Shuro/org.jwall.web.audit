
AuditServer - Usage
===================

The  AuditServer  allows for remote  clients to  register and --  after  proper
authentication -- receive audit events from the server. The connection is based
on an SSL connection and the events will be sent to the clients using a  simple
text oriented protocol.

Given that the org.jwall.web.audit-x.y.jar  file remains within your class path,
the server can can be started by calling the main-class:

	java org.jwall.AuditServer [OPTIONS] /path/to/audit-file


The server expects the  audit-file from which it reads the events that can later 
be sent to clients to be the last argument. Additionally there are a few options,
which may be  specified to customize authentication, etc.  The following options
are currently supported:

 --help, -h, -?      
     
       Will print this usage information.
  
 
 --version, -v
 
       This will simply print out version information.
       
  
 --server-port PORT   
 
       Can be used to set the tcp-port at which the server shall accept connections
       to PORT. PORT should be referring to an unused port, default is 10001. 
                  
                      
 --server-users FILE  
       
       This will specify the file from which the server shall read user names and
       password. The format of this file is expected to contain one line for each
       user in the format:                     
                              username=password
                      	                    
       Lines starting with an "#" character are treated as comments.
                                