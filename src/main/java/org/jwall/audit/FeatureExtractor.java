package org.jwall.audit;

import java.util.Set;

public interface FeatureExtractor<E extends Event, F> {

	/**
	 * This method extracts all available feature names for the given event.
	 * 
	 * @param event
	 * @return
	 */
	public Set<String> getVariables( E event );
	
	
	/**
	 * Extract all values for the specified feature from the given event.
	 * 
	 * @param feature
	 * @param event
	 * @return
	 */
	public F extract( String feature, E event );
}