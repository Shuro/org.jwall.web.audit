package org.jwall.audit;

/**
 * <p>
 * Exceptions of this class are thrown by event processors.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class EventProcessorException extends Exception {

	/** The unique class ID */
	private static final long serialVersionUID = 5378420747364156867L;

	public EventProcessorException(){
		super();
	}
	
	public EventProcessorException( String msg ){
		super( msg );
	}
}