package org.jwall.audit.server;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.AuditEventListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SyslogReceiver extends Thread {

	static Logger log = LoggerFactory.getLogger( SyslogReceiver.class );
    ServerSocket socket;
    AuditEventListener storage = null;
	List<AuditEventStreamHandler> handlers = new ArrayList<AuditEventStreamHandler>();
	
	public SyslogReceiver( InetAddress addr, Integer port, Class<?> handlerClass ) throws Exception {
		socket = new ServerSocket( port, 100, addr );
		log.info( "Listening for connections on " + addr.getHostAddress() + ":" + port );
	}
	
	
	public void run(){
		
		while( true ){
			
			try {
				Socket connection = socket.accept();
				log.info( "incoming connection from {}:{}", connection.getInetAddress().getHostAddress(), connection.getPort() );
				
				AuditEventStreamHandler handler = createHandler( connection );
				handler.start();
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	
	private AuditEventStreamHandler createHandler( Socket connection ) throws Exception {
		
		AuditEventStreamHandler handler = new AuditEventStreamHandler( this, connection, false );
		if( storage != null )
			handler.setEventStore( storage );
		
		synchronized( handlers ){
			handlers.add( handler );
		}
		
		return handler;
	}

	
	protected void handlerFinished( AuditEventStreamHandler handler ){
		synchronized( handlers ){
			handlers.add( handler );
		}
	}
	
	
	public static void main( String[] args ) throws Exception {
		System.setProperty( "storage.engine", "database" );
		SyslogReceiver r = new SyslogReceiver( InetAddress.getByName( "localhost" ), 10000, null );
		r.run();
	}
}