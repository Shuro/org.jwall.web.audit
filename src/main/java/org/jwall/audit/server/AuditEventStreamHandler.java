package org.jwall.audit.server;

import java.io.EOFException;
import java.io.File;
import java.io.InputStream;
import java.net.Socket;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.zip.GZIPInputStream;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.io.AuditEventWriter;
import org.jwall.web.audit.io.ModSecurity2AuditWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventStreamHandler extends Thread implements AuditEventListener {

	static Logger log = LoggerFactory.getLogger( AuditEventStreamHandler.class );
	Long start;
	Integer count = 0;
	Socket socket;
	AuditEventWriter writer;
	final static DecimalFormat fmt = new DecimalFormat( "0.00" );
	AuditEventListener store = null;
	SyslogReceiver parent;
	boolean gzip = false;
	
	public AuditEventStreamHandler( SyslogReceiver parent, Socket socket, boolean gzip ) throws Exception {
		this.parent = parent;
		this.socket = socket;
		this.gzip = gzip;
		//writer = new ConcurrentAuditWriter( new File( "/tmp/" + socket.getInetAddress().getHostAddress() ) );
		File outFile = new File( File.separator + "tmp" + File.separator + socket.getInetAddress().getHostAddress() + "-" + socket.getPort() + "-audit.log" );
		System.out.println( "Writing to " + outFile.getAbsolutePath() );
		writer = new ModSecurity2AuditWriter( outFile );
	}

	public void setEventStore( AuditEventListener store ){
		this.store = store;
	}
	

	/**
	 * @see java.lang.Thread#run()
	 */
	public void run(){

		try {

			//BufferedReader r = new BufferedReader( new InputStreamReader( socket.getInputStream() ) );
			//PrintStream out = new PrintStream( new FileOutputStream( "/tmp/" + socket.getInetAddress().getHostAddress() + "-audit.log" ) );
			//String line = r.readLine();
			start = System.currentTimeMillis();
			InputStream in = null;
			if( gzip )
				in = new GZIPInputStream( socket.getInputStream() );
			else
				in = socket.getInputStream();
			
			ModSecurity2AuditStream reader = new ModSecurity2AuditStream( in, this );
			AuditEvent evt = reader.readNext();
			while( evt != null && !socket.isClosed() ){
				//out.println( line );
				//line = r.readLine();
				eventArrived( evt );
				evt = reader.readNext();
			}
			System.out.println( "Connection closed." );

		} catch (EOFException eof){
			System.out.println( "Connection closed." );
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}


	@Override
	public void eventArrived(AuditEvent evt) {
		try {
			
			if( store != null ){
				store.eventArrived( evt );
			}
			
			count++;
			
			if( count % 1000 == 0 ){
				Long time = (System.currentTimeMillis() - start) ;
				log.info( count +" events received in " + ((int)(time/1000)) + " seconds (" + fmt.format( ((1000*count.doubleValue() ) / time.doubleValue())  ) + " events/second)" );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public void eventsArrived(Collection<AuditEvent> events) {
		for( AuditEvent e : events )
			eventArrived( e );
	}
}
