package org.jwall.audit.server;

import java.io.InputStream;
import java.util.Properties;

import org.jwall.audit.Event;
import org.jwall.audit.EventListener;

public interface LogHandler<E extends Event> extends Runnable {

	public void init( InputStream in, EventListener<E> listener, Properties p ) throws Exception;
	
	public E readNext() throws Exception;
}
