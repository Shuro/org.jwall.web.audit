package org.jwall.audit.script;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jwall.audit.EventType;
import org.jwall.audit.EventView;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.filter.FilterException;
import org.jwall.web.audit.filter.FilterExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * This class defines a bridge to access a user's view from within a script such
 * as a ruby script. The script event view is similar to an AuditEventView but
 * not as powerful.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ScriptEventView<E extends org.jwall.audit.Event> {
	static Logger log = LoggerFactory.getLogger(ScriptEventView.class);
	String user;
	EventView<E> view;

	public ScriptEventView(String user, EventView<E> parentView) {
		log.debug("Creationg ScriptEventView for user {}", user);
		this.user = user;
		this.view = parentView;
	}

	public List<ScriptEvent> list() {
		return list("");
	}

	public List<ScriptEvent> list(String filter) {
		try {
			return new EventList<E>(view, FilterCompiler.parse(filter));
		} catch (FilterException e) {
			e.printStackTrace();
		}

		return new ArrayList<ScriptEvent>();
	}

	public List<ScriptEvent> list(String filter, int offset, int max)
			throws Exception {
		try {
			List<E> list = view.list(FilterCompiler.parse(filter), offset, max);

			List<ScriptEvent> results = new ArrayList<ScriptEvent>();
			for (E e : list)
				results.add(new ScriptEvent(e));

			return results;
		} catch (Exception e) {
			log.error("Failed to execute list() operation: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
		return new ArrayList<ScriptEvent>();
	}

	public void tag(ScriptEvent event, String name) throws Exception {
		if (view == null) {
			log.error("No AuditEventView associated with this ScriptEventView!");
			return;
		}

		if (event != null && event.getType() == EventType.AUDIT) {
			String id = event.get(ModSecurity.TX_ID);
			log.info("tag( {}, '{}' )", id, name);
			FilterExpression f = FilterCompiler.parse("TX_ID @eq '" + id + "'");
			view.tag(f, name);
		}
	}

	public void untag(ScriptEvent event, String name) throws Exception {
		if (view == null) {
			log.error("No AuditEventView associated with this ScriptEventView!");
			return;
		}

		if (event != null && event.getType() == EventType.AUDIT) {
			log.info("tag( {}, '{}' )", event.get(ModSecurity.TX_ID), name);
			FilterExpression f = FilterCompiler.parse("TX_ID @eq '" + name
					+ "'");
			view.untag(f, name);
		}
	}

	public void delete(String txId) {
		if (txId == null)
			return;

		try {
			view.delete(txId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Long count(String filterString) throws Exception {
		log.info("Counting events by filter '{}'", filterString);
		try {
			if (view != null) {
				FilterExpression filter = FilterCompiler.parse(filterString);
				Long count = view.count(filter);
				log.debug("  count is {}", count);
				return count;
			} else {
				log.debug("View is 'null'!");
			}
		} catch (Exception e) {
			log.error("Failed to count events: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}

		return -1L;
	}

	public ScriptEvent get(String txId) throws Exception {
		E e = view.get(txId);
		if (e != null)
			return new ScriptEvent(e);
		return null;
	}

	public Map<String, Long> count(String variable, String filter) {

		try {
			view.count(variable, FilterCompiler.parse(filter));
		} catch (Exception e) {
			return null;
		}

		return new HashMap<String, Long>();
	}
}