package org.jwall.audit.script;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.jwall.audit.EventView;
import org.jwall.audit.script.utils.ListIteratorImpl;
import org.jwall.web.audit.filter.FilterExpression;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class implements a read-only list that is backed up
 * by an event view and a specific filter-expression. It will
 * lazily provide a batch-wise iteration over the list items
 * instead of loading a large set from the database all at once.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E>
 */
public class EventList<E extends org.jwall.audit.Event> 
	extends AbstractList<ScriptEvent> {
	
	static Logger log = LoggerFactory.getLogger( EventList.class );
	EventView<E> view;
	FilterExpression filter;
	int size = 0;
	
	// TODO: Add a cache to the list implementation such that subsequent
	//       get(int) requests are being served from the cache	
	//Map<Integer,ScriptEvent> cache = new LinkedHashMap<Integer,ScriptEvent>(); 

	
	
	public EventList( EventView<E> view, FilterExpression filter ){
		log.debug( "Creating new lazy-list based on view {} and filter {}", view, filter );
		this.view = view;
		this.filter = filter;
		try {
			this.size = view.count( filter ).intValue();
			log.debug( "list has {} elements", size );
		} catch (Exception e) {
			log.error( "failed to check list size: {}", e.getMessage() );
			if( log.isDebugEnabled() )
				e.printStackTrace();
			this.size = 0;
		}
	}

	@Override
	public void clear() {
		log.debug( "clearing list..." );
		size = 0;
	}
	

	@Override
	public ScriptEvent get(int idx) {
		log.debug( "lazy-list.get( {} )   list size is: {}", idx, size );
		ScriptEvent evt = null;
		if( idx >= 0 && idx < size ){
			try {
				Long curSize = view.count( filter );
				log.info( "curSize: {},  size: {}", curSize, size );
				List<E> list = view.list( filter, idx, 1 );
				log.debug( "view.list( {}, {}, 1 )", filter, idx );
				//log.debug( "   => {}", list );
				if( list != null && ! list.isEmpty() )
					evt = new ScriptEvent( list.get( 0 ) );
			} catch (Exception e) {
				log.error( "failed to retrieve event list[{}]: {}", idx, e.getMessage() );
				if( log.isDebugEnabled() )
					e.printStackTrace();
			}
		} else
			log.debug( "index outputWriter of bounds!" );
		log.debug( "returning event({}) = {}", idx, evt );
		return evt;
	}

	@Override
	public boolean isEmpty() {
		return size <= 0;
	}

	@Override
	public Iterator<ScriptEvent> iterator() {
		log.debug( "returning new iterator()" );
		return new ListIteratorImpl( this );
	}

	@Override
	public int lastIndexOf(Object arg0) {
		return -1;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public ListIterator<ScriptEvent> listIterator() {
		log.debug( "returning new listIterator()" );
		return new ListIteratorImpl( this );
	}

	@Override
	public ListIterator<ScriptEvent> listIterator(int arg0) {
		log.debug( "returning new listIterator( {} )", arg0 );
		return new ListIteratorImpl( this, arg0 );
	}
}