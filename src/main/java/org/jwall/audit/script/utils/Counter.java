package org.jwall.audit.script.utils;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <p>
 * This is a very simple counter class that is provided to all scripts
 * and can be used to track the count of event properties when iterating
 * over multiple events.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class Counter {
	
	Map<String,Double> counts = new LinkedHashMap<String,Double>();
	
	
	public void reset(){
		counts.clear();
	}
	
	public void count( String value ){
		Double cnt = counts.get( value );
		if( cnt == null ){
			cnt = 1.0d;
		} else {
			cnt = cnt + 1.0d;
		}
		counts.put( value, cnt );
	}
	
	public Double get( String value ){
		Double cnt = counts.get( value );
		if( cnt == null )
			return 0.0d;
		return cnt;
	}
	
	public void set( String value, Double count ){
		counts.put( value, count );
	}
	
	public Double getTotal(){
		Double total = 0.0d;
		for( Double d : counts.values() )
			total += d;
		
		return total;
	}
	
	public Double sum( Collection<String> keys ){
		Double sum = 0.0d;
		for( String key : keys )
			sum += get( key );
		
		return sum;
	}
}