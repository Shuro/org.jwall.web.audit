package org.jwall.audit.script.utils;

import java.util.Iterator;
import java.util.ListIterator;

import org.jwall.audit.script.EventList;
import org.jwall.audit.script.ScriptEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This is a simple implementation of an iterator and a list iterator. It is
 * backed by a lazy event list.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ListIteratorImpl implements Iterator<ScriptEvent>, ListIterator<ScriptEvent> {

	static Logger log = LoggerFactory.getLogger( ListIteratorImpl.class );
	EventList<?> list;
	int cur = 0;
	int max = 0;
	
	
	public ListIteratorImpl( EventList<?> list ){
		this( list, 0 );
	}
	
	
	public ListIteratorImpl( EventList<?> list, int offset ){
		this.list = list;
		max = this.list.size();
	}
	
	
	@Override
	public void add(ScriptEvent arg0) {
	}

	
	@Override
	public boolean hasNext() {
		return cur < max;
	}

	@Override
	public boolean hasPrevious() {
		return cur > 0;
	}

	@Override
	public ScriptEvent next() {
		log.debug( "returning next element, cur: {}, max: {}", cur, max );
		if( cur < max )
			return list.get( cur++ );
		return null;
	}

	@Override
	public int nextIndex() {
		return cur;
	}

	@Override
	public ScriptEvent previous() {
		return list.get( --cur );
	}

	@Override
	public int previousIndex() {
		return cur - 1;
	}

	@Override
	public void remove() {
	}

	@Override
	public void set(ScriptEvent arg0) {
	}
}