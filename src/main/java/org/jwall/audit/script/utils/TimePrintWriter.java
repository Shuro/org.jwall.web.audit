package org.jwall.audit.script.utils;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

public class TimePrintWriter extends PrintWriter {

	public TimePrintWriter(OutputStream out) {
		super(out);
	}

	
	public TimePrintWriter(Writer writer) {
		super(writer);
	}


	@Override
	public void println(Object x) {
		printTime();
		super.println(x);
	}


	@Override
	public void println(String x) {
		printTime();
		super.println(x);
	}
	
	protected void printTime(){
		super.print( "@" );
		super.print( System.currentTimeMillis() );
		super.print( ": " );
	}
}