package org.jwall.audit.script.utils;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class TimePrintStream extends PrintStream {

	// flag for start-of-line
	boolean sol = true;
	
	public TimePrintStream(OutputStream out) {
		super(out);
	}

	public TimePrintStream( File file ) throws IOException {
		super( file );
	}
	
	

	@Override
	public void print(Object obj) {
		if( sol )
			printTime();
		super.print(obj);
	}

	@Override
	public void print(String s) {
		if( sol )
			printTime();
		super.print(s);
		sol = false;
	}

	@Override
	public void println() {
		super.println();
		sol = true;
	}

	@Override
	public void println(Object x) {
		printTime();
		super.println(x);
	}


	@Override
	public void println(String x) {
		printTime();
		super.println(x);
	}
	
	protected void printTime(){
		super.print( "@" );
		super.print( System.currentTimeMillis() );
		super.print( ": " );
	}

	@Override
	public void write(int b) {

		if( sol ){
			sol = false;
			printTime();
		}
		
		super.write(b);
		sol = b == '\n';
	}
}
