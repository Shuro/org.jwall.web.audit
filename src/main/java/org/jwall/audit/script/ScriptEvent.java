package org.jwall.audit.script;

import java.util.ArrayList;
import java.util.List;

import org.jwall.audit.EventType;
import org.jwall.log.LogMessage;
import org.jwall.web.audit.ModSecurity;

public final class ScriptEvent implements org.jwall.audit.Event {

	/** The unique class ID */
	private static final long serialVersionUID = 371392341967963638L;

	org.jwall.audit.Event e;

	public ScriptEvent(org.jwall.audit.Event evt) {
		this.e = evt;
	}

	public boolean isSet(String var) {
		return e.get(var) != null;
	}

	public String get(String var) {
		return e.get(var);
	}

	public List<String> getAll(String var) {

		if (e instanceof org.jwall.web.audit.AuditEvent) {
			return ((org.jwall.web.audit.AuditEvent) e).getAll(var);
		}

		List<String> vals = new ArrayList<String>(1);
		vals.add(e.get(var) + "");
		return vals;
	}

	@Override
	public Long getTimestamp() {
		return e.getTimestamp();
	}

	@Override
	public EventType getType() {
		return e.getType();
	}

	@Override
	public void set(String variable, String value) {
		e.set(variable, value);
	}

	public String toString() {
		if (e instanceof org.jwall.web.audit.AuditEvent)
			return "ScriptEvent[" + e.get(ModSecurity.TX_ID) + "]";

		if (e instanceof org.jwall.log.LogMessage)
			return "LogMessage[" + e.get(LogMessage.MESSAGE) + "]";

		return "ScriptEvent[@" + e.getTimestamp() + "]";
	}
}