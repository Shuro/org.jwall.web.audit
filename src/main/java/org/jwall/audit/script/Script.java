package org.jwall.audit.script;

import java.io.Serializable;
import java.net.URL;
import java.util.Map;


/**
 * This interface defines the properties of a script definition. A script
 * needs to provide some containers for results, the script source code and
 * optionally some parameters.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface Script extends Serializable {

	/**
	 * This method provides the source code of the script.
	 * 
	 * @return
	 */
	public String getScriptSource();
	
	
	/**
	 * This method determines the language which is used to
	 * interpret/run the script source code.
	 * 
	 * @return
	 */
	public String getScriptLanguage();
	
	
	public void setResultUrl( URL url );
	
	/**
	 * This method returns the ResultUrl of that script. Usually this
	 * value is determined by the script executor and provided to the
	 * implementing class.
	 * 
	 * @return
	 */
	public URL getResultUrl();
	
	
	/**
	 * This method returns a Map of parameters. These can be used as "args"
	 * for the script execution and will be provided to the script using the
	 * <code>$args</code> map.
	 * 
	 * @return
	 */
	public Map<String,Serializable> getParameters();

	
	/**
	 * This returns the result of the script (if any). The script has a
	 * map called <code>$results</code> bound that can be used to add result
	 * data.
	 * 
	 * @return
	 */
	public Map<String,Serializable> getResults();
}