package org.jwall.audit.script;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.script.ScriptContext;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineFactory;
import javax.script.ScriptEngineManager;

import org.jwall.audit.EventView;
import org.jwall.log.LogMessage;
import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This task implements script execution on a cluster. The script is usually given as
 * a plain string (script source), but may also be provided as an accessible URL.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public final class ScriptRunner {
	
	
	public final static String[] RESERVED_VARIABLES = {
			"events", "logs", "result"
	};

	static Logger log = LoggerFactory.getLogger( ScriptRunner.class );
	
	
	String lang;
	String script;
	URL scriptUrl;
	Map<String,Object> results = new LinkedHashMap<String,Object>();
	
	
	/* The following attributes are initialized at run-time, shortly before
	 * executing the script. */
	
	transient Writer outputWriter;
	transient Reader scriptReader;
	transient Writer errorWriter;
	transient ScriptEngine engine;
	final Map<String,Object> bindings = new LinkedHashMap<String,Object>();
	final ScriptContext context;
	
	
	
	public ScriptRunner( String scriptLanguage ) throws Exception {

		this.lang = scriptLanguage;
		log.debug( "Creating script-engine for script-language {}", lang );
		
		long t0 = System.currentTimeMillis();
		ScriptEngineManager manager = new ScriptEngineManager();
		long t1 = System.currentTimeMillis();
		log.debug( "Start of script-engine manager required {} ms", (t1-t0) );
		
		log.debug( "Creating script-engine by name '{}'", lang );
		t0 = System.currentTimeMillis();
		
		List<ScriptEngineFactory> factories = manager.getEngineFactories();
		for( ScriptEngineFactory factory : factories ){
			log.debug( "ScriptEngineFactory" );
			log.debug( "   language name = {}", factory.getLanguageName() );
			log.debug( "   language version = {}", factory.getLanguageVersion() );
			log.debug( "   engine extensions = {}", factory.getExtensions() );
		}
		
		engine = manager.getEngineByName( lang );
		t1 = System.currentTimeMillis();
		log.debug( "start of script-engine required {}ms", (t1-t0) );
		
		context = engine.getContext();
	}
	
	/**
	 * 
	 * @param scriptUrl
	 * @param scriptLanguage
	 * @throws Exception
	 */
	public ScriptRunner( URL scriptUrl, String scriptLanguage ) throws Exception {
		this( scriptLanguage );
		this.script = null;
		this.scriptUrl = scriptUrl;
		this.lang = scriptLanguage;
	}


	/**
	 * 
	 * @param scriptSource
	 * @param scriptLanguage
	 */
	public ScriptRunner( String scriptSource, String scriptLanguage ) throws Exception {
		this( scriptLanguage );
		this.script = scriptSource;
		this.scriptUrl = null;
		this.lang = scriptLanguage;
	}
	
	
	/**
	 * @see org.jwall.data.node.RemoteTask#init(org.slf4j.log, java.io.OutputStream)
	 */
	public void init( OutputStream out, OutputStream err ) throws Exception {

		this.outputWriter = new PrintWriter( out );
		
		log.debug( "Initializing script-execution" );
		
		if( scriptUrl != null ){
			log.debug( "Reading script from script-url: {}", scriptUrl );
			scriptReader = new InputStreamReader( scriptUrl.openStream() );
		} else {
			log.debug( "Reading script from provided string-source\n{}", script );
			scriptReader = new StringReader( script );
		}
		
		if( out != null )
			errorWriter = new OutputStreamWriter( out );
	}
	
	
	public void setErrorWriter( Writer out ){
		if( out != null )
			errorWriter = out;
	}
	
	public void setOutputWriter( Writer out ){
		this.outputWriter = out;
	}

	
	public void bind( String key, Object val ) throws Exception {
		
		if( isReservedVariable( key ) )
			throw new Exception( "Variable '" + key + "' is matching a reserved key-word!" );
		
		this.bindings.put( key, val );
	}
	
	public void setAuditEventView( EventView<AuditEvent> view ){
		bindings.put( "events", new ScriptEventView<AuditEvent>( "", view ) );
		log.debug( "updated bindings: {}", bindings );
	}
	
	public void setLogMessageView( EventView<LogMessage> view ){
		bindings.put( "logs", view );
		log.debug( "updated bindings: {}", bindings );
	}
	
	
	public void setResults(Map<String, Object> results) {
		this.results = results;
	}

	/**
	 * @see org.jwall.data.node.RemoteTask#execute(org.jwall.audit.EventView)
	 */
	public void execute() throws Exception {

		if( context == null )
			throw new Exception( "ScriptRunner not initialied!" );
		
		log.debug( "Script is read from {}", scriptReader );
		if( script != null ){
			log.debug( "Script-source is:\n{}", script );
		}

		log.debug( "Setting output-writer to {}", outputWriter );
		context.setWriter( outputWriter );
		
		if( errorWriter != null ){
			log.debug( "Setting error-output to {}", errorWriter );
			context.setErrorWriter( errorWriter );
		}
		
		
		log.debug( "Bindings: {}", bindings );
		
		for( String key : bindings.keySet() ){
			log.debug("binding variable {} to {}", key, bindings.get( key ) );
			context.setAttribute( key, bindings.get( key ), ScriptContext.ENGINE_SCOPE );
		}
		
		
		log.debug( "Binding 'result' map (String -> Object)" );
		context.setAttribute( "result", results, ScriptContext.ENGINE_SCOPE );
		
		log.debug( "Evaluating script..." );
		Object result = engine.eval( scriptReader );
		
		log.debug( "Script has been executed, result object is: {}", result );
		log.debug( "  result map is: {}", results );
	}
	
	public Map<String,Object> getResults(){
		return results;
	}
	
	public final static boolean isReservedVariable( String variable ){
		for( String var : RESERVED_VARIABLES ){
			if( var.equalsIgnoreCase( variable ) )
				return true;
		}
		return false;
	}
}