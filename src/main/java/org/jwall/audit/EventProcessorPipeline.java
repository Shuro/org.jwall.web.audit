package org.jwall.audit;

import org.jwall.audit.Event;
import org.jwall.audit.EventProcessor;
import org.jwall.audit.EventProcessorException;


/**
 * This interface defines an abstract event processor pipeline. Processors
 * can subscribe to this pipeline to act upon an event.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E>
 */
public interface EventProcessorPipeline<E extends Event> {
	
	
	/**
	 * 
	 * 
	 * @param event
	 * @throws EventProcessorException
	 */
	public void process( E event ) throws EventProcessorException;
	
	
	/**
	 * Register a new event processor in this pipeline. The processor may specify a priority,
	 * signaling at which position it wants to be inserted. Processing is done from low numbers
	 * to high numbers.
	 * 
	 * Priorities smaller than 1 and larger than 10 are reserved to internal processors. Registering
	 * a non-internal processor with invalid priority will register that processor with the nearest
	 * valid priority value.
	 * 
	 * @param priority The priority with which this processor is inserted into the pipeline.
	 * @param proc The new processor to be added to the pipeline.
	 */
	public void register( Double priority , EventProcessor<E> proc );
	
	
	/**
	 * Unregister the given processor from this pipeline.
	 * 
	 * @param proc The processor to remove from the pipeline.
	 */
	public void unregister( EventProcessor<E> proc );
	
	
	public Double getPriority( EventProcessor<E> proc );
}