package org.jwall.audit;

import java.io.Serializable;


/**
 * <p>
 * This interface defines methods common to all observable events.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface Event 
	extends Serializable
{
	public final static String TIMESTAMP = "TIMESTAMP";
	public final static String TYPE = "TYPE";
	
	
	/**
	 * Returns the time of this event in milliseconds since 1970.
	 * 
	 * @return
	 */
	public Long getTimestamp();
	
	
	public EventType getType();

	
	/**
	 * This method returns the specified aspect for the given event.
	 * 
	 * @param variableName
	 * @return
	 */
	public String get( String variableName );
	
	public void set( String variable, String value );
}