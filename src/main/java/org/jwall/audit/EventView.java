package org.jwall.audit;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jwall.web.audit.filter.FilterExpression;
import org.jwall.web.audit.filter.Operator;

/**
 * This interface defines a view for a set of log events. It allows for
 * filtering and counting log events based on filter expressions.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public interface EventView<E extends Event> {

	/**
	 * This method returns the variable names which are supported for filtering
	 * by this view.
	 * 
	 * @return
	 */
	public Set<String> getVariables();

	/**
	 * This method returns the set of variables which are indexed by a view.
	 * Indexed variables require no additional parsing of events and are thus
	 * much more efficiently to be accessed.
	 * 
	 * @return
	 */
	public Set<String> getIndexedVariables();

	/**
	 * This method returns the list of values observed for the specified
	 * variable. Implementation of this method is optional, this method my
	 * simply return an empty list.
	 * 
	 * @param variable
	 * @return
	 */
	public List<String> getCompletions(String variable);

	/**
	 * Returns the number of log-messages within this view which match the given
	 * filter expression.
	 * 
	 * @param filter
	 * @return
	 */
	public Long count(FilterExpression filter) throws Exception;

	/**
	 * Returns the counts of events grouped by the specified variable.
	 * 
	 * @param variable
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public Map<String, Long> count(String variable, FilterExpression filter)
			throws Exception;

	/**
	 * Returns the list of log-messages which match the given filter expression.
	 * The offset and limit can be used to allow for a paging view.
	 * 
	 * @param filter
	 * @param offset
	 * @param limit
	 * @return
	 */
	public List<E> list(FilterExpression filter, int offset, int limit)
			throws Exception;

	/**
	 * Retrieve the event with the given ID from the view.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public E get(String id) throws Exception;

	/**
	 * Delete the event with the given ID from the view (and the underlying
	 * storage).
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 * @deprecated Use delete(FilterExpression) with a filter of TX_ID=id
	 */
	public boolean delete(String id) throws Exception;

	public boolean delete(FilterExpression filter) throws Exception;

	public void tag(FilterExpression filter, String tag) throws Exception;

	public void untag(FilterExpression filter, String tag) throws Exception;

	public Set<Operator> getSupportedOperators();
}