package org.jwall.audit;

public interface Filter<E extends Event> {

	
	public boolean matches( E event );
}
