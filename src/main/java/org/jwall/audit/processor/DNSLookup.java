package org.jwall.audit.processor;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.util.Cache;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

/**
 * <p>
 * A simple processor to enrich events by looking up the REMOTE_ADDR in the DNS
 * service and placing the resolved name in REMOTE_HOST.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class DNSLookup extends AbstractProcessor implements
		EventProcessor<AuditEvent> {

	static Logger log = LoggerFactory.getLogger(DNSLookup.class);
	transient Cache<String> cache = new Cache<String>(10000);
	transient Cache<String> reverseCache = new Cache<String>(10000);

	String key = "REMOTE_HOST";
	String target = "REMOTE_HOSTNAME";
	Integer cacheSize = 10000;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getCacheSize() {
		return cacheSize;
	}

	public void setCacheSize(Integer cacheSize) {
		this.cacheSize = cacheSize;
		this.cache = new Cache<String>(Math.max(100, cacheSize));
		this.reverseCache = new Cache<String>(Math.max(100, cacheSize));
		log.debug("Created DNS cache of size {}", cacheSize);
	}

	public String reverseLookup(String addr) {

		String host = null;

		if (reverseCache != null && cacheSize > 0
				&& reverseCache.containsKey(addr)) {
			String hostname = reverseCache.get(addr);
			log.debug("Found cached host-name '{}' for addr {}", hostname, addr);
			return hostname;
		}

		if (addr != null && host == null) {
			try {
				InetAddress inet = InetAddress.getByName(addr);
				host = inet.getCanonicalHostName();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return host;
	}

	public String lookup(String host) {
		String addr = null;

		if (cache != null && cacheSize > 0 && cache.containsKey(host)) {
			addr = cache.get(host);
			log.debug("Found cached address '{}' for host {}", addr, host);
			return addr;
		}

		if (host != null) {
			try {
				InetAddress inet = InetAddress.getByName(host);
				addr = inet.getHostAddress();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return addr;
	}

	@Override
	public Data process(Data data) {

		Serializable addr = data.get(key);
		if (addr != null) {
			String name = reverseLookup(addr.toString());
			if (name != null) {
				data.put(target, name);
			}
		}

		return data;
	}

	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		String addr = event.get(getKey());
		if (addr == null)
			return event;

		String hostname = reverseLookup(addr);
		if (hostname != null) {
			event.set(getTarget(), hostname);
		}

		return event;
	}
}