package org.jwall.audit.processor;

import java.util.List;
import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;


/**
 * This simple event processor fills in a default host value for events that
 * do not come with a request header including a <code>Host:</code>-field.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class DefaultRequestHost 
	implements EventProcessor<AuditEvent> 
{
	String defaultHost = "N/A";
	
	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event, java.util.Map)
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context) throws Exception {
		
		List<String> values = event.getAll( ModSecurity.REQUEST_HEADERS + ":Host" );
		if( !event.isSet( ModSecurity.REQUEST_HEADERS + ":Host" ) || values == null || values.isEmpty() || "".equals( values.get(0).trim() ) ){
			event.set( ModSecurity.REQUEST_HEADERS + ":Host".toUpperCase(), defaultHost );
		}
		
		return event;
	}

	/**
	 * A simple getter for the default host value.
	 * 
	 * @return
	 */
	public String getValue() {
		return defaultHost;
	}

	
	/**
	 * A simple setter for the default host value.
	 * 
	 * @param defaultHost
	 */
	public void setValue(String defaultHost) {
		this.defaultHost = defaultHost;
	}
}