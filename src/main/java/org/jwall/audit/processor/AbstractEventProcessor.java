package org.jwall.audit.processor;

import java.util.Map;

import org.jwall.audit.Event;
import org.jwall.audit.EventProcessor;

public abstract class AbstractEventProcessor<E extends Event> 
	implements EventProcessor<E> 
{
	/** This processor's priority */
	Double priority = 100.0d;
	
	/** This flag signals whether this processor is enabled or not */
	boolean enabled = true;
	
	
	public AbstractEventProcessor( Double defaultPriority ){
		priority = defaultPriority;
	}
	

	@Override
	public E processEvent(E event, Map<String, Object> context) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}
	

	/**
	 * @return the priority
	 */
	public Double getPriority() {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Double priority) {
		this.priority = priority;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isEnabled() {
		return enabled;
	}
}