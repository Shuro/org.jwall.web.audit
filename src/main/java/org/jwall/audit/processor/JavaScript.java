package org.jwall.audit.processor;

import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class JavaScript extends stream.script.JavaScript implements
		EventProcessor<AuditEvent> {
	static Logger log = LoggerFactory.getLogger(JavaScript.class);

	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		if (event == null) {
			log.debug("Event is 'null' - skipping JavaScript processing.");
			return event;
		}

		this.scriptEngine.put("context", context);
		this.scriptEngine.put("event", event);
		String theScript = loadScript();

		log.debug("Evaluating JavaScript for event '{}'",
				event.get(ModSecurity.TX_ID));
		scriptEngine.eval(theScript);
		log.debug("Returning event...");
		return event;
	}
}