package org.jwall.audit.processor;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.jwall.web.audit.util.VariableContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import stream.runtime.setup.ObjectFactory;

/**
 * This class reads an XML file of AuditEventProcessors, instantiates each
 * processor and deploys it to the local EventProcessor pipeline.
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AuditEventProcessorFinder {

	/* The logger for this class */
	static Logger log = LoggerFactory
			.getLogger(AuditEventProcessorFinder.class);

	final static ObjectFactory objectFactory = ObjectFactory.newInstance();
	static {
		objectFactory.addPackage("org.jwall.audit.processor");
		objectFactory.addPackage("org.jwall.web.audit.processor");
	}

	public static AuditEventProcessorPipeline deployCustomEventProcessors(
			File file) throws Exception {
		AuditEventProcessorPipeline pipe = new AuditEventProcessorPipeline();
		AuditEventProcessorFinder finder = new AuditEventProcessorFinder();
		finder.deployCustomEventProcessors(file, pipe);
		return pipe;
	}

	public AuditEventProcessorFinder() {

		for (Object k : System.getProperties().keySet()) {
			String key = k.toString();
			String value = System.getProperty(key);
			log.debug("Adding ('{}', '{}') to object-factory...", key, value);
			objectFactory.set(key, value);
		}
	}

	public void deployCustomEventProcessors(File procDefs,
			AuditEventProcessorPipeline pipeline) throws Exception {
		deployCustomEventProcessors(new FileInputStream(procDefs), pipeline);
	}

	public void deployCustomEventProcessors(InputStream processorDefinitions,
			AuditEventProcessorPipeline pipeline) throws Exception {

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(processorDefinitions);
		Element root = doc.getDocumentElement();

		if (root.getNodeName().equalsIgnoreCase("event-processors")) {
			registerProcessors(pipeline, root.getChildNodes());
			return;
		}

		NodeList eventProcessorList = root
				.getElementsByTagName("event-processors");

		registerProcessors(pipeline, eventProcessorList);
	}

	protected static void registerProcessors(
			AuditEventProcessorPipeline pipeline, NodeList eventProcessorList) {
		for (int j = 0; j < eventProcessorList.getLength(); j++) {
			Node epNode = eventProcessorList.item(j);
			if (epNode.getNodeType() == Node.ELEMENT_NODE) {
				registerProcessors(pipeline, (Element) epNode);
			}
		}

	}

	protected static void registerProcessors(
			AuditEventProcessorPipeline pipeline, Element node) {
		try {
			Map<String, String> params = objectFactory.getAttributes(node);

			Double priority = new Double(1000.0d);
			if (params.get("priority") != null)
				priority = new Double(params.get("priority"));

			log.info("Found priority {}", priority);

			Map<String, String> parameter = objectFactory.getAttributes(node);
			Map<String, String> expanded = new HashMap<String, String>();
			VariableContext ctx = new VariableContext(System.getProperties());
			for (String key : parameter.keySet()) {
				expanded.put(key, ctx.expand(parameter.get(key)));
			}

			log.debug("Creating object from '{}' with parameters: {}",
					node.getNodeName(), expanded);
			Object o = objectFactory.create(node.getNodeName(), expanded, node);

			if (o instanceof EventProcessor) {
				@SuppressWarnings({ "unchecked" })
				EventProcessor<AuditEvent> eventProcessor = (EventProcessor<AuditEvent>) o;
				log.debug(
						"Registering AuditEventProcessor {} with priority {}",
						eventProcessor, priority);
				pipeline.register(priority, eventProcessor);
			}

		} catch (Exception e) {
			log.error("Failed to register processor: {}", e.getMessage());
			if (log.isDebugEnabled())
				e.printStackTrace();
		}
	}
}