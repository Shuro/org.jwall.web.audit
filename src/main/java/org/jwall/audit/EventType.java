package org.jwall.audit;

public enum EventType {

	AUDIT("AuditEvent"), AUDIT_EVENT_MESSAGE("AuditEventMessage"), ACCESS(
			"AccessLog"), ERROR("ErrorLog"), GENERIC("GenericLog"), DATABASE(
			"DatabaseLog");

	String name;

	EventType(String str) {
		this.name = str;
	}

	public String toString() {
		return this.name;
	}
}
