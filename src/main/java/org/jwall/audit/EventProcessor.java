package org.jwall.audit;

import java.util.Map;


/**
 * <p>
 * This interface defines an abstract event processor class which is able
 * to process events for a given generic type class.
 * </p>
 * <p>
 * In contrast to an EventListener, classes implementing this interface are
 * responsible for any pre-processing of an event.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E>
 */
public interface EventProcessor<E extends Event> {

	public final static String DELETE_FLAG = "event.flag.delete";
	
	
	/**
	 * This method is called upon event reception. The given map provides a context to 
	 * communicate data between several event-processors. The implementor of this interface
	 * needs to take care that calls to this class my come from different threads, i.e.
	 * no state should be stored in the class to diminish the risk of concurrency problems.
	 * 
	 * @param event
	 * @param context
	 * @return
	 * @throws Exception
	 */
	public E processEvent( E event, Map<String,Object> context ) throws Exception;
}
