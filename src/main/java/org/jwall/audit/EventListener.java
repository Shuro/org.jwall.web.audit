package org.jwall.audit;


/**
 * This interface provides a generic way to define a listener class, which
 * can be notified upon arrival of new events.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <E> The type of events listened for by the implementing class.
 */
public interface EventListener<E extends Event> {

	/**
	 * This method is called upon arrival of a new event of the given type.
	 * 
	 * @param event
	 */
	public void eventArrived( E event );
}
