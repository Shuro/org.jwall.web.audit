/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.filter.Operator;
import org.jwall.web.audit.rules.Condition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;


/**
 * <p>
 * This class implements the most generic ModSecurity-like match. It matches the value of a
 * given variable against a single or set of values using a specific operator. This implementation
 * supports the operators
 * <ul>
 *    <li><code>rx</code>, by matching the value against regular expressions</li>
 *    <li><code>pm</code>, by looking for substring matches</li>
 *    <li><code>eq</code>, by looking for equality of strings</li>
 * </ul>
 * 
 * All matches can be inverted by prepending an exclamation-mark &quot;!&quot; to the value.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
@XStreamAlias("Match")
public class Match 
	implements Serializable
{
    /** The unique class ID */
    private static final long serialVersionUID = -3682911278800228726L;
    static Logger log = LoggerFactory.getLogger( Match.class );
    
    public final static Set<String> NUMERIC_FIELDS = new HashSet<String>();

    
    @XStreamAsAttribute
    protected String op = Operator.EQ.toString();
    
    @XStreamAlias("Variable")
    protected String variable;
    
    @XStreamImplicit( itemFieldName="Value" )
    protected TreeSet<String> values = new TreeSet<String>();

    protected Operator operator;
    protected Condition condition;

    
    protected Match(){
    }
    
    
    public Match( String variable ){
        this.variable = variable;
        this.values = new TreeSet<String>();
    }

    public Match( String variable, String value ){
        this( variable, Operator.EQ, value );
    }


    public Match( String variable, Operator op, String value){
        this.variable = variable;
        this.values.add( value );
        this.op = op.toString();
    }

    public String getOperator()
    {
        return op;
    }
    
    public void setOperator( String op ){
        this.op = op;
    }
    
    public Operator getOp(){
    	if( operator == null ){
    		try {
    			operator = Operator.read( op );
    		} catch (Exception e) {
    			operator = Operator.EQ;
    		}
    	}
    	return operator;
    }
    
    
    /**
     * @return the variable
     */
    public String getVariable()
    {
    	if( variable == null )
    		return null;
        return variable.toUpperCase();
    }


    /**
     * @param variable the variable to set
     */
    public void setVariable(String variable)
    {
        this.variable = variable;
    }

    
    

    /**
	 * @return the value
	 */
	public String getValue() {
		if( values.isEmpty() )
			return null;
		
		return values.iterator().next();
	}

	
	public Object getValueObject(){
		
		if( getValue().toString().matches( "-?\\d+" ) && (this.getVariable().equals( AuditEvent.SEVERITY ) || getVariable().equals( ModSecurity.RULE_SEV ) ))
			return new Integer( getValue().toString() );
		
		return getValue();
	}
	

    public void setValue( String val ){
        this.values.clear();
        this.values.add( val );
    }


    

    /**
     * Returns an XML representation of this match.
     * @return
     */
    public String toXML(){
        return AuditEventFilter.getXStream().toXML( this );
    }
    
    public String toString(){
    	return "[[" + this.getVariable().toLowerCase() + " " + this.op + " " + this.getValue() + " ]]";
    }
}