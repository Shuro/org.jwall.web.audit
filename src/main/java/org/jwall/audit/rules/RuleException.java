package org.jwall.audit.rules;

public class RuleException extends Exception {

	/** The unique class ID */
	private static final long serialVersionUID = 4886853091367534573L;

	public RuleException(){
		super();
	}
	
	public RuleException( String msg ){
		super( msg );
	}
}
