package org.jwall.audit.rules;

import org.jwall.audit.Event;

public interface EventRule<E extends Event> {

	
	public boolean matches( E event, RuleContext ctx ) throws RuleException;
}
