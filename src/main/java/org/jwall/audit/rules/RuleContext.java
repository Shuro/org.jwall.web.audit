/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.audit.rules;


/**
 * <p>
 * This interface defines methods provided by all environments in which a (set of) action(s)
 * are executed. Usually instances of this class are set-up for each user-context for a given
 * event and all rules are executed/evaluated within this context.
 * This allows rules to set variables and transport information to rules being fired later on. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface RuleContext
{
    /** */
    public final static String EVENT_PROCESSED = "event.processed";
    
    
    /**
     * This method allows for setting a specific object in the context. If an object
     * with that name already exists, it will be overridden.
     * 
     * @param var
     * @param o
     */
    public void set( String var, Object o );
    
    
    /**
     * This method allows to retrieve an object from the context by some variable name. 
     * 
     * @param var
     * @return
     */
    public Object get( String var );
    
    
    /**
     * Add a log message to the rule processing context.
     * 
     * @param level The log-level of the message.
     * @param msg   The message to log.
     */
    public void log( int level, String msg );
}
