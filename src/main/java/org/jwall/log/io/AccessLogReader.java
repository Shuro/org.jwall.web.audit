package org.jwall.log.io;

import java.io.InputStream;
import java.io.Reader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This reader reads lines from a source and parses each line with the
 * AccessLogParser implementation.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AccessLogReader extends GenericLogReader {

	static Logger log = LoggerFactory.getLogger( AccessLogReader.class );
	
	public AccessLogReader(InputStream in) {
		super(new AccessLogParser(), in);
	}
	
	public AccessLogReader( Reader r ){
		super(new AccessLogParser(), r);
	}
}