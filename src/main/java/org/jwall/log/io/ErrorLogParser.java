package org.jwall.log.io;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.jwall.audit.EventType;
import org.jwall.log.LogMessage;
import org.jwall.log.LogMessageImpl;
import org.jwall.web.audit.io.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A simple reader for Apache error log messages.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ErrorLogParser 
extends GenericLogParser
{
	/** The unique class ID */
	private static final long serialVersionUID = 2784731722816969735L;
	
	public final static String DATE_FORMAT = "[EEE MMM dd HH:mm:ss yyyy]";
	public final static String DATE_PATTERN = "\\[(.*?)\\]";

	static Logger log = LoggerFactory.getLogger( ErrorLogParser.class );
	DateFormat fmt = new SimpleDateFormat( DATE_FORMAT, Locale.ENGLISH );

	public LogMessage parse( String line ) throws ParseException {

		if( line == null )
			return null;

		Long time = 0L; //System.currentTimeMillis();

		MParser p = new MParser();
		String dt = p.readToken( line );
		try {
			Date d = fmt.parse( dt.trim() );
			time = d.getTime();
		} catch (Exception e) {
			log.error( "Failed to parse date '{}' with format-pattern {}", dt.trim(), DATE_FORMAT );
			e.printStackTrace();
			
			time = 0L; //System.currentTimeMillis();
		}

		// maybe we need to extract the source from the message? e.g. in the remote-syslog setting?
		//
		LogMessageImpl msg = new LogMessageImpl( EventType.ERROR, time, "", line );
		log.debug( "Adding defaults {}", defaultValues );
		for( String key : this.defaultValues.keySet() ){
			String val = defaultValues.get( key );
			log.debug( "Adding default value '{}' = '{}'", key, val );
			msg.set( key, val );
		}

		return msg;
	}
	
	
	public static void main( String[] args ) throws Exception {
		String s = "[Fri Nov 20 21:40:42 2009]";
		DateFormat fmt = new SimpleDateFormat( DATE_FORMAT, Locale.ENGLISH );
		System.out.println( "DATE_FORMAT = " + DATE_FORMAT );
		System.out.println( "fmt = " + fmt.toString() );
		System.out.println( s + "  parses to: " + fmt.parse( s ) );
	}
}