package org.jwall.log.io;

import java.io.IOException;
import java.util.Map;

import org.jwall.log.LogMessage;

/**
 * This interface defines the methods provided by all readers which provide
 * access to parsing log-messages from files.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public interface LogReader {

	/**
	 * Reads the next LogMessage. Will return <code>null</code> if no more messages
	 * can be read.
	 * 
	 * @return
	 * @throws IOException
	 */
	public LogMessage readNext() throws IOException;
	
	
	/**
	 * This sets a number of default properties, which will be given to all messages
	 * created by the implementing reader. The defaults may be overwritten by more
	 * message-specific values during parsing.
	 * 
	 * @param defaults
	 */
	public void setDefaults( Map<String,String> defaults );
}
