package org.jwall.log.io;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.jwall.audit.EventType;
import org.jwall.log.LogMessage;
import org.jwall.log.LogMessageImpl;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AccessLogParser extends GenericLogParser {

	/** The unique class ID */
	private static final long serialVersionUID = 3846739323473384809L;
	
	static Logger log = LoggerFactory.getLogger( AccessLogParser.class );
	
	DateFormat fmt = new SimpleDateFormat( AccessLogParser.DATE_FORMAT2, Locale.US );
	
	
	public LogMessage parse( String l ) throws ParseException {
		Long time = 0L;
		
		MParser p = new MParser();
		String remoteAddress = p.readToken( l );
		String remoteUser = p.readToken( l );
		@SuppressWarnings("unused")
		String unknown = p.readToken( l );
		
		String dt = p.readToken( l );
		if( dt != null )
			dt = dt.trim();
		try {
			Date date = fmt.parse( dt );
			time = date.getTime();
		} catch (Exception e) {
			log.error( "Date format error for date-string '{}'? {}", dt, e.getMessage() );
			e.printStackTrace();
			time = 0L; //System.currentTimeMillis(); // extractTimestamp( line );
		}
		
		// maybe we need to extract the source from the message? e.g. in the remote-syslog setting?
		//
		LogMessageImpl msg = new LogMessageImpl( EventType.ACCESS, time, "", l );
		for( String key : this.defaultValues.keySet() ){
			String val = defaultValues.get( key );
			log.debug( "Adding default value '{}' = '{}'", key, val );
			msg.set( key, val );
		}

		String request = p.readToken( l );
		String status = p.readToken( l );
		String size = p.readToken( l );
		String referer = p.readToken( l );
		String userAgent = p.readToken( l );
		if( !MParser.isEmpty( userAgent ) )
			msg.set( ModSecurity.REQUEST_HEADERS + ":User-Agent", userAgent );

		if( !MParser.isEmpty( status ) )
			msg.set( ModSecurity.RESPONSE_STATUS, status );
		
		if( !MParser.isEmpty( size ) )
			msg.set( "RESPONSE_SIZE", size );
		
		if( !MParser.isEmpty( referer ) )
			msg.set( ModSecurity.RESPONSE_HEADERS + ":Referer", referer );
		
		if( !MParser.isEmpty( remoteUser ) )
			msg.set( ModSecurity.REMOTE_USER, remoteUser );
		
		msg.set( ModSecurity.REMOTE_ADDR, remoteAddress );
		try {
			if( request.indexOf( " " ) > 0 ){
				String[] tok = request.split( "\\s+" );
				if( tok.length > 1 )
					msg.set( ModSecurity.REQUEST_URI, tok[1] );
			} else
				msg.set( ModSecurity.REQUEST_URI, request );
			
		} catch (Exception e) {
			log.error( "Failed to extract REQUEST_URI from: {}", request );
			e.printStackTrace();
		}
		return msg;
	}
	
    
    public class Tokenizer {
    	int pos = 0;
    	String str;
    	String token = null;
    	
    	public Tokenizer( String str ){
    		this.str = str;
    		readNext();
    	}
    	
    	public boolean endOfLine(){
    		return pos >= str.length();
    	}
    	
    	public boolean hasMoreTokens(){
    		return token != null;
    	}
    	
    	public String nextToken(){
    		String cur = token;
    		token = readNext();
    		return cur;
    	}
    	
    	private String readNext(){
    		skipWhitespace();
    		
    		if( endOfLine() )
    			return null;
    		
    		if( str.charAt( pos ) == '"' )
    			return readQuoted( '"' );
    		
    		StringBuffer b = new StringBuffer();
    		while( ! endOfLine() && str.charAt( pos ) != ' ' )
    			b.append( str.charAt( pos++ ) );
    		
    		return b.toString();
    	}
    	
    	private void skipWhitespace(){
    		while( pos < str.length() && Character.isWhitespace( str.charAt( pos ) ) )
    			pos++;
    	}
    	
    	public String readQuoted( char qc ){
    		StringBuffer b = new StringBuffer();
    		char last = ' ';
    		
    		while( ! endOfLine() && str.charAt( pos ) != qc && last != '\\' ){
    			last = str.charAt( pos++ );
    			b.append( last );
    		}
    		
    		return b.toString();
    	}
    }
    
    
    
    public static void main( String[] args ) throws Exception {
    	String str = "[22/Nov/2009:12:41:43 +0100]";
    	DateFormat fmt = new SimpleDateFormat( DATE_FORMAT2 );
    	System.out.println( "Format: " + DATE_FORMAT2 );
    	System.out.println( "String: " + str );
    	Date date = fmt.parse( str );
    	System.out.println( "Date: " + date );
    	
    	str = "[09/Dez/2009:20:45:51 +0100]";
    	System.out.println( "Format: " + DATE_FORMAT2 );
    	System.out.println( "String: " + str );
    	date = fmt.parse( str );
    	System.out.println( "Date: " + date );
    	
    	
    	str = "91.209.196.76 - - [22/Nov/2009:09:09:10 +0100] \"quit\" 500 24576 \"-\" \"-\"";
    	AccessLogParser parser = new AccessLogParser();
    	parser.parse( str );
    }
}