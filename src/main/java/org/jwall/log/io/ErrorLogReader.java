package org.jwall.log.io;

import java.io.InputStream;
import java.io.Reader;


/**
 * A simple reader for Apache error log messages.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ErrorLogReader 
	extends GenericLogReader
{
	public final static String DATE_FORMAT = "EEE MMM dd HH:mm:ss yyyy";
	public final static String DATE_PATTERN = "\\[(.*?)\\]";


	public ErrorLogReader(InputStream in) {
		super(new ErrorLogParser(), in);
	}
	
	public ErrorLogReader( Reader r ){
		super(new ErrorLogParser(), r);
	}
}