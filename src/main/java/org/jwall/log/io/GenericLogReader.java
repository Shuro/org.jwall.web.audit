package org.jwall.log.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.log.LogMessage;

public class GenericLogReader implements LogReader {

	public final static String TIME_PATTERN = "\\d\\d:\\d\\d:\\d\\d";
	
	public final static String DATE_FORMAT1 = "EEE MMM dd HH:mm:ss yyyy";
	public final static String DATE_PATTERN1 = "\\[(.*?)\\]";
	
	public final static String DATE_FORMAT2 = "[dd/MMM/yyyy HH:mm:ss Z]";
	public final static String DATE_PATTERN2 = "\\[\\d\\d/\\w{1,3}\\d{4,4}\\s\\d\\d:\\d\\d:\\d\\d\\s(.*)?\\]";
	
	String dateFormat = DATE_FORMAT1;
	String datePattern = DATE_PATTERN1;
	DateFormat fmt = new SimpleDateFormat( dateFormat, Locale.ENGLISH );

	Pattern p;
	BufferedReader reader;
	GenericLogParser parser;

	
	public GenericLogReader( GenericLogParser parse, Reader r ){
		this.parser = parse;
		reader = new BufferedReader( r );
		p = Pattern.compile( datePattern );
	}
	
	public GenericLogReader( GenericLogParser parse, InputStream in ){
		this( parse, new InputStreamReader( in ) );
	}

	
	public GenericLogReader( GenericLogParser parse, InputStream in, String dPattern, String dFormat ){
		this( parse, in );
		datePattern = dPattern;
		dateFormat = dFormat;
		fmt = new SimpleDateFormat( dateFormat, Locale.ENGLISH );
	}
	
	
	public void setDefaults( Map<String,String> defaults ){
		parser.setDefaults( defaults );
	}
	
	
	public void setDefault( String key, String val ){
		parser.setDefault(key, val);
	}
	
	public String getDefault( String key ){
		return parser.getDefault( key );
	}
	
	
	/**
	 * @see org.jwall.log.io.LogReader#readNext()
	 */
	@Override
	public LogMessage readNext() throws IOException {
		String line = reader.readLine();
		if( line == null )
			return null;

		try {
			LogMessage msg = parser.parse( line );
			return msg;
		} catch (Exception e) {
			throw new IOException( e.getMessage() );
		}
	}
	
	
	protected Long extractTimestamp( String line ){
		Matcher m = p.matcher( line );
		while( m.find() ){
			try {
				String str = line.substring( m.start() + 1, m.end() - 1 );
				return fmt.parse( str ).getTime();
			} catch (ParseException e) {
			}
		}
		return System.currentTimeMillis();
	}
}