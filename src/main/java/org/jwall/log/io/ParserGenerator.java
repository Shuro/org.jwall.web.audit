package org.jwall.log.io;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.web.audit.io.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ParserGenerator
{
    static Logger log = LoggerFactory.getLogger( ParserGenerator.class );
    String grammar;

    public ParserGenerator( String grammar ){
        this.grammar = grammar;
    }

    public Map<String,String> parse( String str ) throws ParseException {
        return newParser().parse( str );
    }

    public Parser<Map<String,String>> newParser(){
        return create( grammar );
    }

    public List<String> parseGrammar( String grammar ){
        List<String> toks = new ArrayList<String>();
        int off = 0;
        for( int i = 0; i < grammar.length(); i++ ){
            log.debug( "Parsing grammar: '{}'", grammar.substring( i ) );
            if( grammar.startsWith( "%{", i ) ){
                if( i > off ){
                    String con = grammar.substring( off, i );
                    log.debug( "adding constant {}", con );
                    toks.add( con );
                }

                int end = grammar.indexOf( "}", i + 1 );
                if( end >= i ){
                    String var = grammar.substring( i, end + 1 );
                    log.debug( "Found variable {}", var );
                    toks.add( var );
                    off = end + 1;
                    i += (var.length() - 1);
                }
            } else {
                if( grammar.indexOf( "%{", i ) < 0 ){
                    log.debug( "Found no more variables, treating remainder string as constant token!" );
                    toks.add( grammar.substring( i ) );
                    return toks;
                }
            }
        }

        return toks;
    }


    public Parser<Map<String,String>> create( String grammarDefinition ){

        List<Token> tokenDefs = new ArrayList<Token>();
        List<String> tokens = parseGrammar( grammarDefinition ); // QuotedStringTokenizer.splitRespectQuotes( grammarDefinition, ' ' );
        log.info( "Grammar tokens: {}", tokens );

        for( String toks : tokens ){
            tokenDefs.add( new Token( toks ) );
        }

        return new GenericParser( tokenDefs );
    }


    public boolean isVariableToken( String str ){
        boolean var = str != null && str.startsWith( "%{" ) && str.endsWith( "}" );
        log.debug( "isVariableToken( {} ) = {} ", str, var );
        return var;
    }



    public static class Token {
        String value;
        Pattern pattern;

        public Token( String name ){
            this.value = name;

            if( !name.startsWith( "%{" ) ){
                try {
                    log.debug( "trying to treat '{}' as regular expression", name );
                    pattern = Pattern.compile( name );
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            } else {

                try {
                    int idx = name.indexOf( "|" );
                    int end = name.lastIndexOf( "}" );
                    if( idx >= 0 && end > idx ){
                        pattern = Pattern.compile( name.substring( idx + 1, end ) ); 
                    }
                    log.debug( "Created regex-token with regex = '{}'", pattern );
                } catch (Exception e) {
                    log.debug( "Failed to compile pattern: {}", e.getMessage() );
                    if( log.isDebugEnabled() )
                        e.printStackTrace();
                    pattern = null;
                }
            }
        }

        public boolean isRegex(){
            return pattern != null;
        }

        public Pattern getPattern(){
            return pattern;
        }

        public boolean isVariable(){
            return value != null && value.startsWith( "%{" ) && value.endsWith( "}" );
        }

        public String getName(){
            if( isVariable() ){
                String str = value.substring( 2, value.length() - 1 );
                if( str.indexOf( "|" ) > 0 )
                    return str.substring( 0, str.indexOf( "|" ) );
                return str;
            }

            return value;
        }

        public int skipLength( String str ){
            if( isRegex() ){
                log.debug( "Checking skip-length for pattern '{}' on string {}", pattern.toString(), str );

                Matcher matcher = pattern.matcher( str );
                if( matcher.find() ){
                    int start = matcher.start();
                    int end = matcher.end();
                    log.debug( "checking string '{}'", str );
                    String val = str.substring( start, end );
                    log.debug( "substring '{}' matches {}", val, pattern );
                    return val.length();
                }
            }

            return value.length();
        }

        public String getValue(){
            return value;
        }
    }
}