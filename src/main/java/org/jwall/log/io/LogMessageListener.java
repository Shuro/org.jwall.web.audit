package org.jwall.log.io;

import org.jwall.log.LogMessage;


/**
 * This interface is to be implemented by all classes which want to be notified
 * about the arrival of new log messages.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface LogMessageListener {

	/**
	 * This method is called whenever a new log message arrived at the
	 * instance where this listener is registered.
	 * 
	 * @param m
	 */
	public void messageArrived( LogMessage m );
}
