package org.jwall.log;

import java.io.Serializable;

import org.jwall.audit.Event;



/**
 * <p>
 * This interface defines a simple abstract log message entity, comprised
 * of a timestamp, a source-identifier and the date itself.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
public interface LogMessage extends Serializable, Event {

	public final static String SOURCE = "SOURCE";
	public final static String MESSAGE = "MESSAGE";
	public final static String LOG_LEVEL = "LOG_LEVEL";
	
	/**
	 * @return the source
	 */
	public abstract String getSource();

	/**
	 * @return the message
	 */
	public abstract String getMessage();
	
	
	public String get( String variable );
	
	public void set( String variable, String value );
}