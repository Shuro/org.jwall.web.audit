package org.jwall.log;

import java.util.EventListener;


/**
 * This interface defines a listener interface for
 * LogMessages.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface LogMessageListener 
	extends EventListener 
{

}
