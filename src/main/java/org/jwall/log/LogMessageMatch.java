package org.jwall.log;

import org.jwall.audit.Filter;
import org.jwall.web.audit.filter.Operator;

public class LogMessageMatch implements Filter<LogMessage> {

	String variable;
	Operator op;
	String value;
	
	
	public LogMessageMatch( String var, Operator op, String val ){
		this.variable = var;
		this.op = op;
		this.value = val;
	}
	
	
	@Override
	public boolean matches(LogMessage event) {
		return false;
	}
}