package org.jwall.log;

import org.jwall.audit.EventView;


/**
 * This interface defines a view for a set of log events (log messages). It allows
 * for filtering and counting log events based on filter expressions.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface LogMessageView extends EventView<LogMessage> {

}