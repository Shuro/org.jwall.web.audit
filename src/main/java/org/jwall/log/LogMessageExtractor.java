package org.jwall.log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.jwall.audit.FeatureExtractor;

public class LogMessageExtractor implements FeatureExtractor<LogMessage,List<String>> {

	public final static Set<String> VARIABLES = new LinkedHashSet<String>();
	static {
		VARIABLES.add( LogMessage.TIMESTAMP );
		VARIABLES.add( LogMessage.SOURCE );
		VARIABLES.add( LogMessage.MESSAGE );
	}
	
	@Override
	public List<String> extract(String feature, LogMessage event) {
		List<String> vals = new ArrayList<String>();
		if( event.get( feature ) != null )
			vals.add(event.get( feature ));
		return vals;
	}

	@Override
	public Set<String> getVariables(LogMessage event) {
		return Collections.unmodifiableSet( VARIABLES );
	}
}