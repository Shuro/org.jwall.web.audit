package org.jwall.log;

import java.util.HashMap;
import java.util.Map;

import org.jwall.audit.EventType;




/**
 * A simple implementation of the LogMessage interface.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class LogMessageImpl implements LogMessage {

	/** The unique class ID */
	private static final long serialVersionUID = 1118705739440576195L;
	
	EventType type;
	Long timestamp;
	String source;
	String msg;
	Map<String,String> vals = new HashMap<String,String>();
	
	public LogMessageImpl( EventType type, Long timestamp, String source, String msg ){
		this.type = type;
		this.timestamp = timestamp;
		this.source = source;
		this.msg = msg;
	}
	
	

	/**
	 * @return the type
	 */
	public EventType getType() {
		return type;
	}



	/**
	 * @see org.jwall.log.LogMessage#getMessage()
	 */
	@Override
	public String getMessage() {
		return msg;
	}

	
	/**
	 * @see org.jwall.log.LogMessage#getSource()
	 */
	@Override
	public String getSource() {
		return source;
	}
	

	/**
	 * @see org.jwall.log.LogMessage#getTimestamp()
	 */
	@Override
	public Long getTimestamp() {
		return timestamp;
	}
	
	public String toString(){
		return msg;
	}


	@Override
	public String get(String variable) {
		if( TIMESTAMP.equals( variable ) )
			return getTimestamp() + "";
	
		return this.vals.get( variable );
	}
	
	public void set( String variable, String val ){
		vals.put( variable, val );
	}
}