/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http;

import java.nio.ByteBuffer;

public class HttpChunk
    extends HttpResponse
{
    int size = 0;

    public HttpChunk( String header, ByteBuffer data ) throws ProtocolException {
        super( header, data.array() );
        size = data.capacity();
    }
    
    
    /* (non-Javadoc)
     * @see org.jwall.web.http.HttpResponse#parseStartLine(java.lang.String)
     */
    @Override
    protected void parseStartLine(String line) throws MessageFormatException
    {
    }


    public int size(){
        return getChunkSize();
    }
    
    public int getChunkSize(){
        return size;
    }
    
    public boolean isChunk(){
        return true;
    }
}
