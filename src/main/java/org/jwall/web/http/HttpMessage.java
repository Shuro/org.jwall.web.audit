/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Set;


/**
 * 
 * This class implements an abstract HttpMessage. It includes a simple
 * HttpHeader and a body given as a byte-buffer. In addition to that
 * it provides several constants as defined in RFC2616. 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public abstract class HttpMessage {
    protected String version = ""; // holds the protocol version
    protected ByteBuffer body = ByteBuffer.allocate(0);

    protected HttpHeader header = null;


    public HttpMessage( HttpHeader mh, byte[] reqBody )
    throws MessageFormatException
    {
        header = mh;
        body = ByteBuffer.wrap( reqBody );
        parseStartLine( header.startLine );
    }

    /**
     * 
     * This method must be overwritten
     * 
     * @param line
     * @throws MessageFormatException
     */
    protected abstract void parseStartLine( String line ) throws MessageFormatException;

    public Set<String> getHeaderNames(){
        return header.getHeaderNames();
    }

    public String getHeader(){
        return header.getHeader();
    }

    public String getHeader( String h ){
        if( HttpHeader.CASE_INSENSITIVE_HEADERS )
            return header.getHeader( h.toLowerCase() );

        return header.getHeader( h.toLowerCase() );
    }


    public HttpHeader getMessageHeader(){
        return header;
    }

    public ByteBuffer getBody(){
        return body;
    }
    
    public boolean hasBody(){
        return body != null;
    }

    public String getBodyAsString(){
        if( body == null || body.limit() == 0 )
            return "";

        CharsetDecoder decoder = null;
        try {
            String enc = header.getHeader( HttpHeader.CONTENT_ENCODING );
            Charset cs = Charset.forName( enc );
            decoder = cs.newDecoder();
        } catch (Exception e ){
            decoder = Charset.defaultCharset().newDecoder();
        }

        try {
            CharBuffer cb = decoder.decode( body );
            return cb.toString();
        } catch (Exception e) {
            return body.asCharBuffer().toString();
        }
    }

    public String getVersion(){
        return version;
    }

    public String toString(){
        return header.getHeader() + HttpHeader.CRLF;
    }
}
