/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http;

import java.nio.ByteBuffer;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class HttpHeader {
	public final static char CR = '\r';
	public final static char LF = '\n';
	public final static String SP = " ";
	public final static String CRLF = "\r\n";
	public final static String HEADER_FIELD_SEPARATOR = ":\\s*";
	
	public final static ByteBuffer CRLF_BUF = ByteBuffer.wrap( new byte[]{ 0x0d, 0x0a } );
	
	public static boolean CASE_INSENSITIVE_HEADERS = true;
	
	
	/* The following constants are the _general header_ fields as defined in RFC2616, section 4.5 */
	public final static String CONNECTION = "Connection".toLowerCase();
	public final static String DATE = "Date".toLowerCase();
	public final static String PRAGMA = "Pragma".toLowerCase();
	public final static String TRAILER = "Trailer".toLowerCase();
	public final static String TRANSFER_ENCODING = "Transfer-Encoding".toLowerCase();
	public final static String UPGRADE = "Upgrade".toLowerCase();
	public final static String VIA = "Via".toLowerCase();
	public final static String WARNING = "Warning".toLowerCase();
	public final static String CACHE_CONTROL = "Cache-Control".toLowerCase();

	// a few of the entity headers
	//
	public final static String ALLOW = "Allow".toLowerCase(); 
	public final static String CONTENT_ENCODING = "Content-Encoding".toLowerCase();
	public final static String CONTENT_LANGUAGE = "Content-Language".toLowerCase();
	public final static String CONTENT_LENGTH = "Content-Length".toLowerCase();
	public final static String CONTENT_LOCATION = "Content-Location".toLowerCase();
	public final static String CONTENT_MD5 = "Content-MD5".toLowerCase();
	public final static String CONTENT_RANGE = "Content-Range".toLowerCase();
	public final static String CONTENT_TYPE = "Content-Type".toLowerCase();
	public final static String EXPIRES = "Expires".toLowerCase();
	public final static String LAST_MODIFIED = "Last-Modified".toLowerCase();
	

	String header = null;
	String startLine = null;
	private Map<String,String> headers;
	
	public HttpHeader( String head )
		throws MessageFormatException
	{
		header = head;
		headers = parseHeader( header );
	}
	
	public Map<String,String> parseHeader( String s ) throws MessageFormatException {
		Map<String,String> ht = new LinkedHashMap<String,String>();
		String line = null;
		String[] lines = s.trim().split( HttpHeader.CRLF );
		
		try {
		    startLine = lines[0];
		    line = startLine;
		} catch (Exception e){
		    throw new MessageFormatException("Invalid message format: "+s);
		}
		
		for( int j = 1; j < lines.length; j++ ){
			String[] tok = lines[j].split( HttpHeader.HEADER_FIELD_SEPARATOR, 2 );
			if( tok.length != 2 )
				throw new MessageFormatException("Invalid header-line: "+line);

			if( HttpHeader.CASE_INSENSITIVE_HEADERS )
				ht.put( tok[0].toLowerCase(), tok[1]);
			else
				ht.put( tok[0], tok[1]);
		}
		return ht;
	}
	
	public String getHeader(){
		return header;
	}
	
	public String getHeader( String hn ){
		if( HttpHeader.CASE_INSENSITIVE_HEADERS )
			return headers.get( hn.toLowerCase() );
		
		return headers.get( hn );
	}
	
	public Set<String> getHeaderNames(){
		return headers.keySet();
	}
	
	public String debugString(){
		StringBuffer s = new StringBuffer( "\t"+startLine + "\n" );
		
		for( String k: headers.keySet() ){
			s.append( "\t" + k + " -> " + headers.get(k) + "\n" );
		}

		return s.toString();
	}
	
	/**
	 * 
	 * Returns the value (Integer) of  the Content-Length header. If
	 * the header is not present, a value of <code>-1</code> will be
	 * returned.
	 * 
	 * @return The content-length value of <code>-1</code> of no such
	 *         header is present.
	 */
	public Integer getContentLength(){
		String cl = getHeader( HttpHeader.CONTENT_LENGTH );
		if( cl != null )
			return new Integer( cl.trim() );
		
		return -1;
	}
	
	
	/**
	 * 
	 * This will return, if the header indicates a chunked transfer encoding. 
	 * 
	 * @return
	 */
	public boolean isChunked(){
		String te = getHeader( HttpHeader.TRANSFER_ENCODING );
		if( te != null && !te.trim().equals("identity") )
			return true;
		
		return false;
	}
	
	public boolean isConnectionCloseSet(){
		String con = getHeader( HttpHeader.CONNECTION );
		return con != null && con.indexOf("close") >= 0;
	}
}
