/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.http.HttpHeader;
import org.jwall.web.http.HttpMessage;
import org.jwall.web.http.ProtocolException;

/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 * @param <T> The message-type (i.e. <code>HttpRequest</code> or <code>HttpResponse</code>).
 */
public abstract class HttpMessageChannel<T extends HttpMessage> {
    private static Logger log = LoggerFactory.getLogger( "HttpMessageStream.class" );

    /** This constant defines the state from the start until a complete message header has been read */
    public final static int STATE_READING_HEADER = 0;
    
    /** After the header has been read, this constant defines the body-reading phase */
    public final static int STATE_READING_BODY = 1;

    /** The buffered byte-channel that this channel reads from */
    BufferedReadableByteChannel in;
    
    /** The timeout value (milliseconds) */
    Integer timeout = 60 * 1000;
    
    /** The time-stamp that defines the time of the last read */
    long lastData = 0;

    /** This variable defines the state of the channel (i.e. reading-header / reading-body) */
    int state = 0;

    /** This string-buffer holds a possibly partial complete message-header */
    StringBuffer partialHeader = new StringBuffer();

    /** If the first phase (read header) is completed, this variable holds the message header */
    HttpHeader header = null;

    /** This buffer represents a possibly partial read message body */
    ByteBuffer body = null;


    /**
     * 
     * This constructor creates a new instance of the message channel class. It
     * initializes the buffered byte-channel and sets the time-stamp to the 
     * current date.
     * 
     * @param inChannel The byte-channel to read messages from.
     */
    protected HttpMessageChannel( ReadableByteChannel inChannel ){
        in = new BufferedReadableByteChannel( inChannel, 1024 * 32 );
        touch();
    }


    /**
     * 
     * This method tries to read a HTTP message header from the currently
     * available bytes. If the header cannot be completely read, then the
     * partial header is saved and <code>null</code> is returned. The
     * method is re-entrant in the way, that a later invocation resumes
     * the previously header-read.
     * 
     * @return The header if it can completely be read or <code>null</code> otherwise.
     * @throws TimeOutException In case the channel timed out.
     * @throws IOException If a read-error occurs.
     * @throws ProtocolException In case the header could not be parsed or
     *         the method is called in a wrong state (e.g. body-reading).
     */
    public HttpHeader readHeader() throws TimeOutException, IOException, ProtocolException {
        log.debug("Entering HttpRequestStream.readHeader()");
        
        if( state == STATE_READING_BODY )
            throw new ProtocolException("Last header indicated a message-body - need to read that first!");

        String line = in.readLine();

        if( line == null ){
            //
            // there is no complete line available so we cannot finish our message-header
            //
            return null;
        }
        line = line.trim();
        touch();
        while( !line.equals("") ){
            //
            // wether the header-buffer is empty or not: we are 
            // extending it with the next line
            //
            partialHeader.append( line );
            partialHeader.append( HttpHeader.CRLF );
            
            line = in.readLine();
            if( line == null )
                return null;
            
            line = line.trim();
        }
        
        partialHeader.append( HttpHeader.CRLF );

        //
        // an empty line signals the end of a http-header
        //
        HttpHeader head = null;
        
        try {
            head = new HttpHeader( partialHeader.toString() );
        } catch (Exception e){
            //e.printStackTrace();
            log.error(this + " Exception: " + e.getMessage() );
            log.error(this + " PartialHeader: "+partialHeader);
        }

        // if this header signals a request-body then we
        // switch into the READ_BODY state
        //
        if( head.getContentLength() > 0 )
            state = STATE_READING_BODY;
        
        partialHeader = new StringBuffer();
        log.debug("Completed reading the current header, leaving HttpRequestStream.readHeader()");
        touch();
        return head;

    }


    /**
     * 
     * This method reads the body of a HTTP message of the given length. 
     * 
     * 
     * @param contentLength
     * @return The complete buffer or <code>null</code> if less than <code>contentLength</code> bytes
     *         can be read without blocking.
     * @throws IOException In case an I/O error occurred.
     * @throws TimeOutException If this method is called after the channel timed out.
     * @throws ProtocolException In case a parsing error occured (unlikely <code>;-)</code> or the
     *         method was called in an incorrect state (reading header state).
     */
    public ByteBuffer readBody( int contentLength ) throws IOException, TimeOutException, ProtocolException {
        
        if( state == STATE_READING_HEADER )
            throw new ProtocolException("You're trying to read a message body, but should read a header!");

        
        if( this.body == null ){
            //
            // this is the first try, so we start by allocating
            // a buffer of the desired size
            //
            body = ByteBuffer.allocate( contentLength );
        }

        int bytes = in.read( body );
        if( bytes > 0 )
            touch();
        
        log.debug( this + " readBody: managed to read {} bytes of data, buffer-limit is {}", bytes, body.limit() );
        
        if( body.remaining() == 0 ){
            //
            // ok, we had luck and managed to fully read the message-body
            // thus, we switch to the header-reading state
            //
            state = STATE_READING_HEADER;

            // we return the buffer, and set our "partial-body" buffer 
            // back to null
            ByteBuffer buf = body;
            buf.flip();
            body = null;

            log.debug("readBody: managed to fill the buffer, changing to READ_HEADER state and returning the body");
            return buf;
        }

        log.debug("readBody: there are {} bytes missing for completing the pending message-body", body.remaining() );
        return null;
    }

    
    /**
     * This method returns the <code>int</code>-code of the channel's
     * current state.
     * 
     * @return The current state of the channel.
     */
    public int getState(){
        return state;
    }

    
    /**
     * This method closes the message-channel and also closes the
     * underlying byte-channel.
     * 
     * @throws IOException In case closing the underlying byte-channel
     *         resulted in an I/O error.
     */
    public void close() throws IOException {
        in.close();
    }


    /**
     * This method returns the current timeout (in milliseconds) of the
     * channel.
     * 
     * @return The timeout in milliseconds.
     */
    public Integer getTimeout(){
        return timeout;
    }
    

    /**
     * This method is used to set the timeout (in milliseconds) of the
     * channel. This does <b>not</b> reset the time-stamp.
     * 
     * @param to The timeout (milliseconds).
     */
    public void setTimeout( Integer to ){
        timeout = to;
    }

    
    /**
     * This method can be used to check if the channel is timed out or not.
     * 
     * @return <b>true</b>, if the last read was more than <code>timeout</code>
     *         milliseconds ago, otherwise <b>false</b>.
     */
    public boolean timedOut(){
        return ( System.currentTimeMillis() - this.lastData ) > timeout.longValue();
    }


    /**
     * This method simply <i>touches</i> the channel an sets the time of the
     * last read to <code>System.currentTimeMillis()</code>.
     * 
     */
    public void touch(){
        lastData = System.currentTimeMillis();
    }

    public abstract T readMessage() throws TimeOutException, IOException, ProtocolException;
}
