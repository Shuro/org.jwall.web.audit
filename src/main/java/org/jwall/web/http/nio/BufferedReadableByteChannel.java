/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * This class implements a few convenient methods, mainly for reading a newline
 * terminated line from a channel. It relies on a standard readable byte channel
 * provided by a number of JDK classes, which are given to the constructor at
 * creation time.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class BufferedReadableByteChannel
implements ReadableByteChannel
{
    Logger log = LoggerFactory.getLogger("BufferedReadableByteChannel.class");
    
    /** The underlying byte-channel that this channel reads from */
    ReadableByteChannel ch;
    
    /** The buffer that is used internally */
    ByteBuffer buf;
    
    /** The size of the internal buffer, per default this is 32k*/
    int bufferSize = 1024 * 32;
    
    /** This string buffer holds a partial string when a read does not result in a complete line */
    StringBuffer s = new StringBuffer();


    
    /**
     * 
     * This constructor creates a new instance of this class with an internal
     * buffer of 32k bytes.
     * 
     * @param input The byte-channel this instance reads from.
     */
    public BufferedReadableByteChannel( ReadableByteChannel input ){
        this( input, 1024 * 32 );
    }

    
    /**
     * 
     * This constructor creates a new instance of this class with an internal
     * buffer of the given buffer-size.
     * 
     * @param input
     * @param bufSize
     */
    public BufferedReadableByteChannel( ReadableByteChannel input, int bufSize ){
        ch = input;
        bufferSize = bufSize;
        buf = null;
    }


    /**
     * This method returns the first index of byte <code>b</code> within
     * the given buffer <code>buf</code>. If the buffer does not contain
     * <code>b</code> then -1 will be returned. The search for <code>b</code>
     * starts at the buffers current position. 
     * 
     * @param buf
     * @param b
     * @return
     */
    private int indexOf( ByteBuffer buf, byte b[] ){
        int start = buf.position();

        for( int i = 0; i + b.length <= buf.remaining(); i++ ){
            //
            // check for presence of all bytes of b within the buffer
            //
            boolean match = true;
            for( int k = 0; k < b.length && match && i + k < buf.limit() ; k++){
                byte c = buf.get( start + i + k );
                match = match && c == b[k];
            }

            if( match && i < buf.capacity() ){
                buf.position(start);
                return start + i;
            }
        }

        buf.position(start);
        return -1;
    }

    
    /**
     * 
     * This method tries to read a full line, terminated by a <code>\n</code> newline
     * character, from the currently available bytes. If there can no complete line
     * be read (i.e. no newline char in the buffer), then the method transfers the
     * currently available bytes into a "partial complete line" and immediately
     * returns null.
     * 
     * @return A new-line terminated line or <code>null</code> if no complete line can be read
     *         from the currently available bytes.
     * @throws IOExecption in case 
     * 
     */
    public String readLine() throws IOException
    {
        byte[] CRLF = new byte[]{ (byte) 0xA0, (byte) 0xD0 };
        CRLF = new byte[]{ '\n' };
        //
        // 1. we check if our current buffer contains '\n':
        //      YES: return new String( cur.bytes( pos, indexOf('\n') ), MAKE curBuffer = theRestAfter '\n'
        //       NO: all of the buffer is the beginning of a line, need to read more: 
        //              1. append cur to StringBuffer,
        //              2. create new cur, read bytes into cur, recursively call readLine and append the result to StringBuffer
        //              3. return StringBuffer
        //
        // we fill our current buffer with READ_AHEAD bytes
        //
        //
        if( buf == null ){
            buf = ByteBuffer.allocate( bufferSize );
            
            int bytes = ch.read( buf );
            
            if( bytes < 0 ){
                ch.close();
                return new String( buf.asCharBuffer().array() );
            }
            
            if( bytes == 0 ){
                buf = null;
                return null;
            }
            buf.flip();
        }
        
        if( buf.remaining() == 0 ){
            buf.clear();
            int bytes = ch.read( buf );
            buf.flip();
            log.debug("refilled the buffer with {} new bytes of data", bytes );
            if( bytes == 0 )
                return null;
        }
        
        StringBuffer s = new StringBuffer();
        int start = buf.position();
        int end = indexOf( buf, CRLF );

        // in case "end" is within this buffer ( i.e. >= 0 )
        // we can complete our line by appending all bytes from "start" to "end". 
        //
        if( end >= 0 ){
            end += CRLF.length;
            byte[] data = new byte[ (end - start) ];
            buf.get( data );
            s.append( new String(data) );

            String str = new String( s.toString() );

            // we prepare a new string-buffer for the next "partial" read...
            //
            s = new StringBuffer();
            return str;

        } else {
            //
            // we cannot complete the line (no EndOfLine in the buffer) with the data
            // available, therefore all available data is just a portion of the string
            // and we need to delay completion to a later time...
            //
            byte[] data = new byte[buf.remaining()];
            buf.get(data);
            s.append( new String(data) );
            
            buf.clear();
            buf = null;
            return null;
        }
    }


    /* (non-Javadoc)
     * @see java.nio.channels.ReadableByteChannel#read(java.nio.ByteBuffer)
     */
    public int read(ByteBuffer dst) throws IOException
    {
        // we need to take care of the line-buffer!!
        //
        if( buf == null ){
            if( ! ch.isOpen() )
                return -1;
            
            buf = ByteBuffer.allocate( bufferSize );
            int bytes = ch.read( buf );
            log.debug("Read {} bytes of data into buffer...", bytes );
            buf.flip();
        }
        
        // if the underlying channel is closed, we need to
        // signal this to reader...
        //
        if( ! ch.isOpen() )
            return -1;
        
        int pos = buf.position();

        if( dst.remaining() <= buf.remaining() ){
            //
            // we can entirely fill the destination with our current buffer
            //
            while( dst.remaining() > 0 )
                dst.put( buf.get() );
            //buf.get( dst.array() );
            return buf.position() - pos;

        } else {
            //
            // we need to copy the whole current line-buffer and perhaps need
            // to read in a little bit more...
            //
            int bytes = buf.remaining();

            while( dst.hasRemaining() && buf.hasRemaining() )
                dst.put( buf.get() );

            buf = null;
            bytes += ch.read( dst );
            if( bytes > 0 ){
                //dst.flip();
                return bytes;
            }
            
            if( ! ch.isOpen() )
                return -1;
            
            return bytes;
        }
    }

    
    /**
     * @see java.nio.channels.Channel#close()
     */
    public void close() throws IOException
    {
        ch.close();
    }
    

    /**
     * @see java.nio.channels.Channel#isOpen()
     */
    public boolean isOpen()
    {
        return ch.isOpen();
    }
}
