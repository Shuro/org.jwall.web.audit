/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http.nio;

/**
 * 
 * This type of exception denotes the timeout of a stream or channel
 * instance, i.e. the HttpRequest-/HttpResponseChannels.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class TimeOutException extends Exception {
    
    private static final long serialVersionUID = -5404080665975288906L;

    /**
     * Creates a new instance of this class using the specified message.
     * 
     * @param msg The error message.
     */
	public TimeOutException( String msg ){
		super( msg );
	}
}
