/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http.nio;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


/**
 * 
 * This is a simple compression-handler that handles gzip-compressed message
 * payloads. Currently it is limitted to gzip.
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public class CompressionHandler {
	public static int MAX_READAHEAD_BUFFER = 128 * 1024; // 128k


	public static byte[] gunzip( byte[] data ) throws IOException {
		ByteArrayInputStream in = new ByteArrayInputStream( data );
		GZIPInputStream gz = new GZIPInputStream( in );
		return readUntilEOF( gz );
	}

	public static byte[] gzip( byte[] data ) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		GZIPOutputStream gz = new GZIPOutputStream( out );
		gz.write(data);
		gz.flush();
		gz.finish();
		out.flush();
		byte[] res = out.toByteArray();
		out.close();
		gz.close();
		return res;
	}

	public static byte[] readUntilEOF( InputStream in ) throws IOException {
		byte[] data = new byte[ MAX_READAHEAD_BUFFER ];

		int i = 0;
		byte c = 0;

		while( c != -1 ){
			c = (byte) in.read();
			if( c != -1 )
				data[i++] = c;

			if( i >= MAX_READAHEAD_BUFFER )
				throw new IOException("Read-a-head buffer to small!");
		}

		byte[] buf = new byte[i];
		for( int k = 0; k < buf.length; k++ )
			buf[k] = data[k];

		return buf;
	}



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			File f = new File( args[0] );
			boolean unzip = f.getAbsolutePath().endsWith(".gz");
			
			if( f.length() > Integer.MAX_VALUE )
				throw new Exception("File "+f+" is larger than "+Integer.MAX_VALUE+" bytes!");
			FileInputStream in = new FileInputStream( f);
			
			byte[] data = new byte[ (int) f.length() ];
			int read = in.read( data );
			if( read != data.length )
				throw new Exception("Only read "+read+" bytes, expected to read: "+data.length);

			byte[] buf = data;
			if( unzip ){
				System.out.println("gunzipped data to "+buf.length+" bytes.");
				buf = gunzip( data );
			} else {
				System.out.println("gzipped data to "+buf.length+" bytes.");
				buf = gzip( data );
			}
			
		} catch (Exception e){
			e.printStackTrace();
		}
	}

}
