/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http.nio;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.http.HttpHeader;
import org.jwall.web.http.HttpRequest;
import org.jwall.web.http.ProtocolException;

/**
 * $Id: HttpRequestStream.java 79 2007-10-22 23:43:37Z chris $ <p>
 *
 * This class implements a reader which reads from the input-stream
 * and parses http-requests. The requests are queued and can be fetched
 * by <code>getRequest()</code>.
 * 
 * 
 * @author chris@jwall.org
 */
public class HttpRequestChannel
extends HttpMessageChannel<HttpRequest>
{
    public final static String TIMEOUT_PROPERTY = "org.jwall.web.http.HttpRequestStream.timeout";
    public static Logger log = LoggerFactory.getLogger("HttpRequestStream.class");
    HttpHeader header = null;
    static int id = 0;
    int myId = 0;
    int reqNum = 0;
    
    /**
     * Creates a HttpRequestStream which parses requests from the given input-stream.
     * The <code>cid</cid>-string is used for logging.
     * 
     * @param in
     */
    public HttpRequestChannel( ReadableByteChannel in )
    throws IOException
    {
        super( in );
        
        this.state = HttpRequestChannel.STATE_READING_HEADER;
        
        myId = ++id;
        try {
            setTimeout( 1000 * Integer.parseInt( System.getProperty( TIMEOUT_PROPERTY ) ) );
        } catch ( Exception e ){
            int dt = 60;
            //System.err.println("HttpRequestStream: Using default timeout of "+dt+" seconds");
            setTimeout( dt * 1000 );
        }
    }

    public HttpRequestChannel( InputStream in ) throws IOException {
        this( Channels.newChannel( in ) );
    }


    public HttpRequest readMessage() throws TimeOutException, IOException, ProtocolException {
        return readRequest();
    }


    /**
     * This method returns the next complete request in the input-stream.
     * The result is an array which has size 1 most of the time, but can
     * be of size 2, if the request has a body. 
     * In this case, the body is returned as the second element in the array. 
     * 
     * @return A string array consisting of a request-header and possibly a
     *     request-body element.
     */
    public HttpRequest readRequest()
    throws TimeOutException, IOException, ProtocolException
    {
        if( header == null )
            header = readHeader();

        if( header == null )
            return null;

        if( header.getContentLength() > 0 && state == STATE_READING_BODY ){
            ByteBuffer buf = super.readBody( header.getContentLength() );
            if( buf == null )
                return null;
            else {
                reqNum++;
                return new HttpRequest( header, buf.array() );
            }
        } else {
            HttpHeader hd = header;
            header = null;
            state = STATE_READING_HEADER;
            reqNum++;
            return new HttpRequest( hd, new byte[0] );
        }
    }
    
    public void close()
    throws IOException
    {
        super.close();
    }
    
    public int getNumberOfRequests(){
        return reqNum;
    }

    public String toString(){
        return "HttpRequestChannel["+myId+ " request #"+reqNum+"] ";
    }
}
