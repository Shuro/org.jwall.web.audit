/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.http;

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;

import org.jwall.web.http.nio.HttpResponseChannel;


/**
 * 
 * This class represents an abstract http message.
 * 
 * 
 * @author Christian Bockermann <chris@jwall.org>
 *
 */
public class HttpResponse extends HttpMessage {
    public final static String ACCEPT_RANGES = "Accept-Ranges"; //  ; Section 14.5
    public final static String AGE = "Age";   //  ; Section 14.6
    public final static String ETAG = "ETag"; //    ; Section 14.19
    public final static String LOCATION = "Location"; //     ; Section 14.30
    public final static String PROXY_AUTHENTICATE = "Proxy-Authenticate"; //     ; Section 14.33
    public final static String RETRY_AFTER = "Retry-After"; //    ; Section 14.37
    public final static String SERVER = "Server"; //   ; Section 14.38
    public final static String VARY = "Vary"; //   ; Section 14.44
    public final static String WWW_AUTHENTICATE = "WWW-Authenticate"; //     ; Section 14.47  
	
	String msg = null;
	Integer status = null;

	/** The http stream over which this response has been received */
	protected HttpResponseChannel channel = null;

	
	public HttpResponse( HttpHeader header, byte[] data)
		throws ProtocolException
	{
		super( header, data );
		parseStartLine( header.startLine );
	}
	
	/**
	 * 
	 * 
	 * @param reqData
	 * @throws Exception
	 */
	public HttpResponse( String resHeader, byte[] responseData )
	throws ProtocolException
	{
		super( new HttpHeader(resHeader), responseData );
		parseStartLine( header.startLine );
	}


	public HttpResponse( String resHeader, byte[] responseData, HttpResponseChannel ch )
	throws ProtocolException
	{
		this( resHeader, responseData );
		channel = ch;
	}

	protected void parseStartLine( String line ) throws MessageFormatException {
		try {
			String[] token = line.split( HttpHeader.SP, 3 );

			version = token[0];
			status = new Integer( token[1] );
			msg = "";
			if( token.length > 2 )
				msg = token[2];
		} catch (Exception e ){
			throw new MessageFormatException("Invalid status-line: " + line);
		}
	}



	public HttpResponseChannel getHttpStream(){
		return channel;
	}

	public Integer getStatus(){
		return status;
	}

	public String getMessage(){
		return msg;
	}

	public boolean isConnectionClosed(){
		return header.isConnectionCloseSet();
	}

	public boolean isChunked(){
		return header.isChunked();
	}
	
	public boolean isGZipped(){
		String ce = header.getHeader( HttpHeader.CONTENT_ENCODING );
		return ce != null && ce.indexOf( "gzip" ) >= 0;
	}
	
	public String getCharset(){
		String s = getHeader( HttpHeader.CONTENT_TYPE );
		if( s == null )
			return "UTF-8";
		
		String tok[] = s.split(";");
		for( String ch : tok ){
			if( ch.startsWith("charset=") )
				return ch.substring( "charset=".length() ).trim();
		}
		
		return "UTF-8";
	}
	
	public String getContentType(){
		String s = getHeader( HttpHeader.CONTENT_TYPE );
		if( s != null ){
			String tok[] = s.split(";");
			return tok[0].trim();
		}
		
		return "";
	}
	
	public String getPayload(){
		//return new String( this.body, Charset.forName( getCharset() ) );
		try {
		    Charset cs = Charset.forName( getCharset() );
		    CharsetDecoder decoder = cs.newDecoder();
		    CharBuffer cb = decoder.decode( body );
		    return cb.toString();
		} catch (Exception e) {
		    return new String( this.body.array() );
		}
	}
	
	public void setBody( ByteBuffer body ){
		this.body = body;
	}
	
	public void setChannel( HttpResponseChannel ch ){
		this.channel = ch;
	}
	
	public HttpResponseChannel getChannel(){
		return channel;
	}
	
	public boolean isChunk(){
	    return false;
	}
}
