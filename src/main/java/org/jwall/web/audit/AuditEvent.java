/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.jwall.audit.Event;

/**
 * <p>
 * This interface defines an abstract audit-event. Specifying events as
 * interface aims at several optimized implementations. A simple one can be
 * found at <code>AuditEventImpl</code>.
 * </p>
 * <p>
 * This interface also defines several event properties, which are beyond the
 * set of variables supported by.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public interface AuditEvent extends Event, Serializable, Comparable<AuditEvent> {
	/* An internal unique ID for each audit-event. */
	public final static String ID = "ID";

	public final static String EVENT_ID = "EVENT_ID";

	/** The collection key for storing all tags for an event */
	public final static String EVENT_TAGS = "TAGS";

	/** A key denoting the time an event has been received */
	public final static String RECEIVED_AT = "RECEIVED";

	/**
	 * This is a special variable, which refers to the age of the event in
	 * milliseconds with regard to the moment the variable is accessed (i.e.
	 * calculated on-time of extraction)
	 */
	public final static String AGE = "AGE";

	/**
	 * This variable holds the ID of the sensor from which this event was
	 * received (if available)
	 */
	public final static String SENSOR_ID = "SENSOR_ID";

	/**
	 * This variable holds the name of the sensor from which this event was
	 * received (if available)
	 */
	public final static String SENSOR_NAME = "SENSOR_NAME";

	/** This simply serves as alias for SENSOR_NAME */
	public final static String SENSOR = "SENSOR";

	public final static String SENSOR_ADDR = "SENSOR_ADDR";

	public final static String SENSOR_TYPE = "SENSOR_TYPE";

	/** This variable denotes the site-id to which this event is mapped */
	public final static String SITE_ID = "SITE_ID";

	/** This variable references the site-name to which this event is mapped */
	public final static String SITE_NAME = "SITE_NAME";

	/** This variable holds the path where the event is stored */
	public final static String FILE = "FILE";

	/*
	 * This variable can be used to specify a storage URL from where this event
	 * can be retrieved
	 */
	public final static String EVENT_URL = "EVENT_URL";

	/**
	 * This variable indicates the offset (in bytes) from the beginning of the
	 * file where this event is stored
	 */
	public final static String FILE_OFFSET = "FILE_OFFSET";

	public final static String SIZE = "SIZE";

	public final static String TAGS = EVENT_TAGS;

	public final static String SEVERITY = "SEVERITY";

	public final static String MESSAGE = "MESSAGE";

	public final static String SESSION_ID = "SESSION_ID";

	public final static String[] VARIABLES = { ID, EVENT_ID, EVENT_TAGS,
			RECEIVED_AT, SENSOR_ID, SENSOR_NAME, SITE_ID, SITE_NAME, FILE,
			FILE_OFFSET, SIZE, EVENT_TAGS, SEVERITY, MESSAGE };

	/**
	 * The unique ID, created by modsecurity.
	 * 
	 * @return A unique event id.
	 */
	public abstract String getEventId();

	/**
	 * Returns the string, which contains the whole section of this audit-event.
	 * The available sections are defined in <code>ModSecurity.java</code>.
	 * 
	 * @param i
	 *            Index of the section as defined by
	 *            <code>ModSecurity.SECTION_*</code>.
	 * @return The string value of the appopriate section of this event.
	 */
	public abstract String getSection(int i);

	/**
	 * Returns the date of this events birth. The value of this date is parsed
	 * from the audit-event entry.
	 * 
	 * @return The date when this event was created.
	 */
	public abstract Date getDate();

	/**
	 * The session-id of this request or an empty string, if no session-id has
	 * been set.
	 * 
	 * @return The session id.
	 * @deprecated Use <code>get( "SESSION_ID" );</code> instead.
	 */
	public abstract String getSessionId();

	/**
	 * This method sets the session-id of this event. Its primary use is to
	 * group several events by external session-trackers.
	 * 
	 * @param id
	 *            The id of the session with which this event is associated.
	 * @deprecated Use <code>set( "SESSION_ID", id )</code> instead.
	 */
	public abstract void setSessionId(String id);

	/**
	 * Checks wether the given variable is set or not.
	 * 
	 * @param var
	 *            The name of the variable that is checked.
	 * @return <code>true</code> iff the variable is set.
	 */
	public abstract boolean isSet(String var);

	/**
	 * Returns the given variable as <code>String</code> or an empty string if
	 * the variable is not set. If the given variable is referring to a
	 * collection the first value of that collection is returned.
	 * 
	 * To test if a variable has been set, use
	 * <code>isSet(String variable)</code>.
	 * 
	 * @param var
	 *            The name of the variable.
	 * @return The value of this variable.
	 */
	public abstract String get(String var);

	/**
	 * This method returns the list of a collection which if referenced by the
	 * given variable <code>var</code>.
	 * 
	 * @param var
	 *            The variable name of the collection. May also be a
	 *            non-collection variable in which case a single-item list will
	 *            returned if the variable has a value.
	 * @return The list of values for the variable, of <code>NULL</code> if no
	 *         collection is found by that variable name.
	 */
	public List<String> getAll(String var);

	/**
	 * This method can be used to modify/set properties of the event. There is a
	 * distinction between request and event properties. The request properties
	 * are parsed from the incoming request and are immutable.
	 * 
	 * The event properties may be changed.
	 * 
	 * @param variable
	 * @param value
	 */
	public void set(String variable, String value);

	/**
	 * This method can be used to modify/set a collection of the event. There is
	 * a distinction between request and event properties. The request
	 * properties are parsed from the incoming request and are immutable.
	 * 
	 * The event properties may be changed.
	 * 
	 * @param variable
	 * @param values
	 */
	public void setAll(String variable, List<String> values);

	/**
	 * This method returns the list of all variables available in this event.
	 * 
	 * @return The list of variable names (single variables AND collections)
	 *         which may be accessed from this event.
	 */
	public List<String> getVariables();

	public abstract int compareTo(AuditEvent o);

	/**
	 * This method returns an array of strings that contain the section-data of
	 * the appopriate audit-log entry of this event.
	 * 
	 * @return The list of section-strings.
	 * @deprecated
	 */
	public abstract String[] getRawData();

	public AuditEventMessage[] getEventMessages();

	public AuditEventType getAuditEventType();
}