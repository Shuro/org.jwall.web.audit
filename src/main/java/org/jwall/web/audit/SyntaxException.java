/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

/**
 * 
 * This exception is thrown whenever a syntax-error occurs.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class SyntaxException
    extends Exception
{
    public final static long serialVersionUID = -1L;
    int line = -1;  // not set
    
    /**
     * Creates a SyntaxException with the given message.
     * 
     * @param m
     */
    public SyntaxException(String m){
        super(m);
    }
    
    public SyntaxException(String m, int ln){
        super(m);
        line = ln;
    }
    
    public void setLineNumber(int i){
        line = i;
    }
    
    public int getLineNumber(){
        return line;
    }
    
    public String getMessage(){
        if(line >= 0)
            return super.getMessage()+" at line: "+line+"!";
        
        return super.getMessage();
    }
}
