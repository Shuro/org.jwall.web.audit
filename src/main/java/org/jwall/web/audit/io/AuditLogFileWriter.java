/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.ModSecurity;


/**
 * 
 * This is a persistence-thread which listens for events and writes them to a file.
 * Writing to a file can also be configured to create a file for each set of events
 * grouped together by the value of the Host-header field.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AuditLogFileWriter
extends Thread
implements AuditEventListener, AuditEventWriter
{
    /** A unique logger for this class */
    private static Logger log = LoggerFactory.getLogger("AuditLogFileWriter");
    
	public final static String VERSION = "$Revision: 128 $";
	private BlockingQueue<AuditEvent> queue;
	private static HashMap<String,PrintWriter> writers = new HashMap<String,PrintWriter>();

	boolean hostBasedLogs = true;
	File logDir = new File("/tmp"); 
	double eventCount = 0.0d;

	Hashtable<String,String> map;
	String key = "Host";
	boolean steady = true;
	boolean finished = false;

	/**
	 * This constructor creates a new instance of this class which writes events to
	 * several files grouped by a request-property that is given by <code>groupBy</code>.
	 * 
	 * @param dir The basic directory where the files are created.
	 * @param logLevel A log-level for debugging.
	 * @param groupBy The name of the property by which events should by grouped.
	 */
	public AuditLogFileWriter( File dir, int logLevel, String groupBy ){
		this( dir, true, logLevel );
		this.key = groupBy;
	}

	/**
	 * 
	 *
	 */
	public AuditLogFileWriter(File dir, boolean hostBased, int logLevel){
		hostBasedLogs = hostBased;
		queue = new LinkedBlockingQueue<AuditEvent>( 25000 );
		logDir = dir;

		map = new Hashtable<String,String>();

		try {
			PrintWriter w = null;

			if(! dir.isDirectory()){
				log.debug("Writing all events to file {}", dir.getAbsolutePath() );
				hostBasedLogs = false;
				w = new PrintWriter(new FileWriter(dir));
			} else {
				if(dir.isFile()){
					hostBasedLogs = false;
					w = new PrintWriter(new FileWriter(dir.getAbsoluteFile()));
				} else
					w = new PrintWriter(new FileWriter(dir.getAbsolutePath()+"/default-audit.log"));
			}

			writers.put("__default__", w);
		} catch (Exception e) {

		}
	}



	/**
	 * 
	 */
	public void run(){

		while( !finished ){

			try {
				int count = 0;
				while(! queue.isEmpty()){
					AuditEvent evt = queue.poll();

					if(evt != null){
						writeEvent(evt);
						if( count++ % 100 == 0 )
							System.out.print("w");
						eventCount += 1.0d;
					}
				}

				//
				// being not steady means we finish after writing all events...
				//
				if(! steady)
					finished = true;

				int sleep = 64;
				while(queue.isEmpty() && !finished){

					Thread.sleep( sleep );
					if(sleep < 500)
						sleep *= 2;
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public void writeEvent(AuditEvent evt) throws IOException {
		String host = extractKey( evt );

		if(host == null)
			host = "errors";

		//
		// if there is a port specified in the host-string
		//
		if(host.indexOf(":") > 0)
			host = host.substring(0, host.indexOf(":"));


		if(! hostBasedLogs)
			host = "__default__";
		else {
			if(map.get(host) != null)
				host = map.get(host);
		}


		host = host.toLowerCase();

		PrintWriter wr = writers.get(host);
		if(wr == null){
			wr = new PrintWriter(new FileWriter(logDir.getAbsolutePath()+"/"+host+"-audit.log", true));
			writers.put(host, wr);
		}

		boolean nl = true;
		if( evt.getSection( 0 ).endsWith("\n") )
			nl = false;

		for(int i = 0; i < ModSecurity.SECTIONS.length(); i++){
			char sec = ModSecurity.SECTIONS.charAt(i);
			if(! "".equals(evt.getSection(i))){
				wr.println("--"+evt.getEventId()+"-"+sec+"--");
				if(nl)
					wr.println(evt.getSection(i));
				else
					wr.print(evt.getSection(i));
			}
		}

		wr.println("--"+evt.getEventId()+"-Z--");

		wr.flush();
	}


	private String extractKey( AuditEvent evt ){

		if( "SESSION_ID".equals( this.key) )
			return evt.getSessionId();

		if( ! hostBasedLogs )
			return null;

		return evt.get( ModSecurity.REQUEST_HEADERS + ":Host" );
	}


	/**
	 * 
	 * this will block if the queue is full !
	 * Needs fixing!
	 * 
	 */
	public synchronized void eventArrived(AuditEvent evt){
		if(evt != null)
			queue.add(evt);
	}


	public void add( AuditEvent evt ){
		queue.add( evt );
	}

	public void add( Collection<AuditEvent> evts ){
		queue.addAll( evts );
	}


	public void addEventListener( AuditEventListener listener ){

	}

	/**
	 * Returns the number of events written since the last
	 * call of this method.
	 */
	public double[] getStatistics(){
		double[] data = new double[]{ eventCount };
		eventCount = 0.0d;
		return data;
	}

	public String[] getColumnNames(){
		return new String[]{ "eventCount" };
	}

	public String getFile(){
		return "AuditLogFileWriter";
	}

	/**
	 * This method lets you specify an alias for a webserver so that
	 * events having this alias in their <code>Host:</code>-header will
	 * be written to the same file as the ones with the server-name in
	 * the <code>Host</code>-header.
	 * 
	 * @param alias
	 * @param server
	 */
	public void addAlias(String alias, String server){
		map.put(alias, server);
	}


	public boolean hasEventsPending(){
		return ! queue.isEmpty();
	}

	public int eventsPending(){
		return queue.size();
	}

	public void eventsArrived( Collection<AuditEvent> events ){
	    addEvents( events );
	}
	
	public void addEvents(Collection<AuditEvent> events){
		queue.addAll( events );
	}

	public int length(){
		return queue.size();
	}

	public int left(){
		return queue.size();
	}

	/**
	 * A writer not being steady just writes it queue on disk and exits.
	 * A steady writer will wait for new events and then start writing
	 * again...
	 * 
	 * @param b
	 */
	public void setSteadyWriter(boolean b){
		steady = b;
	}

	public void close(){
		finished = true;
	}
}

