/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventFilter;


/**
 * 
 * This interface defines the methods which all sources of AuditEvent-objects have
 * to implement. Right now this is just like an iterator over AuditEvents.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public interface AuditEventSource
{
    /**
     * This sets a filter on the source. A source should only return events that
     * match this filter or all events, if no filter is set.
     * 
     * To disable filtering filter can be set to <code>null</code>.
     * 
     * @param filter The filter that determines the events which are returned.
     */
    public void setFilter(AuditEventFilter filter);
    
    
    /**
     * Returns the next available AuditEvent. If no event is available <code>null</code>
     * is returned.
     * 
     * @return The next event.
     */
    public AuditEvent nextEvent();
    
    
    /**
     * This method returns true if there is at least one more event pending, so
     * if <code>hasNext</code> returns true, the next call to <code>nextEvent</code>
     * should neither fail nor throw an exception.
     * 
     * @return <code>true</code> iff there is another event available.
     */
    public boolean hasNext();
}
