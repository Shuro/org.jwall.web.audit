package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;

public abstract class IronBeeSectionParser implements Processor {

	static Logger log = LoggerFactory.getLogger(IronBeeSectionParser.class);

	public abstract String getSectionName();

	final Map<String, String> sectionHeader = new LinkedHashMap<String, String>();

	/**
	 * @see stream.data.Processor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data input) {

		Serializable section = input.get(getSectionName());
		if (section == null) {
			return input;
		}

		try {
			sectionHeader.clear();
			BufferedReader reader = new BufferedReader(new StringReader(
					section.toString()));

			String line = reader.readLine();
			while (line != null && !"".equals(line.trim())) {
				log.trace("Parsing section-header: {}", line);
				String[] tok = line.split(": ", 2);
				if (tok.length == 2) {
					log.trace("Adding    ( {}, {} )", tok[0], tok[1]);
					sectionHeader.put(tok[0], tok[1]);
				} else {
					log.warn("Invalid section-header: {}", line);
				}
				line = reader.readLine();
			}

			input = parseSection(reader, input);

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}

		return input;
	}

	public abstract Data parseSection(BufferedReader reader, Data data)
			throws IOException;
}