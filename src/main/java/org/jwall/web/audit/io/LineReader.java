package org.jwall.web.audit.io;

public interface LineReader {

	public void skip( Long bytes );
	
	public Long bytesRead();
	
	public String readLine();
	
	public String getSource();
}
