/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.IOException;


/**
 * 
 * Exceptions of this class are thrown in case of incompatible formats of
 * audit-events or files which is tried to read from.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditFormatException
    extends IOException
{
    /** The serial class ID. */
    private static final long serialVersionUID = -7956489301079227005L;
    
    
    /**
     * This constructor creates a new instance of this class using the given
     * error message. 
     * 
     * @param message A message describing the cause of this exception.
     */
    public AuditFormatException( String message ){
        super( message );
    }
}
