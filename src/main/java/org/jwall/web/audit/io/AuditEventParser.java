/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.session.HttpProtocol;
import org.jwall.web.audit.util.ParserUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventParser {
	/** The unique logger for this class */
	static Logger log = LoggerFactory.getLogger(AuditEventParser.class
			.getName());

	/** the format in which the date is printed out */
	public final static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(
			"d/MMM/y:HH:mm:ss Z", Locale.ENGLISH);

	/*
	 * The following prefixes are used to detect/extract the specific properties
	 * from the audit-log trailer, i.e. the file/line of the rule which fired
	 * this event, etc. This is used when parsing the audit-log trailer.
	 */
	private final static String RULE_FILE_PREFIX = "[file ";
	private final static String RULE_LINE_PREFIX = "[line ";
	private final static String RULE_ID_PREFIX = "[id ";
	private final static String RULE_MSG_PREFIX = "[msg ";
	private final static String RULE_SEVERITY_PREFIX = "[severity ";
	private final static String RULE_TAG_PREFIX = "[tag ";

	/*
	 * This is the list of patterns to extract from the audit-log trailer. The
	 * arrays elements at even positions (0,2,...) specify the prefix for the
	 * extraction. The elements at odd positions (1,3,...) determine the
	 * variable under which the extracted values will be stored within the
	 * resulting event.
	 */
	protected final static String extractTrailer[] = new String[] {
			RULE_FILE_PREFIX, ModSecurity.RULE_FILE, RULE_LINE_PREFIX,
			ModSecurity.RULE_LINE, RULE_ID_PREFIX, ModSecurity.RULE_ID,
			RULE_MSG_PREFIX, ModSecurity.RULE_MSG, RULE_TAG_PREFIX,
			ModSecurity.RULE_TAG, RULE_SEVERITY_PREFIX, ModSecurity.RULE_SEV };

	/**
	 * This method will read a header section of a ModSecurity event and parse
	 * all the http headers. Depending on the format of the first line, the
	 * method will determine whether the header data is a request or response
	 * header and appropriately insert the resulting headers into either the
	 * <code>REQUEST_HEADERS</code> or the <code>RESPONSE_HEADERS</code>
	 * collection.
	 * 
	 * @param cols
	 *            The collection mapping which will be filled during the parsing
	 *            process.
	 * @param header
	 *            The data which actually contains the request-/response-header.
	 * @return The method will simply return the collection mapping for
	 *         convenience purposes.
	 * @throws ParseException
	 *             In case the header could not be parsed correctly an
	 *             appropriate ParseException will be thrown.
	 */
	public static Map<String, List<String>> parseHttpHeader(
			Map<String, List<String>> cols, String header)
			throws ParseException {

		BufferedReader r = new BufferedReader(new StringReader(header));
		Hashtable<String, String> headers = new Hashtable<String, String>();

		try {
			String line = r.readLine();
			String headerCollection = ModSecurity.REQUEST_HEADERS;

			while (line != null && line.equals(""))
				line = r.readLine();

			if (line == null) {
				log.debug("No header found, possbile due to an invalid client request!");
				return cols;
			}

			if (line.startsWith("HTTP")) {

				headerCollection = ModSecurity.RESPONSE_HEADERS;
				addValue(cols, ModSecurity.RESPONSE_LINE, line);

				String d[] = line.split(" ");
				addValue(cols, ModSecurity.RESPONSE_STATUS, d[1]);

			} else {

				addValue(cols, ModSecurity.REQUEST_LINE, line);

				int idx1 = line.indexOf(" ");
				int idx2 = line.lastIndexOf(" ");

				// String[] d = line.split(" ");
				String requestUriRaw = "";

				try {
					requestUriRaw = line.substring(idx1 + 1, idx2);

					addValue(cols, ModSecurity.REQUEST_METHOD,
							line.substring(0, idx1));
					addValue(cols, ModSecurity.REQUEST_URI_RAW, requestUriRaw);

				} catch (Exception e) {
					throw new ParseException(
							"Failed to split the request-line: \"" + line
									+ "\", error is " + e.getMessage());
				}

				if (idx2 > 0) {
					String proto = line.substring(idx2 + 1);
					addValue(cols, ModSecurity.REQUEST_PROTOCOL, proto);
				} else
					addValue(cols, ModSecurity.REQUEST_PROTOCOL, "");

				String queryString = "";

				// According to the ModSecurity docs, the REQUEST_URI does
				// contain the
				// full request-path, including the query-string data
				//
				StringBuffer requestUri = new StringBuffer();

				// check for the query-string separator
				int qsSeparatorIndex = requestUriRaw.indexOf("?");
				int uriStart = 0;

				if (requestUriRaw.toLowerCase().startsWith("http://")) {
					uriStart = requestUriRaw.indexOf("/", "http://".length());
					if (uriStart < 0)
						uriStart = "http://".length();
				}

				if (qsSeparatorIndex > 0) {
					requestUri = requestUri.append(requestUriRaw.substring(
							uriStart, qsSeparatorIndex));
					queryString = requestUriRaw.substring(qsSeparatorIndex + 1);
				} else {
					queryString = "";
					requestUri.append(requestUriRaw);
				}

				addValue(cols, ModSecurity.REQUEST_URI, requestUri.toString());
				addValue(cols, ModSecurity.QUERY_STRING, queryString);
			}

			//
			// Now, parse all the header lines....
			//

			while (r.ready() && line != null) {
				line = r.readLine();
				if (!"".equals(line) && line != null) {
					String[] pairs = line.split(":", 2); // this should fix
															// Headers with
															// Host:www.jwall.org

					if (pairs.length == 2) {
						while (pairs[1].startsWith(" "))
							pairs[1] = pairs[1].substring(1);

						headers.put(pairs[0], pairs[1]);

						addValue(cols, headerCollection + ":" + pairs[0],
								pairs[1]);
						addValue(
								cols,
								headerCollection + ":" + pairs[0].toUpperCase(),
								pairs[1]);
						addValue(cols, headerCollection + "_NAMES", pairs[0]);
						addValue(cols, headerCollection, pairs[1]);

						if ("Cookie".equalsIgnoreCase(pairs[0])
								&& !"".equals(pairs[1])) {
							parseCookies(cols, ModSecurity.REQUEST_COOKIES,
									pairs[1]);
							addValue(cols, ModSecurity.REQUEST_COOKIES,
									pairs[1]);
						}

						if ("Set-Cookie".equalsIgnoreCase(pairs[0])
								&& !"".equals(pairs[1])) {
							parseCookies(cols, ModSecurity.RESPONSE_COOKIES,
									pairs[1]);
							addValue(cols, ModSecurity.RESPONSE_COOKIES,
									pairs[1]);
						}
					} else
						throw new ParseException(
								"invalid response-header-line: " + line
										+ "\nrequest:\n" + header);
				}

			}

		} catch (Exception e) {
			throw new ParseException(e.getMessage());
		}

		return cols;
	}

	private static Map<String, List<String>> parseCookies(
			Map<String, List<String>> cols, String selector, String c) {

		String ct[] = c.split("(;|,)"); // ct now has cookie-tokens:
										// $Version=.., name1=value1, $Path=..,
										// $Domain=.., $Port=..

		for (int i = 0; i < ct.length; i++) {
			ct[i] = ct[i].trim();

			if (ct[i].matches("\\w*=.*")) {
				String[] av = ct[i].split("=");
				if (av.length == 2) {
					addValue(cols, selector + ":" + av[0], av[1]);
					addValue(cols, selector + "_NAMES", av[0]);
				}
			}
		}
		return cols;
	}

	/**
	 * 
	 * This method parses all parameters (get and post) and fills the appopriate
	 * hashes. It does not implicitly apply any codec (URLdeocde, etc) !!
	 * 
	 */
	public static void parseParameters(Map<String, List<String>> cols,
			String selector, String qs) {

		if (qs != ModSecurity.REQUEST_BODY) {

			Map<String, String> params = ParserUtils.parseQueryString(qs);
			for (String key : params.keySet()) {
				String val = params.get(key);
				addValue(cols, ModSecurity.ARGS_NAMES, key);
				addValue(cols, ModSecurity.ARGS, val);
				addValue(cols, ModSecurity.ARGS_GET, val);
				addValue(cols, ModSecurity.ARGS + ":" + key, val);
			}

			return;
		}

		if (cols.get(ModSecurity.REQUEST_BODY) == null)
			return;

		String body = cols.get(ModSecurity.REQUEST_BODY).get(0);

		if (!"".equals(body.trim())) {
			String ct = cols.get(ModSecurity.REQUEST_HEADERS + ":Content-Type")
					.get(0);

			if (ct != null && ct.indexOf("multipart/form-data") > 0) {

				try {

					String contentType = cols.get(
							HttpProtocol.REQUEST_HEADER_CONTENT_TYPE).get(0);
					addValue(cols, "SECTION_FORM_DATA",
							ParserUtils.extractFormData(contentType, body));

				} catch (Exception e) {
					e.printStackTrace();
				}

				Map<String, String> postParams = ParserUtils
						.parseQueryString(body);
				for (String key : postParams.keySet()) {
					String val = postParams.get(key);

					addValue(cols, ModSecurity.ARGS + ":" + key, val);
					addValue(cols, ModSecurity.ARGS_POST + ":" + key, val);
					addValue(cols, ModSecurity.ARGS_NAMES, key);
					addValue(cols, ModSecurity.ARGS_POST_NAMES, key);
				}
			}
		}
	}

	/**
	 * 
	 * 
	 * @param cols
	 * @param trailer
	 * @return
	 * @throws ParseException
	 */
	public static Map<String, List<String>> parseAuditTrailer(
			Map<String, List<String>> cols, String trailer)
			throws ParseException {
		return cols;
	}

	public static void addValue(Map<String, List<String>> cols, String col,
			String value) {

		List<String> collection = cols.get(col);
		if (collection == null) {
			collection = new LinkedList<String>();
			cols.put(col, collection);
		}
		collection.add(value);
	}

	public static void setValue(Map<String, List<String>> cols, String key,
			String value) {

		List<String> list = cols.get(key);
		if (list == null) {
			list = new LinkedList<String>();
			list.add(value);
			cols.put(key, list);
		} else {
			list.clear();
			list.add(value);
		}
	}

	/**
	 * Parse date from AuditTrailer-Section
	 */
	public static Date parseDate(String s) {
		Date d;
		int idx = 0;
		String str = "";
		try {
			idx = s.indexOf("] ");
			str = s.substring(1, idx);
			d = DATE_FORMAT.parse(str);
		} catch (Exception e) {
			//
			// This tries to catch a ModSecurity bug, which causes ModSecurity
			// to
			// produce a time-zone string with an extra leading '-' on negative
			// GMT offsets
			//
			try {
				str = str.replaceAll("--", "-");
				log.trace("Trying to parse date from '{}'", str);
				d = DATE_FORMAT.parse(str);
			} catch (Exception e1) {
				d = new Date();
				log.trace("string is: {}", str);
			}
		}
		return d;
	}

	/**
	 * 
	 * @param collections
	 * @param auditLogHeader
	 * @return
	 */
	public static Map<String, List<String>> parseAuditLogHeader(
			Map<String, List<String>> collections, String auditLogHeader)
			throws ParseException {
		//
		// extract remote-addr, remote-port, server-addr, server-port from
		// AuditLog-Header
		// 0 1 2 3 4 5 6
		// s[] = [date, TZ], ID, Client-IP, Client-Port, Server-IP, Server-Port
		String[] s = auditLogHeader.trim().split(" ");

		if (s.length < 7)
			throw new ParseException(
					"Unexpected number of columns in AUDIT_LOG_HEADER! Columns found:"
							+ s.length + ", expected: 7!");

		Date createdAt = parseDate(s[0]);

		addValue(collections, ModSecurity.EVENT_ID,
				Long.toHexString(createdAt.getTime()));
		addValue(collections, ModSecurity.UNIQUE_ID, s[2]);
		addValue(collections, ModSecurity.TX_ID, s[2]);
		addValue(collections, ModSecurity.REMOTE_ADDR, s[3]);
		addValue(collections, ModSecurity.REMOTE_HOST, s[3]);
		addValue(collections, ModSecurity.REMOTE_PORT, s[4]);
		addValue(collections, ModSecurity.SERVER_ADDR, s[5]);
		addValue(collections, ModSecurity.SERVER_NAME, s[5]);
		if (s[6].equals("www"))
			s[6] = "80";

		addValue(collections, ModSecurity.SERVER_PORT, s[6]);
		addValue(collections, ModSecurity.REMOTE_USER, "-");

		return collections;
	}

	public static Map<String, String> parseMetaInfSection(
			Map<String, List<String>> collections, String metaInfSection)
			throws ParseException {
		Map<String, String> variables = readMetaInf(metaInfSection);
		for (String key : variables.keySet()) {
			addValue(collections, key, variables.get(key));
		}

		return variables;
	}

	public static Map<String, String> readMetaInf(String metaInfSection)
			throws ParseException {
		Map<String, String> variables = new LinkedHashMap<String, String>();
		try {
			BufferedReader r = new BufferedReader(new StringReader(
					metaInfSection));
			String line = r.readLine();
			while (line != null) {
				int idx = line.indexOf("=");
				if (idx > 0) {
					String key = line.substring(0, idx);
					String val = line.substring(idx + 1);
					variables.put(key, val);
				}
				line = r.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return variables;
	}

	public static String storeMetaInf(Map<String, String> data) {
		StringBuffer s = new StringBuffer();
		for (String key : data.keySet()) {
			s.append(key);
			s.append("=");
			s.append(data.get(key));
			s.append("\n");
		}
		return s.toString();
	}
}