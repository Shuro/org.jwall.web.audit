package org.jwall.web.audit.io;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.jwall.web.audit.AuditData;
import org.jwall.web.audit.AuditEvent;

import stream.Data;
import stream.io.AbstractStream;

/**
 * This class implements a data stream in the context of the streams library.
 * The stream parses ModSecurity audit-log data.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ModSecurityAuditStream extends AbstractStream {

	URL url;
	InputStream input;
	ModSecurity2AuditReader reader;

	public ModSecurityAuditStream(URL url) throws IOException {
		this(url.openStream());
		this.url = url;
	}

	public ModSecurityAuditStream(InputStream in) throws IOException {
		input = in;
		reader = new ModSecurity2AuditReader(input);
	}

	/**
	 * @see stream.io.DataStream#close()
	 */
	@Override
	public void close() throws Exception {
		reader.close();
	}

	/**
	 * @see stream.io.AbstractDataStream#read()
	 */
	@Override
	public Data readNext() throws Exception {

		AuditEvent evt = reader.readNext();
		if (evt == null)
			return null;

		return new AuditData(evt);
	}
}
