package org.jwall.web.audit.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.james.mime4j.MimeException;
import org.apache.james.mime4j.parser.ContentHandler;
import org.apache.james.mime4j.parser.MimeStreamParser;
import org.apache.james.mime4j.stream.BodyDescriptor;
import org.apache.james.mime4j.stream.Field;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.IronBeeAuditEvent;
import org.jwall.web.audit.util.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.ProcessContext;
import stream.Processor;
import stream.StatefulProcessor;
import stream.data.DataFactory;
import stream.runtime.ProcessContextImpl;

public class IronBeeAuditReader implements AuditEventReader {
	static Logger log = LoggerFactory.getLogger(IronBeeAuditReader.class);
	BufferedInputStream inputStream;

	final IBEventHandler handler = new IBEventHandler();
	final MimeStreamParser stream = new MimeStreamParser();
	List<Processor> processors = new ArrayList<Processor>();

	public IronBeeAuditReader(InputStream in) throws IOException {
		inputStream = new BufferedInputStream(in);

		processors.add(new IronBeeHttpHeaderParser("http-request-headers",
				"HTTP_REQUEST_HEADERS", "REQUEST_LINE"));

		processors.add(new IronBeeHttpHeaderParser("http-response-headers",
				"HTTP_RESPONSE_HEADERS", "RESPONSE_LINE"));

		processors.add(new IronBeeJSONProcessor());
		processors.add(new Timestamp("DATE", "yyyy-mm-dd'T'hh:mm:ss.SZ",
				"REQUEST_TIMESTAMP"));
		processors.add(new Timestamp("RECEIVED", "yyyy-mm-dd'T'hh:mm:ss.SZ",
				"LOG_TIMESTAMP"));

		final ProcessContext ctx = new ProcessContextImpl();

		for (Processor p : processors) {
			try {
				if (p instanceof StatefulProcessor)
					((StatefulProcessor) p).init(ctx);
			} catch (Exception e) {
				throw new IOException(
						"Failed to initialize processor for IronBeeAuditReader: "
								+ e.getMessage());
			}
		}

		stream.setContentHandler(handler);
	}

	@Override
	public Iterator<AuditEvent> iterator() {
		return null;
	}

	@Override
	public AuditEvent readNext() throws IOException, ParseException {

		try {

			handler.reset();
			stream.parse(inputStream);

			Data data = handler.getItem();
			for (Processor p : processors) {
				data = p.process(data);
			}
			log.debug("Data parsed is:\n{}", data);
			data.put(IronBeeAuditEvent.SECTION_BOUNDARY_KEY, handler.boundary);
			return new IronBeeAuditEvent(data);

		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
	}

	@Override
	public void close() throws IOException {
		inputStream.close();
	}

	@Override
	public boolean atEOF() {
		return false;
	}

	@Override
	public long bytesRead() {
		return 0;
	}

	@Override
	public long bytesAvailable() {
		// TODO Auto-generated method stub
		return 0;
	}

	public class IBEventHandler implements ContentHandler {

		Logger log = LoggerFactory.getLogger(IBEventHandler.class);
		String name = null;
		Data item = DataFactory.create();
		String boundary = "";
		Map<String, String> sections = new LinkedHashMap<String, String>();
		StringBuffer currentSection = new StringBuffer();

		public void reset() {
			item = DataFactory.create();
		}

		@Override
		public void startMessage() throws MimeException {
			log.debug("startMessage()");
		}

		@Override
		public void endMessage() throws MimeException {
			log.debug("endMessage()");
		}

		@Override
		public void startBodyPart() throws MimeException {
			log.debug("startBodyPart()");
		}

		@Override
		public void endBodyPart() throws MimeException {
			log.debug("endBodyPart()");
			if (name != null)
				sections.put(name, currentSection.toString());
		}

		@Override
		public void startHeader() throws MimeException {
			log.debug("startHeader()");
			name = null;
			currentSection = new StringBuffer();
		}

		@Override
		public void field(Field rawField) throws MimeException {
			log.debug("field( {} )", rawField);

			currentSection.append(rawField.getName() + ": "
					+ rawField.getBody());
			currentSection.append("\r\n");

			if ("Content-Disposition".equals(rawField.getName())) {

				String line = rawField.getBody();
				int idx = line.indexOf("name=\"");
				if (idx > 0) {
					idx += "name=\"".length();
					int end = line.indexOf("\"", idx);
					if (end > 0) {
						name = line.substring(idx, end);
						log.debug("Diving into section '{}'", name);
					}
				}
			}

			if ("Content-Type".equals(rawField.getName())) {
				int idx = rawField.getBody().indexOf("boundary=");
				if (idx > 0) {
					boundary = rawField.getBody().substring(
							idx + "boundary=".length());
					log.debug("Boundary is '{}'", boundary);
				}
			}
		}

		@Override
		public void endHeader() throws MimeException {
			log.debug("endHeader()");
			currentSection.append("\r\n");
		}

		@Override
		public void preamble(InputStream is) throws MimeException, IOException {
			log.debug("preamble(...)");
		}

		@Override
		public void epilogue(InputStream is) throws MimeException, IOException {
			log.debug("epilogue(...)");
		}

		@Override
		public void startMultipart(BodyDescriptor bd) throws MimeException {
			log.debug("startMultiPart( {} )", bd);
		}

		@Override
		public void endMultipart() throws MimeException {
			log.debug("endMultiPart()");
		}

		@Override
		public void body(BodyDescriptor bd, InputStream is)
				throws MimeException, IOException {

			if (name == null) {
				log.warn("No section name found, skipping section!");
			}
			log.debug("section: '{}'", name);
			log.debug("body( {} )", bd);
			log.debug("subType: {}", bd.getSubType());
			log.debug("body-length is {} bytes", bd.getContentLength());

			BufferedReader reader = new BufferedReader(
					new InputStreamReader(is));
			String line = reader.readLine();
			while (line != null) {
				currentSection.append(line + "\r\n");
				line = reader.readLine();
			}
			item.put("SECTION:" + name, currentSection.toString());

			/*
			 * if ("json".equalsIgnoreCase(bd.getSubType())) {
			 * 
			 * log.debug("Handling JSON content, parts is named {}"); JSONParser
			 * p = new JSONParser( JSONParser.DEFAULT_PERMISSIVE_MODE);
			 * 
			 * try { JSONObject object = p.parse( new
			 * StringReader(currentSection.toString()), JSONObject.class);
			 * log.info("object: {}", object);
			 * 
			 * if (object instanceof Map) { for (String key : object.keySet()) {
			 * Object value = object.get(key); if (value instanceof
			 * Serializable) item.put(IronBeeAuditEvent.mapVariable(key),
			 * (Serializable) value); else
			 * item.put(IronBeeAuditEvent.mapVariable(key),
			 * object.get(key).toString()); } } return; } catch (Exception e) {
			 * log.error("Failed to parse JSON data: {}", e.getMessage()); } }
			 */

			StringBuffer s = new StringBuffer();
			BufferedReader r = new BufferedReader(new StringReader(
					currentSection.toString()));
			line = r.readLine();
			while (line != null) {
				s.append(line + "\r\n");
				log.debug("   body.line: {}", line);
				line = r.readLine();
			}
			r.close();
			log.debug("/body()");
		}

		@Override
		public void raw(InputStream is) throws MimeException, IOException {
			log.debug("raw( ... )");
		}

		public Data getItem() {

			for (String sect : sections.keySet()) {
				item.put(sect, sections.get(sect));
			}

			return item;
		}
	}
}
