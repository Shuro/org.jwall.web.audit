package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class IronBeeHttpHeaderParser extends IronBeeSectionParser {

	static Logger log = LoggerFactory.getLogger(IronBeeHttpHeaderParser.class);
	String sectionName;
	String collectionName;
	String firstLine;

	public IronBeeHttpHeaderParser(String sectionName, String collectionName,
			String firstLineName) {
		this.sectionName = sectionName;
		this.collectionName = collectionName;
		this.firstLine = firstLineName;
	}

	@Override
	public String getSectionName() {
		return sectionName;
	}

	@Override
	public Data parseSection(BufferedReader reader, Data data)
			throws IOException {

		String line = reader.readLine();
		if (line != null) {
			log.trace("Adding   ( {}, {} )", firstLine, line);
			data.put(firstLine, line);
		}

		line = reader.readLine();
		while (line != null) {

			int idx = line.indexOf(": ");
			if (idx > 0) {

				String header = line.substring(0, idx);
				String value = line.substring(idx + 2);
				String key = collectionName + ":" + header.toUpperCase();
				data.put(key, value);
				log.trace("Adding   ( {}, {} )", key, value);
			}

			line = reader.readLine();
		}

		return data;
	}
}
