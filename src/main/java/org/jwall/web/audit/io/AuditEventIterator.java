/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.util.Iterator;

import org.jwall.web.audit.AuditEvent;


/**
 * Creates a new Iterator for walking over a set of AuditEvent objects. The source
 * of these objects can be any AuditEventReader implementation.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditEventIterator
    implements Iterator<AuditEvent>
{

    AuditEvent next;
    
    AuditEventReader reader;
    
    public AuditEventIterator( AuditEventReader reader ) throws Exception {
        this.reader = reader;
        next = reader.readNext();
    }

    
    /**
     * @see java.util.Iterator#hasNext()
     */
    @Override
    public boolean hasNext()
    {
        return next != null;
    }

    
    /**
     * @see java.util.Iterator#next()
     */
    @Override
    public AuditEvent next()
    {
        AuditEvent event = next;
        try {
            next = reader.readNext();
        } catch (Exception e ){
            next = null;
        }
        return event;
    }

    @Override
    public void remove()
    {
    }
}
