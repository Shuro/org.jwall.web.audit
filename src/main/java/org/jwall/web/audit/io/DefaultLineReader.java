package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class DefaultLineReader implements LineReader {

	BufferedReader reader;
	Long bytesRead = 0L;
	
	public DefaultLineReader( InputStream in ){
		reader = new BufferedReader( new InputStreamReader( in ) );
	}
	
	
	@Override
	public String readLine() {
		try {
			String line = reader.readLine();
			bytesRead += line.getBytes().length;
			return line;
		} catch (Exception e) {
			return null;
		}
	}


	@Override
	public String getSource() {
		return "input-stream";
	}


	@Override
	public void skip(Long bytes) {
		try {
			Long skipped = reader.skip( bytes );
			bytesRead += skipped;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@Override
	public Long bytesRead() {
		return bytesRead;
	}
}