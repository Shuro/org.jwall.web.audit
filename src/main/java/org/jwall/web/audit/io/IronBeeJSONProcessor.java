/**
 * 
 */
package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.Serializable;
import java.io.StringReader;
import java.util.Map;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.IronBeeEventMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public class IronBeeJSONProcessor extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(IronBeeJSONProcessor.class);
	String[] keys = new String[] { "events", "http-request-metadata",
			"http-response-metadata" };

	/**
	 * @see stream.data.DataProcessor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data data) {

		for (String section : keys) {

			Serializable content = data.get(section);
			String contentType = "";

			try {

				BufferedReader reader = new BufferedReader(new StringReader(
						content.toString()));

				String line = reader.readLine();
				while (line != null && !line.trim().isEmpty()) {
					if (line.toLowerCase().startsWith("content-type: "))
						contentType = line.substring("content-type: ".length());
					line = reader.readLine();
				}

				if ("application/json".equalsIgnoreCase(contentType)) {

					log.debug("Handling JSON content, parts is named {}");
					JSONParser p = new JSONParser(
							JSONParser.DEFAULT_PERMISSIVE_MODE);

					JSONObject object = p.parse(reader, JSONObject.class);
					log.debug("object: {}", object);

					if (object instanceof Map) {
						for (String key : object.keySet()) {

							if (key.equalsIgnoreCase("events")) {

								JSONArray evts = (JSONArray) object.get(key);
								AuditEventMessage[] msgs = new AuditEventMessage[evts
										.size()];

								for (int i = 0; i < evts.size(); i++) {

									IronBeeEventMessage ib = new IronBeeEventMessage();

									log.debug("events[{}] = {}", i, evts.get(i));

									JSONObject event = (JSONObject) evts.get(i);
									log.debug("  message: {}", event);
									for (String k : event.keySet()) {
										log.debug("     message[{}] = {}", k,
												event.get(k));

										Object obj = event.get(k);
										if (obj instanceof JSONArray) {
											JSONArray arr = (JSONArray) obj;
											String[] d = new String[arr.size()];
											for (int idx = 0; idx < arr.size(); idx++) {
												d[idx] = arr.get(idx)
														.toString();
											}
											log.debug("Setting {} = {}", k, d);
											ib.set(k, d);
										} else {
											ib.set(k, event.get(k).toString());
										}
									}
									msgs[i] = ib;
								}

								data.put("event_messages", msgs);
							} else {
								log.debug("Processing section {}", key);
								Object value = object.get(key);
								if (value instanceof Serializable)
									data.put(key, (Serializable) value);
								else
									data.put(key, object.get(key).toString());
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				log.error("Failed to parse JSON data in section '{}': {}",
						section, e.getMessage());
			}
		}

		return data;
	}
}