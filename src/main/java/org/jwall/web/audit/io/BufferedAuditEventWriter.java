/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.LinkedBlockingQueue;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;

/**
 * 
 * This class implements a simple persistency thread. It is created by specifying
 * an AuditEventWriter implementation used for writing the events to disk. Upon
 * startup, it waits for new events to arrive and writes them to disk asynchronously.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class BufferedAuditEventWriter 
extends Thread 
implements AuditEventWriter, AuditEventListener
{

	/** The writer-instance, used to make events persistent */
	private AuditEventWriter writer;
	
	/** The list of events, waiting to be written */
	private LinkedBlockingQueue<AuditEvent> queue;
	
	/** A flag for signaling this writer to finish */
	private boolean finished = false;
	
	
	/**
	 * 
	 * This creates a new buffered event-writer that uses the given 
	 * event-writer implementation to write audit-events to disk.
	 * 
	 * 
	 * @param wr An instance that implements the AuditEventWriter interface.
	 * 
	 */
	public BufferedAuditEventWriter( AuditEventWriter wr ){
		queue = new LinkedBlockingQueue<AuditEvent>();
		writer = wr;
	}
	
	
	/**
	 * 
	 * This method loops until the finished-method has been called. It checks
	 * for new events and writes them to disk.
	 * 
	 * @see java.lang.Thread#run()
	 */
	public void run(){
		
		while( ! finished  ){
			
			while( ! queue.isEmpty() ){
				
				try {
					AuditEvent evt = queue.poll();
					writer.writeEvent( evt );
				} catch ( Exception e ){
					e.printStackTrace();
				}
			}
			
			try {
				Thread.sleep( 256 );
			} catch (InterruptedException ie) { }
			
		}
	}

	
	/**
	 * Add a new event to the list of events that need to be written to disk. Unless
	 * the writer is in <i>finished</i>-state, this will enqueue the event to the end
	 * of the list.
	 * <br/>
	 * <b>NOTE:</b> The internal queue-length might grow to a size of Integer.MAX_VALUE
	 * events. If this maximum size is reached, a call to this method might block.
	 * 
	 * @param evt The event to be written.
	 */
	public void add( AuditEvent evt ){
		
		if( evt != null && !finished )
			queue.add( evt );
		
	}
	
	
	/**
	 * 
	 * This method can be used to add a collection of events to the queue. As with the
	 * <code>add(AuditEvent evt)</code>-method this might block if the maximum size of
	 * the internal queue is reached.
	 * 
	 * @param evts A collection of events that are to be written.
	 * 
	 */
    public void addAll( Collection<AuditEvent> evts ){
        queue.addAll( evts );
    }

	
	
	/**
	 * 
	 * This method sets the finish-signal for this writer instance. After
	 * calling this method, new events will not be accepted and the thread
	 * finishes after having written all currently pending events to disk.
	 *
	 */
	public void close(){
		
		finished = true;
		
	}
	
	
	/**
	 * This method can be used to check if any events are pending to be
	 * written to disk by the underlying writer. 
	 * 
	 * @return <code>true</code>, iff the event-queue is not empty.
	 */
    public boolean hasEventsPending(){
    	return ! queue.isEmpty();
    }


    /**
     * 
     * This method simply enqueue the event <code>evt</code> to the list
     * of events waiting to be written to disk.
     * 
     */
	public void writeEvent(AuditEvent evt) throws IOException {
		add( evt );
	}
	
	public void eventArrived( AuditEvent evt ){
		add( evt );
	}
	
	public void eventsArrived( Collection<AuditEvent> events ){
	    this.addAll( events );
	}
}
