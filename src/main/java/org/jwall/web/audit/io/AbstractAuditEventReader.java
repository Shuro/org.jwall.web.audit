/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventFactory;
import org.jwall.web.audit.DefaultAuditEventFactory;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This abstract class implements the basic methods of an AuditEventReader. Implementations of
 * the AuditEventReader interface may inherit this class to ease implementation. The only
 * method that needs to be implemented is the <code>readNext()</code> method, which parses an
 * audit event object from the underlying file/stream. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public abstract class AbstractAuditEventReader
    implements AuditEventReader
{
    public final static String DEFAULT_AUDIT_EVENT_FACTORY_CLASS = DefaultAuditEventFactory.class.getName();
    
    /** A unique logger for this class */
    private static Logger log = LoggerFactory.getLogger( AbstractAuditEventReader.class );
    
    /** The reader which is used for reading */
    protected BufferedReader reader;

    /** The factory used for creating audit-events */
    protected AuditEventFactory eventFactory = null;
    
    /** The file to read from */
    protected File inputFile = null;
    
    /** A counter of all events read by this reader */
    protected int counter = 0;
    
    /** The size of the file which this reader reads from */
    protected double fileSize = 0.0d;
    
    /** The number of bytes read so far (this is of type double to allow an efficient conversion to the fraction value) */
    protected double bytesRead = 0.0;

    /** The currently pending event */
    protected AuditEvent pending = null;
    
    /** A flag indicating if the end of file/stream has been reached */
    protected boolean eofReached = false;
    
    /**
     * This constructor creates a new AbstractAuditEventReader by initializing the
     * basic reader instance to read from the given stream.
     * 
     */
    public AbstractAuditEventReader( InputStream in )
    {
        this( new InputStreamReader( in ) );
    }

    
    /**
     * This constructor creates a new AbstractAuditEventReader by initializing the
     * basic reader instance to read from the given stream.
     * 
     */
    public AbstractAuditEventReader( Reader in )
    {
        
        try {
            
            String factoryClassName = System.getProperty( AuditEventFactory.AUDIT_EVENT_FACTORY_CLASS );
            
            if( factoryClassName != null ){
                Class<?> factory = Class.forName( factoryClassName );
                AuditEventFactory factoryClass = (AuditEventFactory) factory.newInstance();
                if( factoryClass != null ){
                    eventFactory = factoryClass;
                    //System.out.println( "Using event-factory class \"" + eventFactory.getClass().getName() + "\"" );
                }
            }

            
        } catch (Exception e){
            e.printStackTrace();
            eventFactory = null;
        }
        
        if( eventFactory == null )
            eventFactory = DefaultAuditEventFactory.getInstance();
        
        reader = new BufferedReader( in );
    }

    
    /**
     * This method returns the index of the section, that the given line maps to.
     * 
     * @param line A section boundary line.
     * @return The index of the section, to which this line belongs.
     */
    public static int getSectionIndex(String line)
    {
        // line has to be a section-separator-line
        //
        if(! (line.matches("--[\\-0-9A-Za-z\\@]*-[A-Z]--") ) )
            return -1;

        char sect = line.charAt( line.length() - 3 );
    
        if( log.isDebugEnabled() )
            log.debug("looking up index for section >{}< (line: {}", String.valueOf( sect ), line );
    
        for(int i = 0; i < ModSecurity.SECTIONS.length(); i++){
            if( sect == ModSecurity.SECTIONS.charAt( i ) )
                return i;
        }
    
        return -1;
    }

    /**
     * Returns the number of bytes that are available in the file.
     * This method returns <code>-1.0</code> if the reader is reading
     * from a stream.
     * 
     * @return The size of the file from which this reader reads.
     */
    public double getFileSize()
    {
        return fileSize;
    }

    
    /**
     * Returns the number of bytes that have been read. This can be
     * used to display the progress of the reader-process.
     * 
     * @return Number of bytes that have been read.
     */
    public double getDataRead()
    {
        return bytesRead;
    }
    

    /**
     * @see org.jwall.web.audit.io.AuditEventReader#readNext()
     */
    public abstract AuditEvent readNext() throws IOException, ParseException;

    
    /**
     * @return <code>true</code>, if there is another event pending. 
     * @deprecated
     */
    public boolean hasNext()
    {
        return pending != null && !eofReached;
    }

    
    /**
     * @see org.jwall.web.audit.io.AuditEventReader#close()
     */
    public void close() throws IOException
    {
    	reader.close();
    }

    
    /**
     * @see org.jwall.web.audit.io.AuditEventReader#bytesRead()
     */
    public long bytesRead()
    {
    	return (long) bytesRead;
    }

    
    /**
     * @see org.jwall.web.audit.io.AuditEventReader#bytesAvailable()
     */
    public long bytesAvailable()
    {
    	if( fileSize >= 0 )
    		return (long) fileSize;
    	
    	return Long.MAX_VALUE;
    }
    
    
    public boolean atEOF(){
        return eofReached;
    }
}
