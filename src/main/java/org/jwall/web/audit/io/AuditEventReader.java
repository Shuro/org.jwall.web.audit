/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.IOException;

import org.jwall.web.audit.AuditEvent;


/**
 * 
 * This interface defines the methods of an abstract event-reader. All
 * readers must implement the method <code>readNext()</code> which is
 * used to read the next event from the underlying source (i.e. file).
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *  
 */
public interface AuditEventReader
    extends Iterable<AuditEvent>
{
	
	/**
	 * This method tries to read the next event that is available. 
	 * 
	 * @return The next audit-event from or <code>NULL</code> if no event is available.
	 */
	public AuditEvent readNext() throws IOException, ParseException;
	
	public void close() throws IOException;
	
	public boolean atEOF();
	
	public long bytesRead();
	
	public long bytesAvailable();
}
