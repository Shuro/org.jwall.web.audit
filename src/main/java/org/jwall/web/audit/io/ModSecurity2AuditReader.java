/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Iterator;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * This class implements a parser for audit-logfile for the <code>modsecurity2</code>. It
 * also implements the <code>AuditEventSource</code> interface.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ModSecurity2AuditReader extends AbstractAuditEventReader
implements AuditEventReader
{
    /** A unique logger for this class */
    static Logger log = LoggerFactory.getLogger( ModSecurity2AuditReader.class );
    
    /** A flag indicating whether this reader should poll new events after hitting EOF */
    boolean tail;
    
    
    /**
     * This constructor creates an audit-event-Reader that reads from the
     * given input stream.
     * 
     * @param in The stream to read from.
     * @throws IOException In case the stream cannot be opened or another IO error occurs.
     */
    public ModSecurity2AuditReader( InputStream in )
    throws IOException
    {
    	this( in, true );
    }

    
    /**
     * This constructor creates an audit-event-Reader that reads from the
     * given input stream.
     * 
     * @param in The stream to read from.
     * @throws IOException In case the stream cannot be opened or another IO error occurs.
     */
    public ModSecurity2AuditReader( InputStream in, boolean persist )
    throws IOException
    {
        super( in );
        this.tail = persist;
    }


    /**
     * This constructor creates a ModSecurity2-AuditReader with a file
     * as source.
     * 
     * @param file The file to read from.
     * @param tail Skip the whole file at first and read only events that
     *             are subsequently appended.
     * @throws IOException In case an IO error occurs.
     */
    public ModSecurity2AuditReader(File  file, boolean tail)
    throws IOException
    {
        this( new FileInputStream(file) );
        inputFile = file;
        fileSize =  (double) file.length();
        this.tail = tail;
        if( this.tail )
            reader.skip( file.length() );
        
    }

    
    /**
     * This simple constructor creates a reader which will simply read
     * events from the given file without listening for newly appended
     * events. 
     * 
     * @param file The file to read events from.
     * @throws IOException In case an IO error occurs.
     * @throws Exception In case of a parsing error.
     */
    public ModSecurity2AuditReader( File file )
    	throws IOException
    {
    	this( file, false );
    }

    
    public ModSecurity2AuditReader( Reader inputReader )
        throws IOException
    {
        super( inputReader );
        tail = true;
    }
    
    /**
     * @see org.jwall.web.audit.io.AuditEventReader#readNext()
     */
    public AuditEvent readNext() throws IOException, ParseException
    {
        log.debug( "readEvent " + counter );
        
        counter++;
        
        String line = "";  // this holds the current line to be worked on
        long offset = (long) getDataRead();
        
        try {
            reader.mark( 1024 * 1024 );
        } catch (Exception e) {
            log.error( "Failed to set marker at offset {}: {}", offset, e );
            return null;
        }
        
        // this array of strings is filled with the appropriate section-strings
        // stringbuffers are needed because the sections are read one line at a time
        StringBuffer[] sections = new StringBuffer[ModSecurity.SECTIONS.length()];  // at maximum 26 sections are possible;
        for(int i = 0; i < sections.length; i++)
            sections[i] = new StringBuffer();
    
        // ptr always points to the stringbuffer which is to be read
        int ptr = 0;
    
        try {
            // 
            // we skip lines until we found an audit-start-line, which has the form "--[0-9a-f]*-A--"
            // if the reader hits EOF => line is null and an exception will be thrown, catched below and null is returned 
            //
            while(reader.ready() && (! line.matches("--[\\-\\@0-9A-Za-z]*-A--"))){
                try {
                    line = reader.readLine();
                } catch ( java.io.EOFException eof ){
                    eofReached = !tail;
                }
                if( line == null ){
                    reader.reset();
                    return null;
                }
                
                bytesRead += line.length() + 1.0d;
            }            
            
            String id = line.replaceFirst("--", "").replaceAll("-A--", "");
            
            //
            // ok, now line points to the beginning-separator-line of the audit event.
            //
            while(!(line.startsWith("--") && line.endsWith("-Z--"))){
                log.debug( "line: {}", line ); // this is the section-beginning-marker
                
                ptr = getSectionIndex(line);
    
                line = reader.readLine();  // now line is the first line of the sections body
                if( line == null ){
                    if( reader.markSupported() )
                        reader.reset();
                    return null;
                }
                
                bytesRead += line.length() + 1.0d;
    
                //
                // if ptr is -1 an invalid section-name was found !
                //
                if(ptr >= 0){
                    //
                    // no we read a section, until a new one begins
                    //
                    sections[ptr] = new StringBuffer();
                    do {
                        sections[ptr].append(line+"\n");
                        line = reader.readLine();
                        
                        //
                        // in case the audit-log entry is not completely written yet
                        //
                        if( line == null ){
                            reader.reset();
                            return null;
                        }
                        
                        bytesRead += line.length();
                        
                        log.debug("line: {}", line);
                        
                    } while(!( line.trim().matches("^--.*-[A-Z]--$") ) );
                } else {
                    //
                    // invalid section-name
                    //
                    log.debug( "Line contains invalid section-name: " + line );
                }
            }
    
            //
            // We need to convert the stringBuffer-array into a string-array
            // as the AuditEvent-constructor expects a string-array
            //
            String[] data = new String[ModSecurity.SECTIONS.length()];
            for(int i = 0; i < data.length; i++)
                data[i] = sections[i].toString();
    
            //
            // the A-section is empty - it seems that the EOF has been reached
            //
            if(data[0].equals(""))
                return null;
            
            return eventFactory.createAuditEvent( id, data, inputFile, offset, (long) bytesRead - offset, AuditEventType.ModSecurity2 );
            
        } catch ( EOFException eof ) {
            
            eofReached = true;
            return null;
            
        } catch ( Exception e ) {
            e.printStackTrace();
            return null;
            
        }
    }
    


    @Override
    public Iterator<AuditEvent> iterator()
    {
        try {
            return new AuditEventIterator( this );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
