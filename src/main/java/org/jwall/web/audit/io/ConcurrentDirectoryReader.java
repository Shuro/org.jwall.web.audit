package org.jwall.web.audit.io;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This is a simple directory reader which recursively scans all files from a specified directory
 * and tries to parse audit-events from them.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ConcurrentDirectoryReader
    implements AuditEventReader
{
    /* A global logger for this class */
    static Logger log = LoggerFactory.getLogger( ConcurrentDirectoryReader.class );

    /** The target directory from which to read */
    File dir;
    
    /** The list of files found in thae target directory */
    LinkedList<File> files = new LinkedList<File>();

    /** The total number of FILES that are to be read */
    Integer total = 0;

    /** The number of FILES that have been read */
    Integer read = 0;

    
    /** The reader of the current file */
    ModSecurity2AuditReader reader;
    

    /**
     * Create a new directory reader, which reads files from the specified directory.
     * 
     * @param directory
     * @throws IOException
     */
    public ConcurrentDirectoryReader( File directory ) throws IOException {

        dir = directory;
        if( !dir.isDirectory() )
            throw new IOException( "Specified file '" + directory.getAbsolutePath() + "' is not a directory!" );

        if( !dir.canRead() )
            throw new IOException( "Cannot open directory '" + dir.getAbsolutePath() + "' for reading!" );

        files.addAll( findFiles( dir ) );
        total = files.size();
        log.debug( "Found {} files...", total );
        read = 0;
        
        reader = new ModSecurity2AuditReader( files.removeFirst() );
    }



    protected List<File> findFiles( File directory ){

        log.debug( "Adding files from directory '{}'", directory );

        List<File> files = new LinkedList<File>();
        for( File file : directory.listFiles() ){
            if( file.isFile() && file.canRead() ){
                log.debug( "  Adding file '{}'", file );
                files.add( file );
            } else {
                files.addAll( findFiles( file ) );
            }
        }

        return files;
    }



    public boolean atEOF()
    {
        return files.isEmpty();
    }

    public long bytesAvailable()
    {
        return total;
    }

    public long bytesRead()
    {
        return read;
    }

    public void close() throws IOException
    {
        files.clear();
    }

    public AuditEvent readNext() throws IOException, ParseException
    {
        AuditEvent evt = null;
        File file = null;

        if( reader != null ){
            log.debug( "Trying to read next event from current file..." );
            try {
                
                evt = reader.readNext();
                if( evt != null ){
                    return evt;
                }
                
            } catch (Exception e) {
                reader.close();
                reader = null;
                log.error( "Failed to read more events from current file, processing next file..." );
                e.printStackTrace();
            }
        }

        //
        // proceed to the next file that might reveal event(s)
        //
        while( !files.isEmpty() ){

            try {
                total--;
                file = files.removeFirst();
                reader = new ModSecurity2AuditReader( file, false );
                evt = reader.readNext();
                if( evt != null )
                    return evt;
                
            } catch (Exception e) {
                log.error( "Failed to read event from file '{}': {}", file, e.getMessage() );
            }
        }

        return evt;
    }

    public Iterator<AuditEvent> iterator()
    {
        log.error( "This reader does not implement the iterator function!" );
        return null;
    }
}