/**
 * 
 */
package org.jwall.web.audit.io;

import java.io.Serializable;

import stream.AbstractProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public class IronBeeTimestampProcessor extends AbstractProcessor {

	public final static String TIMESTAMP_FORMAT = "yyyy-mm-ddThh:mm:ss.Z";

	public IronBeeTimestampProcessor() {

	}

	/**
	 * @see stream.data.DataProcessor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data data) {

		Serializable ts = data.get("REQUEST_TIMESTAMP");

		return data;
	}
}
