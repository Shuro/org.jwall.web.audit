package org.jwall.web.audit.io;

import java.io.File;

public interface FileHandler {

	public void handle( File file ) throws Exception;
}
