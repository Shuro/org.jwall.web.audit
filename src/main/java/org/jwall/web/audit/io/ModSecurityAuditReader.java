/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Vector;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurityAuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * This class implements a reader for AuditEvent-objects read from a file produced
 * by mod_security prior version 2.0.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ModSecurityAuditReader
extends AbstractAuditEventReader
{
	static Logger log = LoggerFactory.getLogger( ModSecurityAuditReader.class );

    /**
	 * This creates a new instance of this class that reads events from the
	 * given file.
	 * 
	 * @param f
	 * @throws IOException
	 */
	public ModSecurityAuditReader(File f)
	throws IOException
	{
		super( new FileInputStream(f) );
	}
	
	
	public ModSecurityAuditReader( InputStream in ){
		super( in );
	}
	

    /**
     * This creates a new instance of this class that reads events from the
     * given file.
     * 
     * @param f
     * @throws IOException
     */
    public ModSecurityAuditReader(File f, boolean tail)
    throws IOException
    {
        this(f);
        if( tail )
            reader.skip( f.length() );
    }

	
	/**
	 * This method reads another event from the stream.
	 * 
	 * @return The event that was read.
	 */
	public AuditEvent readNext() throws IOException, ParseException 
	{
		try {
			String line = reader.readLine();
			// skip lines until we find a audit-entry-start
			//
			while(reader.ready() && line != null && ! line.matches("==[A-Fa-f0-9]*=*")){
				//log.debug("line: "+line);
				line = reader.readLine();
			}
			
			if( line == null )
			    return null; 
			   
			log.debug( "Found separator line: {}", line );
			String id = line.replaceAll("=", "");
			line = reader.readLine();


			if(line == null || (! reader.ready())){
				log.error( "Line is null and reader not ready...aborting." );
				throw new Exception("Error !!");
			}


			// now line points to the first audit-entry-line
			//
			String[] access = parseAccessLogLine(line);
			String transformedHeader = "["+access[5]+"] "+id+" "+access[2]+" 0 "+access[1]+" 80\n";

			while(line != null && !line.startsWith("------------") ){
				log.debug( "Skipping line: {}", line );
				line = reader.readLine();
			}

			StringBuffer msg = new StringBuffer();
			StringBuffer request = new StringBuffer();


			int contentLength = -1;
			do {
				line = reader.readLine();

				if(line != null){
					if(line.startsWith("mod_security")){
						msg.append( line );
						msg.append( "\n" );
					} else {
						request.append(line);
						request.append("\n");
					}

					if( line.startsWith("Content-Length") )
						contentLength = Integer.parseInt( line.substring( "Content-Length: ".length() ) );
				}
			} while(line != null && !line.equals(""));


			StringBuffer reqBody = new StringBuffer();
			if( contentLength > 0 ){
				reader.readLine();

				char[] body = new char[contentLength];
				int read = reader.read( body );
				if( read != body.length )
				    throw new Exception("Did not read as much bytes as expected!"); 
				    
				reqBody.append(body);
			}

			boolean csRead = false;   // stable predicate that becomes true after the content-size line is read (first line)
			while(line != null && !line.startsWith("HTTP")){
				do {
					line = reader.readLine();
				} while( "".equals(line) );

				if(line != null && ! line.startsWith("HTTP")){
					if(! csRead && line.matches("[A-Fa-f0-9]*")){
						csRead = true;
					} else {
						reqBody.append( line );
						reqBody.append( "\n" );
					}
				}
			}

			StringBuffer response = new StringBuffer();
			while(line != null && ! line.equals("") && ! line.matches("^--[A-Fa-f0-9]*--")){
				if(! line.startsWith("=====")){
					response.append(line);
					response.append("\n");
				}
				line = reader.readLine();
			}
			response.append("\n");


			String[] data = new String[ModSecurity.SECTIONS.length() + 1];
			for(int i = 0; i < data.length; i++){
				data[i] = "";

				if(i == 0)
					data[i] = "";
			}

			data[ModSecurity.SECTION_AUDIT_LOG_HEADER] = transformedHeader;
			data[ModSecurity.SECTION_REQUEST_HEADER] = request.toString();
			data[ModSecurity.SECTION_REQUEST_BODY] = reqBody.toString();
			data[ModSecurity.SECTION_FINAL_RESPONSE_HEADER] = response.toString();
			data[ModSecurity.SECTION_AUDIT_TRAILER] = msg.toString();

			return new ModSecurityAuditEvent(data, AuditEventType.ModSecurity2);
		} catch (Exception e) {
			return null;
		}
	}


	/**
	 * 
	 * This method reads a line of the input-file and parses it according to the logfile-format.
	 * 
	 * Problem: Certain fields in the input-line are strings in quotes or brackets. These contain
	 *     spaces, thus the line cannot simply be split by spaces.
	 * 
	 */
	private String[] parseAccessLogLine(String input)
	throws ParseException
	{
		int lineNr = 0;

		String[] token;

		lineNr++;
		// Preprocessing:
		//    We extract strings in quotes. Strings in brackets are treated the same, so we need
		//    to convert brackets to quotes.
		// TODO: Brackets in quoted strings should remain brackets ;-)

		StringBuffer line = new StringBuffer(input.replaceFirst("\\[","\"").replaceFirst("\\]","\""));

		//
		// Now the line consists of strings and possibly quoted strings.
		//

		// this checks the number of quote-marks. should be even.
		int esc = 0;
		for(int k = 0; k < line.length(); k++)
			if(line.charAt(k) == '"')
				esc++;

		if(esc % 2 > 0)
			throw new ParseException("Error in line " + lineNr +": Number of '\"' characters is odd!");
		

		//
		// The next part replaces quotet strings by representatives, so the line can be
		// splitted by spaces
		//
		Vector<String> subs = new Vector<String>();

		do {
			// TODO: Hier fehlt das "richtige" Erkennen von Anfuehrungszeichen, bzw. escap'den Anf.zeichen
			int s = line.indexOf("\"") - 1;     // start of the quoted string
			int e = line.indexOf("\"", s+2)+1;  // end of the quoted string

			String satz = line.substring(s,e);
			//log.debug("Found : >>>"+satz+"<<< ("+(subs.size())+" subs in mind)");

			subs.add( satz.substring(2, satz.length() - 1) );
			line = line.replace(s,e, " $"+subs.size()+"");

		} while(line.indexOf("\"") > 0);

		//log.debug("finally the line looks like: "+line.toString());
		token = line.toString().split(" ");

		//
		// resubstitute the substitutes to the original strings
		//
		int ptr = 0;
		for(int k = 0; k < token.length; k++)
			if(token[k].matches("\\$\\d"))
				token[k] = subs.elementAt(ptr++);

		return token;
	}
	


    @Override
    public Iterator<AuditEvent> iterator()
    {
        try {
            return new AuditEventIterator( this );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
