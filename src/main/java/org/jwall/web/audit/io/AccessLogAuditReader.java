/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.io;

import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurityAuditEvent;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;


/**
 * 
 * This simple class creates small AuditEvent objects from a line of an
 * web-server access log. The resulting events are not complete, i.e.
 * they do not include the complete header.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class AccessLogAuditReader
extends AbstractAuditEventReader
{
    /** A unique logger for this class */
    private static Logger log = LoggerFactory.getLogger( "AccessLogAuditReader" );


    /**
     * 
     * This creates an instance of this class that reads from the
     * given file <code>accessLogFile</code>.
     * 
     * @param accessLogFile The file to read events from.
     * @throws IOException In case a reader on this file cannot be created.
     */
    public AccessLogAuditReader( File accessLogFile, boolean tail )
    throws IOException
    {
        super( new FileInputStream( accessLogFile ) );
        fileSize = accessLogFile.length();
        if( tail )
            reader.skip( accessLogFile.length() );
    }


    /**
     * 
     * This creates an instance of this class that reads from the
     * given file <code>accessLogFile</code>.
     * 
     * @param accessLogFile The file to read events from.
     * @throws IOException In case a reader on this file cannot be created.
     */
    public AccessLogAuditReader( File accessLogFile )
    throws IOException
    {
        super( new FileInputStream( accessLogFile ) );
        fileSize = accessLogFile.length();
    }


    public AccessLogAuditReader( InputStream in ){
	super(in);
    }

    /**
     * This method reads the next line from the underlying file
     * and creates an AuditEvent-object from it.
     * 
     * @see org.jwall.web.audit.io.AuditEventReader#readNext()
     */
    public AuditEvent readNext(){
        try {
            String line = reader.readLine();
            if( line != null ){
                bytesRead += ( line.length() + 1 );
                return AccessLogAuditReader.createEvent( line );
            }

        } catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }


    /**
     * 
     * This method splits the given line into tokens. In contrast to
     * the java-native split, it preserves quoted strings.
     * 
     * @param input The string to be split into parts.
     * @return An array of strings that are parts of the original string.
     */
    public static String[] splitAccessLine( String input ){
        StringBuffer line;
        int lineNr = 0;

        String[] token;

        input = input.replaceAll("\\\"", "\"");
        lineNr++;
        // Preprocessing:
        //    We extract strings in quotes. Strings in brackets are treated the same, so we need
        //    to convert brackets to quotes.
        // TODO: Brackets in quoted strings should remain brackets ;-)

        line = new StringBuffer(input.replaceFirst("\\[","\"").replaceFirst("\\]","\""));

        //
        // Now the line consists of strings and possibly quoted strings.
        //

        // this checks the number of quote-marks. should be even.
        int esc = 0;
        for(int k = 0; k < line.length(); k++)
            if(line.charAt(k) == '"')
                esc++;

        if(esc % 2 > 0){
            log.warn( lineNr + ": " + line );
            return null;
        }

        //
        // The next part replaces quotet strings by representatives, so the line can be
        // splitted by spaces
        //
        Vector<String> subs = new Vector<String>();

        do {
            int s = line.indexOf("\"") - 1;     // start of the quoted string
            int e = line.indexOf("\"", s+2)+1;  // end of the quoted string

            String satz = line.substring(s,e);

            subs.add( satz.substring(2, satz.length() - 1) );
            line = line.replace(s,e, " $"+subs.size()+"");

        } while(line.indexOf("\"") > 0);

        //log.debug("finally the line looks like: "+line.toString());
        token = line.toString().split(" ");

        //
        // resubstitute the substitutes to the original strings
        //
        int ptr = 0;
        for(int k = 0; k < token.length; k++)
            if(token[k].matches("\\$\\d"))
                token[k] = subs.elementAt(ptr++);

        return token;
    }


    /**
     * This method create an audit-event from the given access-line. The line is
     * splitted into parts and an intermediate-data buffer is created, holding
     * a pseudo http-header and simple audit-log sections.
     * 
     * @param accessLine The line to create the event from.
     * @return An audit-event instance.
     * @throws Exception In case anything goes wrong ;-)
     */
    public static AuditEvent createEvent( String accessLine ) throws Exception {
        String[] token = splitAccessLine( accessLine );

        String[] data = new String[ ModSecurity.SECTIONS.length() ];

        String id = Long.toHexString( System.currentTimeMillis() );
        for( int i = 0; i < data.length; i++ )
            data[ i ] = "";

        data[ ModSecurity.SECTION_AUDIT_LOG_HEADER ] = "["+token[3]+"] "+id+" "+token[0]+" ? 127.0.0.1 80";
        String method = "GET";
        String uri = token[4];
        String proto = "HTTP/1.1";
        String host = token[0];
        String[] req = token[4].split(" ");
        if( req.length > 1 ){
            method = req[0];
            uri = req[1];
        }

        if( req.length > 2 )
            proto = req[2];

        StringBuffer h = new StringBuffer();
        h.append( method + " " + uri + " " + proto + "\n" );
        h.append( "Host: "+ host + "\n" );
        if( token.length > 8 )
            h.append( "User-Agent: " + token[8] + "\n" );

        if( token.length > 7 ){
            if( token[5] != null && !"".equals( token[7] ) && !"-".equals( token[7] ) )
                h.append("Referer: " + token[7] + "\n" );
        }
        h.append("\n");

        data[ ModSecurity.SECTION_REQUEST_HEADER ] = h.toString();

        StringBuffer r = new StringBuffer();
        r.append( proto + " "+ token[5] + "\n" );

        r.append("\n");
        data[ ModSecurity.SECTION_FINAL_RESPONSE_HEADER ] = r.toString();

        return new ModSecurityAuditEvent( data, AuditEventType.ModSecurity2 );
    }


    @Override
    public Iterator<AuditEvent> iterator()
    {
        try {
            return new AuditEventIterator( this );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
