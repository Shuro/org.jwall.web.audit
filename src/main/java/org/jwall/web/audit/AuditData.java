package org.jwall.web.audit;

import org.jwall.audit.Event;
import org.jwall.audit.EventType;

import stream.data.DataImpl;

public class AuditData extends DataImpl implements Event {

	/** The unique class ID */
	private static final long serialVersionUID = 3092385126433800772L;
	
	public final static String TIMESTAMP_KEY = "@timestamp";
	
	
	public AuditData(){
		put( TIMESTAMP_KEY, new Long( System.currentTimeMillis() ) );
	}
	
	public AuditData( AuditEvent evt ){
		put( TIMESTAMP_KEY, evt.getDate().getTime() );
		
		for( int i = 0; i < ModSecurity.SECTIONS.length(); i++ ){
			String sect = evt.getSection(i);
			if( sect != null )
				put( "section:" + ModSecurity.SECTIONS.charAt( i ), sect );
		}
	}
	
	/**
	 * @see org.jwall.audit.Event#getTimestamp()
	 */
	@Override
	public Long getTimestamp() {
		return (Long) super.get( TIMESTAMP_KEY );
	}

	/**
	 * @see org.jwall.audit.Event#getType()
	 */
	@Override
	public final EventType getType() {
		return EventType.AUDIT;
	}

	/**
	 * @see org.jwall.audit.Event#getString(java.lang.String)
	 */
	@Override
	public String get(String variableName) {
		
		if( containsKey( variableName ) )
			return super.get(variableName) + "";
		
		return null;
	}

	/**
	 * @see org.jwall.audit.Event#set(java.lang.String, java.lang.String)
	 */
	@Override
	public void set(String variable, String value) {
		put( variable, value );
	}
}
