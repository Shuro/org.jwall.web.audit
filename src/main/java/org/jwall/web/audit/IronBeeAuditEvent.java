package org.jwall.web.audit;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import org.jwall.audit.EventType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

public class IronBeeAuditEvent extends LinkedHashMap<String, String> implements
		AuditEvent {

	/** The unique class ID */
	private static final long serialVersionUID = -5660788992216264848L;

	static Logger log = LoggerFactory.getLogger(IronBeeAuditEvent.class);

	public final static String SECTION_BOUNDARY_KEY = "__SECTION_BOUNDARY_KEY__";

	Long timestamp = System.currentTimeMillis();
	AuditEventMessage[] messages;

	public IronBeeAuditEvent() {
	}

	public IronBeeAuditEvent(Data data) {

		put(ModSecurity.TX_ID, data.get(SECTION_BOUNDARY_KEY) + "");

		for (String key : data.keySet()) {
			Serializable val = data.get(key);
			if (val instanceof String) {
				String repl = mapVariable(key);
				log.trace("  Replacing '{}' with '{}'", key, repl);
				put(repl, val.toString());
			}
		}

		if (data.containsKey("event_messages")) {
			try {
				AuditEventMessage[] msgs = (AuditEventMessage[]) data
						.get("event_messages");
				messages = msgs;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public Long getTimestamp() {
		return timestamp;
	}

	@Override
	public EventType getType() {
		return EventType.AUDIT;
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getEventId()
	 */
	@Override
	public String getEventId() {
		return get(ModSecurity.TX_ID);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getSection(int)
	 */
	@Override
	public String getSection(int i) {
		return null;
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getDate()
	 */
	@Override
	public Date getDate() {
		return new Date(timestamp);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getSessionId()
	 */
	@Override
	public String getSessionId() {
		return get(ModSecurity.SESSIONID);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#setSessionId(java.lang.String)
	 */
	@Override
	public void setSessionId(String id) {
		set(ModSecurity.SESSIONID, id);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#isSet(java.lang.String)
	 */
	@Override
	public boolean isSet(String variable) {
		String var = IronBeeVariables.mapToIronBee(variable);
		if (VariableMapping.isCaseInsensitive(var))
			var = var.toUpperCase();
		return containsKey(var);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#get(java.lang.String)
	 */
	@Override
	public String get(String variable) {
		String var = IronBeeVariables.mapToIronBee(variable);
		if (VariableMapping.isCaseInsensitive(var))
			return super.get(var.toUpperCase());
		else
			return super.get(var);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getAll(java.lang.String)
	 */
	@Override
	public List<String> getAll(String variable) {
		String var = IronBeeVariables.mapToIronBee(variable);
		if (VariableMapping.isCaseInsensitive(var))
			var = var.toUpperCase();

		List<String> list = new ArrayList<String>(1);
		if (containsKey(var))
			list.add(get(var));
		return list;
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#set(java.lang.String,
	 *      java.lang.String)
	 */
	@Override
	public void set(String variable, String value) {
		if (value == null)
			remove(IronBeeVariables.mapToIronBee(variable));
		else
			put(IronBeeVariables.mapToIronBee(variable), value);
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#setAll(java.lang.String,
	 *      java.util.List)
	 */
	@Override
	public void setAll(String variable, List<String> values) {
		if (values.isEmpty()) {
			remove(IronBeeVariables.mapToIronBee(variable));
		} else {
			put(IronBeeVariables.mapToIronBee(variable), values.get(0));
		}
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getVariables()
	 */
	@Override
	public List<String> getVariables() {
		return new ArrayList<String>(keySet());
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#compareTo(org.jwall.web.audit.AuditEvent)
	 */
	@Override
	public int compareTo(AuditEvent o) {

		if (o == null)
			return 1;

		if (this == o)
			return 0;

		int ret = timestamp.compareTo(o.getTimestamp());
		if (ret != 0)
			return ret;

		return this.getEventId().compareTo(o.getEventId());
	}

	/**
	 * @see org.jwall.web.audit.AuditEvent#getRawData()
	 */
	@Override
	public String[] getRawData() {
		return new String[0];
	}

	@Override
	public AuditEventType getAuditEventType() {
		return AuditEventType.IronBee;
	}

	public String toString() {
		StringBuilder s = new StringBuilder("\r\n\r\n");
		String boundary = get(SECTION_BOUNDARY_KEY);

		s.append("MIME-Version: 1.0\r\nContent-Type: multipart/mixed;");
		if (boundary != null)
			s.append(" boundary=" + boundary);
		s.append("\r\n\r\n");
		s.append("This is a multi-part message in MIME format.\r\n\r\n");

		for (String key : keySet()) {
			if (key.startsWith("SECTION:")) {
				s.append("--");
				s.append(boundary);
				s.append("\r\n");
				s.append(get(key));
			}
		}

		s.append("--");
		s.append(boundary);
		s.append("--\r\n");
		return s.toString();
	}

	public static String mapVariable(String str) {
		return str.toUpperCase().replaceAll("-", "_");
	}

	@Override
	public AuditEventMessage[] getEventMessages() {
		return messages;
	}
}