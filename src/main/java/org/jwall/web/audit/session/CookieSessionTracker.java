/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.session;

import java.util.Set;
import java.util.TreeSet;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;

/**
 * 
 * This tracker creates a session-context wrt to a specific session-cookie. Sessions are
 * only created by events that contain a <code>Set-Cookie</code> header, thus this tracker
 * is somewhat immune to session-fixation.
 *
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class CookieSessionTracker 
    extends AbstractSessionTracker
{
    /** Name of the cookie, by which the session will be tracked*/
    private String sessionCookie;
    private Set<String> valid = new TreeSet<String>(); 
    
    /**
     * This tracker creates sessions on behalf of a id sent in the
     * request-cookie names <code>cookieName</code>.
     * 
     * @param cookieName The name of the session-cookie.
     */
    public CookieSessionTracker(String cookieName){
        super();
        this.type = Session.COOKIE_BASED;
        sessionCookie = cookieName;
        valid = new TreeSet<String>();
    }
    
    
    /**
     * This method extracts the session-id that is sent in this event
     * within the cookie, identified by <code>sessionCookie</code>.
     * 
     * @param evt The AuditEvent from which the session-id is to be extracted.
     * @return The id of the session associated with this event.
     */
    public String extractKey( AuditEvent evt ){
        
        String var = ModSecurity.REQUEST_COOKIES + ":" + sessionCookie;
        log.debug( "Looking up cookie by variable '" + var + "'" );
        
        String sid = evt.get( var );
        log.debug( "    value = '" + sid + "'" );
        
        if( sid != null ){ 
            if( valid.contains(sid) || !strictSessions )  // ok, we created this id before
                return sid;                           //
            else {
                log.warn( "No valid session found for ID '" + sid + "'!" );
                return null;
            }
        }
        
        //
        // check if there is a new cookie set => new session
        //
        sid = evt.get( ModSecurity.RESPONSE_COOKIES + ":" + sessionCookie );
        if( sid != null ){
            log.debug("Creating new session from response:  Set-Cookie: '{}' = {}", sessionCookie, sid);
            valid.add( sid );
            return sid;
        }
        
        return null;
    }
    
    
    /**
     * Expire the session as normally, but also remove the session-id from
     * the valid-list.
     */
    public void expireSession( Session s ){
        super.expireSession(s);
        valid.remove( s.getId() );
    }
    
    public String toString(){
    	return "CookieSessionTracker["+this.sessionCookie+","+this.sessionTimeOut+"]";
    }
}
