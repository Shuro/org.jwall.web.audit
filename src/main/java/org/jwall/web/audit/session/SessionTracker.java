/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.session;

import java.util.Collection;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;

/**
 *
 * This interface defines the basic function of a session tracker. 
 *
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface SessionTracker extends AuditEventListener
{
    
    /**
     * Enables strict-mode for tracking/creating new sessions. When settings this to <code>true</code>,
     * new sessions are only created by session-identifiers sent from the server-side (Set-Cookie,
     * href,...).
     * 
     * @param b
     */
    public void setStrictSessions(boolean b);
    

    /**
     * 
     * This method sets the session timeout. After this amount of time without any
     * requests by the client the session is simply timed out.
     * 
     * @param s The session timeout in <b>milliseconds</b>!
     */
    public void setSessionTimeOut( long s );
    
    
    /**
     * This method simply returns the session timeout of the tracker implementation.
     * The value returned is the amount of <b>milliseconds</b> after which the session
     * is declared to have timed out.  
     * 
     * @return The session timeout in <b>milliseconds</b>!
     */
    public long getSessionTimeOut();
    
    
    /**
     * Returns <code>true</code>, if the tracker only produces strict sessions.
     * 
     * @return <code>true</code>, if this tracker creates only strict sessions.
     */
    public boolean usesStrictSessions();
    
    
    /**
     * This method simply associates the give event with a session
     * or creates a new session if this event is not related to a previous
     * one.
     * 
     * @param event
     */
    public void eventArrived(AuditEvent event);
    
    
    /**
     * This method resets the tracker to a clean state, forgetting about
     * all sessions.
     *
     */
    public void reset();
    
    
    /**
     * Returns a list (collection) of the sessions that have been created/observed by this
     * tracker. This collections contains all active and inactive sessions.
     * 
     * @return A collection of the sessions created by this tracker. 
     */
    public Collection<Session> getSessions();
    
    
    /**
     * This method is meant to return the session that is associated with the given ID. The
     * session id is expected to be unique among all sessions.
     * 
     * @param sessionId The id of the session that is to be returned.
     * @return The session that is assiciated with the given id.
     */
    public Session getSession( String sessionId );
}
