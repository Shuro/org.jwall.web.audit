/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.session;

import java.util.Date;
import java.util.Set;


/**
 * 
 * This interface defines an abstract session context that is associated with a session-identifier
 * and provides features like setting variables and removing or expiring variables of session scope.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface SessionContext
{
    /**
     * This method returns an unique identifier that serves as a key to this session.
     * 
     * @return The session identifier.
     */
    public String getId();
    
    
    /**
     * This method returns a set of variable names (strings) that are set within the session scope
     * and did not expire yet. Not that due to expiration the returned set of variables may not
     * reflect the set of currently set variables, as after calling this method a variable might
     * expire. 
     * 
     * @return The variable-names that are valid in this session-context.
     */
    public Set<String> getVariableNames();
    

    /**
     * This method returns the string-value of the variable given by <code>name</code>. If no such
     * variable exists within this session, <code>null</code> is returned.
     * 
     * @param name
     * @return The value of the variable given by <code>name</code> or <code>null</code> if the
     *         variable does not exist.
     */
    public String getVariable( String name );
    
    
    /**
     * This method will increment the count of the variable <code>var</code> by the 
     * amount of <code>count</code>. If the variable is a non-numeric one, calling this
     * method will have no effect.
     * 
     * @param var
     * @param count
     */
    public void increment( String var, int count );
    
    
    /**
     * This method creates a new variable with the given name and initializes it to the
     * value provided by <code>value</code>. The variable will expire at the given date or
     * as the session itself expires. 
     * 
     * By providing a <code>null</code> value for the date, the variable will be set to
     * expire at the sessions end. 
     * 
     * @param name
     * @param value
     * @param expires
     */
    public void setVariable( String name, String value, Date expires);
    
    
    /**
     * This method shall remove the variable with the given name from the session-context.
     * If no such variable exists, calling this method will have no effect.
     * 
     * @param name
     */
    public void removeVariable( String name );
}
