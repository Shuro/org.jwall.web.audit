/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.session;

/**
 * 
 * This interface defines the constants that are part of the HTTP protocol. This
 * might become obsolete in the future.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public interface HttpProtocol
{
    public final static String REQUEST_METHOD_GET = "GET";
    public final static String REQUEST_METHOD_POST = "POST";
    public final static String REQUEST_METHOD_PUT = "PUT";
    public final static String REQUEST_METHOD_DELETE = "DELETE";
    public final static String REQUEST_METHOD_OPTIONS = "OPTIONS";

    /* String-Array mit allen verf&uuml;gbaren Methoden */
    public final static String[] REQUEST_METHODS = { 
                                    REQUEST_METHOD_GET, 
                                    REQUEST_METHOD_POST, 
                                    REQUEST_METHOD_PUT, 
                                    REQUEST_METHOD_DELETE, 
                                    REQUEST_METHOD_OPTIONS 
                                  };
    
    
    public final static String REQUEST_HEADER_ACCEPT = "Accept";
    public final static String REQUEST_HEADER_ACCEPT_LANGUAGE = "Accept-Language";
    public final static String REQUEST_HEADER_ACCEPT_ENCODING = "Accept-Encoding";
    public final static String REQUEST_HEADER_USER_AGENT = "User-Agent";
    public final static String REQUEST_HEADER_AUTHORIZATION = "Authorization";
    public final static String REQUEST_HEADER_CONNECTION = "Connection";
    public final static String REQUEST_HEADER_CONTENT_TYPE = "Content-Type";
    public final static String REQUEST_HEADER_HOST = "Host";
    public final static String REQUEST_HEADER_REFERER = "Referrer";
    public final static String REQUEST_HEADER_MODIFIED_SINCE = "If-Modified-Since";
    
    /* String-Array mit allen unterst&uuml;tzten Header-Feldern */
    public final static String[] REQUEST_HEADERS = { 
                                    REQUEST_HEADER_ACCEPT, 
                                    REQUEST_HEADER_ACCEPT_LANGUAGE,
                                    REQUEST_HEADER_ACCEPT_ENCODING,
                                    REQUEST_HEADER_USER_AGENT, 
                                    REQUEST_HEADER_AUTHORIZATION, 
                                    REQUEST_HEADER_CONNECTION, 
                                    REQUEST_HEADER_HOST,
                                    REQUEST_HEADER_REFERER,
                                    REQUEST_HEADER_MODIFIED_SINCE
                                  };
    
    public final static String RESPONSE_HEADER_SET_COOKIE = "Set-Cookie";
    public final static String RESPONSE_HEADER_KEEP_ALIVE = "Keep-Alive";
    public final static String RESPONSE_HEADER_CONTENT_TYPE = "Content-Type";
    public final static String RESPONSE_HEADER_CONNECTION = "Connection";
    public final static String RESPONSE_HEADER_CONTENT_LENGTH = "Content-Length";
    public final static String RESPONSE_HEADER_PRAGMA = "Pragma";
    public final static String RESPONSE_HEADER_TRANSFER_ENCODING = "Transfer-Encoding";
    public final static String RESPONSE_HEADER_EXPIRES = "Expires";
    public final static String RESPONSE_HEADER_ETAG = "ETag";
    public final static String RESPONSE_HEADER_ACCEPT_RANGES = "Accept-Ranges";
    public final static String RESPONSE_HEADER_LAST_MODIFIED = "Last-Modified";
    
    public final static String[] RESPONSE_HEADERS = { 
                                        RESPONSE_HEADER_SET_COOKIE, 
                                        RESPONSE_HEADER_KEEP_ALIVE, 
                                        RESPONSE_HEADER_CONTENT_TYPE, 
                                        RESPONSE_HEADER_CONTENT_LENGTH, 
                                        RESPONSE_HEADER_CONNECTION,
                                        RESPONSE_HEADER_PRAGMA,
                                        RESPONSE_HEADER_TRANSFER_ENCODING,
                                        RESPONSE_HEADER_EXPIRES,
                                        RESPONSE_HEADER_ETAG,
                                        RESPONSE_HEADER_ACCEPT_RANGES,
                                        RESPONSE_HEADER_LAST_MODIFIED
                                     };

    public final static String RESPONSE_STATUS_OK = "200";
    public final static String REPONSE_STATUS_AUTH_REQUIRED = "401";
    public final static String RESPONSE_STATUS_NOT_FOUND = "404";
}
