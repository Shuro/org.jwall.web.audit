/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.IOException;
import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.audit.io.AuditEventReader;


/**
 * <p>
 * This class implements a dispatcher for audit-events. Classes implementing the
 * AuditEventListener interface can register to an instance of this class for
 * being notified upon arrival of new events.
 * </p>
 * <p>
 * An instance of this dispatcher class can be created by using a given AuditEventReader
 * implementation in which case the dispatcher will continuously read events from that
 * reader and dispatch these to all listeners
 * </p>
 * <p>
 * Another use is to create an <i>empty</i> or non-connected dispatcher without a given
 * audit reader and externally inject events using the <code>enqueueEvent()</code> method.
 * This method will add the event(s) to an internal queue and dispatch them to the listeners
 * in a separate thread.
 * </p>
 * <p>
 * A dispatcher instance can either be <i>persistent</i> or <i>non-peristent</i>. In the
 * persistent mode it will read all events from the given reader and wait for new events
 * to arrive if the reader reached the end of file/input. In non-persistent mode the 
 * dispatcher will terminate once no more events can be read from the input-reader.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditEventDispatcher 
extends Thread
{
    /** The amount of milliseconds the dispatcher waits between polls when waiting for new events
     *  in persistent mode. This does not have any effect in non-persistent mode. */
    public final static int READER_WAITING_TIME = 256;

    /** A unique logger for this class */
    private static Logger log = LoggerFactory.getLogger( "AuditEventDispatcher" );

    /** The source from which events are obtained */
    private AuditEventReader source;

    /** The list of listeners that are to be notified of new events */
    private Queue<AuditEventListener> listener;

    /** This attribute is a counter for the number of events that have been dispatched by this instance */
    int eventsDispatched = 0;
    
    /** The internal buffer of events that are pending to be dispatched */
    AuditEventQueue buffer = null;

    /** A boolean flag indicating whether the dispatcher is running */
    boolean running = true;

    /** A boolean flag for setting the dispatcher into persistent mode */
    boolean persistent = true;
    
    
    
    /**
     * 
     * This constructor creates a new instance which does externally receive events
     * from other creators and dispatches them to registered listeners. The events
     * are not stored, i.e. if events arrive and no listener is registered, they will
     * simply be ignored.
     * 
     */
    public AuditEventDispatcher(){
        buffer = new AuditEventQueue();
        source = null;
        listener = new LinkedBlockingQueue<AuditEventListener>();
    }

    
    /**
     * This constructor creates a new dispatcher which reads events from the given
     * AuditEventReader. To make the dispatcher actually read events, the instance
     * has to be started using the <code>start()</code> method inherited by the 
     * Thread-superclass.
     * 
     * @param src The AuditEventReader used for reading events.
     */
    public AuditEventDispatcher( AuditEventReader src ){
        source = src;
        listener = new LinkedBlockingQueue<AuditEventListener>();
    }

    
    /**
     * Signals whether the dispatcher is running in persistent mode.
     * 
     * @return <code>true</code> if working persistently.
     */
    public boolean isPersistent(){
        return persistent;
    }
    
    
    /**
     * This methods can be used to set the dispatcher into persistent or non-persistent mode.
     * 
     * @param p A boolean flag indicating persistent (true) or non-persistent (false) mode.
     */
    public void setPersistent( boolean p ){
        persistent = p;
    }

    
    /**
     * This method adds the event to the dispatcher which will immediately dispatch it
     * to all registered listeners.
     * 
     * @deprecated The same functionality is now provided by <code>enqueueEvent(AuditEvent)</code>.
     * @param evt The event to be dispatched.
     */
    public void add( AuditEvent evt ){
        dispatchEvent( evt );
    }

    
    /**
     * This method enqueues the given event into the list of events to be dispatched to
     * the registered listeners. If this instance is reading events from a given AuditEventReader,
     * then no internal buffer does exist and the event is immediately dispatched to the set of
     * listeners currently registered.
     * 
     * @param evt The event to be dispatched.
     */
    public void enqueueEvent( AuditEvent evt ){
        if( buffer != null )
            buffer.eventArrived( evt );
        else
            dispatchEvent( evt );
    }

    
    /**
     * Register another listener which is to be notified if 
     * another event has been created.
     * 
     * @param listener
     */
    public void addAuditEventListener( AuditEventListener listener ){
        this.listener.add( listener );
    }

    
    /**
     * Remove the given listener from the list of listeners.
     * This method simply does nothing, if the given listener
     * is not registered. 
     * 
     * @param listener
     */
    public void removeAuditEventListener( AuditEventListener listener ){
        this.listener.remove( listener );
    }

    
    /**
     * This method will clear the list of all listeners currently registered
     * at this instance.
     */
    public void removeAuditEventListeners(){
        this.listener.clear();
    }

    
    /**
     * Returns the collection of currently registered listeners.
     * @return The set of listeners.
     */
    public Collection<AuditEventListener> getAuditEventListeners(){
        return this.listener;
    }
    

    /**
     * This method actually delivers events to all listeners currently registered. 
     * 
     * @param evt The event to be dispatched.
     */
    private void dispatchEvent( AuditEvent evt ){
        
        // should not happen, but you never know...
        //
        if( evt == null )
            return;

        log.debug("AuditEventDispatcher: Notifying listeners...");

        for( AuditEventListener l : listener )
            l.eventArrived( evt );
        
        eventsDispatched++;
    }


    /**
     * 
     * This is a loop that fetches events from the AuditEventSource
     * associated with this dispatcher and notifies the registered
     * listeners.
     * 
     */
    public void run(){

        while( running ){
            AuditEvent evt = null;

            while( evt == null ){
                try {
                    log.debug("AuditEventDispatcher: waiting for next event from source {}", source);
                    evt = source.readNext();
                } catch (Exception e) {
                    e.printStackTrace();
                    evt = null;
                }

                if( evt == null ){

                    if( persistent ) {
                        log.debug("Dispatcher in persistent mode...");
                        try {
                            Thread.sleep( READER_WAITING_TIME );
                        } catch ( Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        log.debug( "Dispatcher in non-persistent mode... shutting down...");
                        running = false;
                        break;
                    }
                } else
                    log.debug("ScriptEvent read, now dispatching it .... ");
            }

            dispatchEvent( evt );
        }
    }
    
    
    /**
     * Returns the number of events that have been dispatched by this instance. 
     * @return The number of dispatched events since startup.
     */
    public int getNumberOfEvents(){
        return eventsDispatched;
    }

    
    /**
     * Closes this dispatcher by closing the underlying reader (if any) and terminating
     * the dispatch thread. 
     * 
     * @throws IOException In case the reader could not be closed without error. The
     *                     dispatch thread is terminated in any case.
     */
    public void close() throws IOException {
        this.running = false;
        if( source != null )
            source.close();
    }
}
