/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.util.Collection;

import org.jwall.audit.EventListener;


/**
 *
 * This interface defines the methods all AuditEvent persistence-classes
 * have to implement. Also this interface needs to be implemented by the
 * classes that are thought to register to an event dispatcher and are
 * then notified on arrival of new events.
 *
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public interface AuditEventListener extends EventListener<AuditEvent>
{
    /**
     * This method is called when a new event arrives at the listener.
     * The Listener should be threaded and response-time to this method
     * should be as small as possible.
     * <p>
     * After the dispatcher calls this method each listener gets a call to 
     * {@link #notify()}, thus a listener might wait for packets to arrive.
     * 
     * @param evt The event which arrived.
     */
    public void eventArrived(AuditEvent evt);
    
    
    /**
     * This method is called for notifying the listener of arrival of 
     * several events in a block. Usually, this method is implemented by
     * calling {@link #eventArrived(AuditEvent)} for each of the given
     * events, but may be handled in a more efficient way by implementations
     * capable of processing blocks of events.
     * 
     * @param events The block of events arrived.
     * @deprecated
     */
    public void eventsArrived( Collection<AuditEvent> events );
}
