/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.rmi.RemoteException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.jwall.audit.EventView;
import org.jwall.web.audit.filter.AuditEventFilter;

/**
 * <p>
 * This interface defines all method that are accessible by a user for
 * investigating events contained in an event store. Typically implementations
 * of this interface are acquired by looking up the remote store and querying a
 * view instance using a users credentials.
 * </p>
 * <p>
 * The view returned by the store then contains all events that are viewable by
 * that specific user.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public interface AuditEventView extends EventView<AuditEvent> {

	public boolean contains(AuditEvent e);

	/**
	 * This method will return the number of events which match the given filter
	 * <code>filter</code> AND the user-specific implicit filter expressions.
	 * 
	 * @param filter
	 *            The filter to select the events being counted.
	 * @return The number of events in this view, matching the given filter.
	 * @throws RemoteException
	 */
	public Long count(AuditEventFilter filter) throws Exception;

	/**
	 * This method will return the complete audit-event from the storage. This
	 * may require loading the event from the event-storage (e.g. disk), which
	 * may take more time than just fetching the event-entry.
	 * 
	 * @param id
	 * @return
	 * @throws RemoteException
	 */
	public AuditEvent getEvent(Integer id) throws Exception;

	/**
	 * This method will return the complete audit-event from the storage. The ID
	 * provided is expected to be the event ID provided by ModSecurity and used
	 * by ModSecurity within the section separators.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public AuditEvent getEvent(String id) throws Exception;

	public boolean delete(Integer id) throws Exception;

	/**
	 * This method will retrieve the entry with the given id from the index or
	 * <code>null</code> if no entry exists with that id.
	 * 
	 * @param id
	 * @return
	 * @throws Exception
	 */
	public AuditEvent getEventEntry(Integer id) throws Exception;

	public ByteBuffer getEventRaw(Integer id);

	public InputStream getEventAsStream(Integer id);

	/**
	 * Creates an iterator over all event entries matching the given filter. The
	 * iterator supports the remove() method, but this does not have an effect
	 * on the database.
	 * 
	 * @param filter
	 * @return
	 * @throws Exception
	 */
	public Iterator<Integer> iterator(AuditEventFilter filter) throws Exception;

	public List<AuditEventMessage> getLastAlertMessages(int max)
			throws Exception;

	public Set<String> getVariables();

	/**
	 * 
	 * @return
	 * @throws Exception
	 * @deprecated
	 */
	public Set<String> getTags() throws Exception;

	/**
	 * 
	 * @param id
	 * @return
	 * @deprecated
	 */
	public Set<String> getTagsByEventId(Integer id);

	/**
	 * 
	 * @param user
	 * @return
	 * @deprecated
	 */
	public Set<String> getTagsByUser(String user);

	/**
	 * 
	 * @param filter
	 * @param offset
	 * @param num
	 * @deprecated
	 * @return
	 */
	public List<AuditEvent> list(AuditEventFilter filter, int offset, int num);

	/**
	 * 
	 * @param filter
	 * @param order
	 * @param offset
	 * @param num
	 * @return
	 * @deprecated
	 */
	public List<AuditEvent> list(AuditEventFilter filter, List<String> order,
			int offset, int num);

	/**
	 * 
	 * @param eventId
	 * @param user
	 * @param tags
	 * @deprecated
	 */
	public void tag(Integer eventId, String user, List<String> tags);

	/**
	 * 
	 * @param filter
	 * @param user
	 * @param tags
	 * @deprecated
	 */
	public void tag(AuditEventFilter filter, String user, List<String> tags);

	/**
	 * @param eventId
	 * @param user
	 * @param tags
	 * @deprecated
	 */
	public void untag(Integer eventId, String user, List<String> tags);

	/**
	 * 
	 * @param filter
	 * @param user
	 * @param tags
	 * @deprecated use untag(FilterExpression,..) instead
	 */
	public void untag(AuditEventFilter filter, String user, List<String> tags);
}
