package org.jwall.web.audit.test;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.filter.FilterCompiler;
import org.jwall.web.audit.filter.FilterExpression;

/**
 * <p>
 * This class parses a string line-by-line, reading a filter-expression from each line.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class TestParser {

	public static List<FilterExpression> parse( String str ) throws Exception {
		List<FilterExpression> list = new ArrayList<FilterExpression>();
		
		BufferedReader r = new BufferedReader( new StringReader( str ) );
		String line = r.readLine();
		while( line != null ){
			if( ! line.startsWith( "#" ) && !"".equals( line.trim() ) )
				list.add( FilterCompiler.parse( line.trim() ) );
			line = r.readLine();
		}
		r.close();
		return list;
	}
}
