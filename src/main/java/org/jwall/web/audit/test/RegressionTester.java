package org.jwall.web.audit.test;

import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.FilterExpression;

public class RegressionTester {

	public static List<String> test( AuditEvent evt, String testSection ) throws Exception {
		List<String> errors = new ArrayList<String>();
		
		List<FilterExpression> tests = TestParser.parse( testSection );
		for( FilterExpression test : tests )
			if( !test.matches( evt ) )
				errors.add( "Test '" + test + "' failed on event " + evt.getEventId() );
		
		return errors;
	}
}
