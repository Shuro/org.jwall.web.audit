/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.SyntaxException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


/**
 * <p>
 * This class implements a date match of an event. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("DateMatch")
public class AuditEventDateMatch
	extends AuditEventMatch
{
    /** The unique class ID */
    private static final long serialVersionUID = 8260257445184659695L;

    /* This is used to create the valid string-representation from the date */
    public static SimpleDateFormat FMT = new SimpleDateFormat( "yyyy-MM-dd hh:mm:ss" );


    @XStreamAsAttribute
    String variable = ModSecurity.DATE;

    @XStreamAsAttribute
    Date date = new Date();



    public AuditEventDateMatch( Operator op, Date date ) throws SyntaxException {
    	super( ModSecurity.DATE, op, FMT.format(date) );
    }




    public AuditEventDateMatch( String var, Operator op, Date date )
    throws SyntaxException
    {
    	super( ModSecurity.DATE, op, FMT.format(date) );
        this.date = date;
        
        if( var.equals( ModSecurity.DATE ) || var.equals( AuditEvent.RECEIVED_AT ) )
            variable = var;
        else
            throw new SyntaxException( "Variable '" + variable + "' is not known to be a date-variable! Valid Variables are " + ModSecurity.DATE + " and " + AuditEvent.RECEIVED_AT );
    }


    public void setComparator( String compare ) throws SyntaxException {
        if( isValidOp( compare ) )
            this.setOperator( compare );
        else
            throw new SyntaxException( "Invalid comparator for match: '" + compare + "'!" );
    }


    /**
     * @see org.jwall.web.audit.filter.AuditEventMatch#addValue(java.lang.String)
     */
    public void setDate(Date val)
    {
        this.date = val;
    }
    
    public Date getDate(){
    	return date;
    }
    
    public Date getValueObject(){
    	return getDate();
    }


    public boolean isValidOp( String op ){
        return getSupportedOperators().contains( op );
    }

    





    public String getVariable()
    {
        return this.variable;
    }




    public boolean matches(AuditEvent evt)
    {
        Date evtDate = evt.getDate();
        Date date = new Date(0L);
        try {
        	date = this.date;
            String val = evt.get( variable );
            if( val == null )
                System.out.println( "Variable '" + variable + "' cannot be extracted from event " + evt.getEventId() + ", falling back to event.date comparison!");
            
            evtDate = new Date( new Long( val ) );
            
        } catch (Exception e) {
            evtDate = evt.getDate();
        }

        switch( getOp() ){
        	case GT: 
        		return evtDate.after( date );
        		
        	case GE: 
        		return evtDate.equals( date ) || evt.getDate().after( date );
        		
        	case LT: 
        		return evtDate.before( date );
        		
        	case LE: 
        		return evtDate.equals( date ) || evt.getDate().before( date );
        		
        	default: 
        		return evtDate.equals( date );
        }
    }

    
    public Object readResolve(){
    	return this;
    }
    

    public List<String> getSupportedOperators(){
        List<String> list = new LinkedList<String>();
        list.add( Operator.EQ.toString() );
        list.add( Operator.LT.toString() );
        list.add( Operator.GT.toString() );
        list.add( Operator.GE.toString() );
        list.add( Operator.LE.toString() );
        return list;
    }


    public String toXML()
    {
        XStream xs = new XStream();
        xs.processAnnotations( AuditEventDateMatch.class );
        return xs.toXML( this );
    }
}
