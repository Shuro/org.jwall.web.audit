/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import org.jwall.audit.Match;
import org.jwall.web.audit.AuditEvent;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("Filter")
public class AuditEventFilter
	implements Serializable, FilterExpression
{
    /** The unique class ID */
	private static final long serialVersionUID = 7710316960125556052L;
	
    @XStreamImplicit
    LinkedList<AuditEventMatch> matches = new LinkedList<AuditEventMatch>();
    
    @XStreamAlias("enabled")
    @XStreamAsAttribute
    boolean enabled = true;
    
    public AuditEventFilter(){
        matches = new LinkedList<AuditEventMatch>();
    }

    
    public List<AuditEventMatch> getMatches(){
        if( matches == null )
            matches = new LinkedList<AuditEventMatch>();
        return matches;
    }
    
    public void add( Match m ){
    	this.add( new AuditEventMatch( m.getVariable(), m.getOp(), m.getValue() ) );
    }
    
    
    public void add( AuditEventMatch match ){
        matches.add( match );
    }
    
    public void remove( AuditEventMatch match ){
        matches.remove( match );
    }
    
    public boolean remove( int i ){
        try {
            matches.remove(i);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    
    public void removeMatch( String variable ){

        int idx = -1;
        for( AuditEventMatch m : matches ){
            if( variable.equals( m.getVariable() ) ){
                idx = matches.indexOf( m );
                break;
            }
        }
     
        if( idx >= 0 )
            matches.remove( idx );
    }
    
    
    public boolean hasMatchOn( String variable ){
        for( AuditEventMatch m : matches ){
            if( variable.equals( m.getVariable() ) )
                return true;
        }
        
        return false;
    }

    
    public String toXML(){
        return getXStream().toXML( this );
    }
    
    public AuditEventFilter fromXML( String input ){
        return (AuditEventFilter) getXStream().fromXML( input );
    }
    
    public static XStream getXStream( ClassLoader loader ){
        XStream xs = new XStream(); // new PureJavaReflectionProvider(), new XppDriver(), loader );
        xs.processAnnotations( AuditEventFilter.class );
        xs.processAnnotations( AuditEventMatch.class );
        return xs;
    }
    
    
    public static XStream getXStream(){
        return getXStream( AuditEventFilter.class.getClassLoader() );
    }
    
    public boolean matches( AuditEvent evt ){
     
    	if( matches == null )
    		return false;
    	
        for( AuditEventMatch m : matches )
            if( !m.matches( evt ) ){
                return false;
            }
        
        return true;
    }
    
    public boolean isEnabled(){
        return enabled;
    }
    
    public void setEnabled( boolean b ){
        enabled = b;
    }
}
