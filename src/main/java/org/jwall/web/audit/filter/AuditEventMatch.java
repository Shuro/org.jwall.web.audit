/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.filter;

import org.jwall.audit.Match;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.rules.operators.AbstractCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * <p>
 * This class implements the most generic ModSecurity-like match. It matches the value of a
 * given variable against a single or set of values using a specific operator. This implementation
 * supports the operators
 * <ul>
 *    <li><code>rx</code>, by matching the value against regular expressions</li>
 *    <li><code>pm</code>, by looking for substring matches</li>
 *    <li><code>eq</code>, by looking for equality of strings</li>
 * </ul>
 * 
 * All matches can be inverted by prepending an exclamation-mark &quot;!&quot; to the value.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 */
@XStreamAlias("AuditEventMatch")
public class AuditEventMatch 
	extends Match
	implements FilterExpression
{
    /** The unique class ID */
    private static final long serialVersionUID = -3682911278800228726L;
    static Logger log = LoggerFactory.getLogger( AuditEventMatch.class );

    public AuditEventMatch(){
    	super();
    }
    
    public AuditEventMatch( String variable ){
    	super( variable );
    }

    public AuditEventMatch( String variable, String value ){
        this( variable, Operator.EQ, value );
    }


    public AuditEventMatch( String variable, Operator op, String value){
    	super( variable, op, value );
    }
    

    
    

    


    /**
     * @see org.jwall.web.audit.filter.AuditEventMatch#matches(org.jwall.web.audit.AuditEvent)
     */
    public boolean matches(AuditEvent evt)
    {
        if( log.isDebugEnabled() )
            log.debug( "Matching event {}", evt.getEventId() );
        
        if( evt == null )
            return false;
        
        
        if( condition == null && getValue() != null ){
            try {
                condition = AbstractCondition.createCondition( getOp(), getVariable(), getValue() );
            } catch (Exception e) {
                log.error( "Failed to create condition for op: {}", getOperator() );
                return false;
            }
        }
        
        if( condition.matches( evt ) ){
            return true;
        }
        return false;
    }

    

    /**
     * Returns an XML representation of this match.
     * @return
     */
    public String toXML(){
        return AuditEventFilter.getXStream().toXML( this );
    }
    
    public Object readResolve(){
        try {
            condition = AbstractCondition.createCondition( this.getOp(), this.getVariable(), this.getValue() );
        } catch (Exception e) {
        	log.error( "Variable: {}", getVariable() );
        	log.error( "Operator: {}", getOp() );
        	log.error( "Value: {}", getValue() );
        	log.error( "Error: {}", e.getMessage() );
            e.printStackTrace();
            condition = null;
        }
        
        return this;
    }
}