package org.jwall.web.audit.filter;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.jwall.audit.Match;
import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.rules.Condition;
import org.jwall.web.audit.rules.operators.AbstractCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterCompiler {
	
	
	static Logger log = LoggerFactory.getLogger( FilterCompiler.class );

	
	
	public Condition createCondition( FilterExpression e ) throws SyntaxException {
		if( e instanceof Match ){
			Match m = (Match) e;
			return AbstractCondition.createCondition( m.getOp(), m.getVariable(), m.getValue() );
		}
		
		throw new FilterException( "Don't know how to compile condition from expression " + e + "!" );
	}
	
	
	public final static FilterExpression map( AuditEventFilter filter ) throws FilterException {
		
		if( filter == null || filter.getMatches().isEmpty() )
			return null;
		
		StringBuffer s = new StringBuffer();
		Iterator<AuditEventMatch> it = filter.getMatches().iterator();
		while( it.hasNext() ){
			AuditEventMatch m = it.next();
			s.append( m.getVariable() );
			s.append( " " );
			s.append( m.getOperator() );
			s.append( " " );
			if( m.getValue().indexOf( " " ) >= 0 ){
				s.append( "'" );
				s.append( m.getValue() );
				s.append( "'" );
			} else
				s.append( m.getValue() );
			
			if( it.hasNext() )
				s.append( " AND " );
		}
		
		return parse( s.toString() );
	}
	
	

	public final static FilterExpression parse( String str ) throws FilterException {
		log.debug( "Parsing expression: '{}'", str );
		if( str == null || str.trim().isEmpty() )
			return new AuditEventFilter();
		
		ExpressionReader r = new ExpressionReader( str );
		return r.readFilterExpression();
	}

	public final static FilterExpression parse( String str, Collection<String> variables ) throws FilterException {
		log.debug( "Parsing expression: '{}'", str );
		ExpressionReader r = new ExpressionReader( str, variables );
		return r.readFilterExpression();
	}
	
	public final static FilterExpression parse( String str, Collection<String> variables, Set<Operator> operators ) throws FilterException {
		log.debug( "Parsing expression: '{}'", str );
		ExpressionReader r = new ExpressionReader( str, variables, operators );
		return r.readFilterExpression();
	}
	
	public final static FilterExpression parse( String str, Collection<String> variables, Set<Operator> operators, Set<BooleanOperator> boolOps ) throws FilterException {
		log.debug( "Parsing expression: '{}'", str );
		ExpressionReader r = new ExpressionReader( str, variables, operators );
		r.setBooleanOperators( boolOps ); 
		return r.readFilterExpression();
	}
	
	
	public final static AuditEventFilter compile( String str ) throws FilterException {
		return compile( parse( str ) );
	}
	

	public final static AuditEventFilter compile( String str, Collection<String> variables ) throws FilterException {
		return compile( parse( str, variables ) );
	}
	
	
	public final static AuditEventFilter compile( String str, Set<String> variables, Set<Operator> operators ) throws FilterException {
		return compile( parse( str, variables, operators ) );
	}
	
	public final static AuditEventFilter compile( String str, Set<String> variables, Set<Operator> operators, Set<BooleanOperator> boolOps ) throws FilterException {
		return compile( parse( str, variables, operators, boolOps ) );
	}
	
	public final static AuditEventFilter compile( FilterExpression exp ) throws FilterException {
		
		AuditEventFilter filter = new AuditEventFilter();
		
		if( map( exp ) != null ){
			filter.add( map(exp) );
		}
		
		if( exp instanceof FilterExpressionList ){
			FilterExpressionList list = (FilterExpressionList) exp;
			for( FilterExpression e : expand( list ) ){
				if( map( e ) != null )
					filter.add( map(e) );
			}
		}
		
		return filter;
	}

	
	public static List<FilterExpression> expand( FilterExpressionList list ){
		List<FilterExpression> exp = new LinkedList<FilterExpression>();
		
		for( FilterExpression e : list.getElements() ){
			if( e instanceof FilterExpressionList )
				exp.addAll( expand( (FilterExpressionList) e ) );
			else
				exp.add( e );
		}
		
		return exp;
	}
	
	
	public final static Match map( FilterExpression e ){
		if( e instanceof Match ){
			return (Match) e;
		}
		
		if( e instanceof AuditEventMatch ){
			AuditEventMatch aem = (AuditEventMatch) e;
			return new Match( aem.getVariable(), aem.getOp(), aem.getValue() );
		}
		
		return null;
	}
	
	
	public final static String toFilterString( Object e ){
		if( e == null ){
		        return "null";
		}

		StringBuffer s = new StringBuffer();
		if( e instanceof AuditEventFilter ){
			AuditEventFilter aef = (AuditEventFilter) e;
			Iterator<AuditEventMatch> it = aef.getMatches().iterator();
			while( it.hasNext() ){
				AuditEventMatch match = it.next();
				s.append( match.getVariable() );
				s.append( " " );
				s.append( match.getOperator() );
				s.append( " " );
				s.append( match.getValueObject() );
				if( it.hasNext() ){
					s.append( " AND " );
				}
			}
			
		} else
			s.append( e.toString() );
		
		return s.toString();
	}
}
