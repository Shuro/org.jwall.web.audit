package org.jwall.web.audit.rules;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jwall.audit.FeatureExtractor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.util.QuotedStringTokenizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides an extractor method for scores to be read
 * from an event's K section.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class PersistentCollectionExtractor
	implements FeatureExtractor<AuditEvent,String>
{

	static Logger log = LoggerFactory.getLogger( PersistentCollectionExtractor.class );
	final Set<String> variables = new LinkedHashSet<String>();


	/**
	 * <p>
	 * This method extracts scores and environment variables from the given event
	 * by effectively simulating the actions fired by this event which are included
	 * in the K section of the event.
	 * </p>
	 * <p>
	 * If no K section is available, this method simply returns an empty map. 
	 * </p>
	 * 
	 * @param evt
	 * @return
	 * @throws Exception
	 */
	public static Map<String,String> extractScores( AuditEvent evt ) throws Exception {
		log.debug( "Extracting score for event {}", evt.getEventId() );
		return extractScores( evt, new LinkedHashMap<String,String>() );
	}


	public static Map<String,String> processRuleLog( String ruleLog, Map<String,String> env ){
		try {
			BufferedReader reader = new BufferedReader( new StringReader( ruleLog ) );
			String line = reader.readLine();

			while( line != null ){

				line = line.trim();
				int idx = -1;

				if( line.startsWith( "SecAction" ) ){
					idx = 1;
				}

				if( line.startsWith( "SecRule" ) ){
					idx = 3;
				}
				
				if( line.indexOf( "setvar" ) < 0 ){
					line = reader.readLine();
					continue;
				}

				if( idx > 0 ){

					List<String> actions = new ArrayList<String>();
					List<String> args = QuotedStringTokenizer.splitRespectQuotes( line.trim() , ' ' );
					String actionString = removeQuotes( args.get( idx ) );

					for( String action : QuotedStringTokenizer.splitRespectQuotes( actionString, ',' ) ){
						String act = removeQuotes( action.trim() );
						actions.add( act );
						processAction( act, env );
					}
				}
				line = reader.readLine();
			}
			reader.close();
			return env;
		} catch (Exception e) {
			log.error( "Failed to parse K-section of event:\n{}", ruleLog );
			log.error( "   Exception was: {}", e.getMessage() );
			if( log.isDebugEnabled() )
				e.printStackTrace();
		}
		return env;
	}
	
	
	/**
	 * 
	 * This method extracts scores and environment variables in the same way as the method
	 * above, but uses the specified map as an initial start to evaluate initial parameters.
	 * 
	 * @param s
	 * @return
	 */
	public static Map<String,String> extractScores( AuditEvent evt, Map<String,String> env ) throws Exception {

		String ruleLog = evt.getSection( ModSecurity.SECTION_RULE_LOG );
		if( ruleLog == null || ruleLog.length() == 0 ){
			log.debug( "No rule-log found in event! Did you forget to enable the K-section?" );
			return env;
		} else {
			log.debug( "Parsing K-section of event {}", evt.getEventId() );
		}

		try {
			BufferedReader reader = new BufferedReader( new StringReader( ruleLog ) );
			String line = reader.readLine();

			while( line != null ){

				line = line.trim();
				int idx = -1;

				if( line.startsWith( "SecAction" ) ){
					idx = 1;
				}

				if( line.startsWith( "SecRule" ) ){
					idx = 3;
				}
				
				if( line.indexOf( "setvar" ) < 0 ){
					line = reader.readLine();
					continue;
				}

				if( idx > 0 ){

					List<String> actions = new ArrayList<String>();
					List<String> args = QuotedStringTokenizer.splitRespectQuotes( line.trim() , ' ' );
					String actionString = removeQuotes( args.get( idx ) );

					for( String action : QuotedStringTokenizer.splitRespectQuotes( actionString, ',' ) ){
						String act = removeQuotes( action.trim() );
						actions.add( act );
						processAction( act, env );
					}
				}
				line = reader.readLine();
			}
			reader.close();
			return env;
		} catch (Exception e) {
			log.error( "Failed to parse K-section of event {}:\n{}", evt.getEventId(), ruleLog );
			log.error( "   Exception was: {}", e.getMessage() );
			if( log.isDebugEnabled() )
				e.printStackTrace();
			throw e;
		}
	}


	/**
	 * This method processes the given action, handles actions like setvar,
	 * and others to enrich the specified environment map.
	 * 
	 * @param action
	 * @param env
	 */
	private static void processAction( String action, Map<String,String> env ){
		if( action.startsWith( "setvar" ) ){
			String[] tok = action.split( ":", 2 );
			if( tok.length > 1 ){
				String setvar = tok[1];
				if( setvar.charAt( 0 ) == '\'' )
					setvar = setvar.substring( 1 );

				if( setvar.endsWith( "'" ) )
					setvar = setvar.substring( 0, setvar.length() - 1 );

				int idx = setvar.indexOf( "=" );
				if( idx > 0 ){
					String var = setvar.substring( 0, idx );
					String val = setvar.substring( idx + 1 );
					log.trace( "   found assignment:  {} := {}", var, val );
					String eval = eval( val, env );
					String old = env.get( var );
					if( old == null )
						old = "0";
					if( eval.startsWith( "+" ) ){
						try {
							eval = (Integer.parseInt( eval.substring( 1 ) ) + Integer.parseInt( old ) ) + "" ;
						} catch (Exception e) {
							log.error( "Parse error! Argument to setvar is not an integer: '{}'!", eval.substring(1) );
							eval = "0";
						}
					}
					if( eval.startsWith( "-" ) ){
						try {
							eval = (Integer.parseInt( old ) - Integer.parseInt( eval.substring( 1 ) ) ) + "" ;
						} catch (Exception e) {
							log.error( "Parse error! Argument to setvar is not an integer: '{}'!", eval.substring(1) );
							eval = "0";
						}
					}

					log.trace( "           setting   {} := {}", var, eval );
					env.put( var, eval );
				}
			}
			return;
		}

		log.debug( "Ignoring action {}", action );
	}




	public static String removeQuotes( String s ){
		int start = 0;
		if( s.startsWith("\"") )
			s = s.substring( ++start );

		if( s.endsWith("\"") )
			s = s.substring(0, s.length() - 1 );

		return s;
	}



	private static String eval( String str, Map<String,String> env ){
		if( str.indexOf( "%{" ) >= 0 ){
			for( String key : env.keySet() ){
				if( str.indexOf( "%{" + key + "}" ) >= 0 )
					str = str.replace( "%{" + key + "}", env.get( key ) );
			}
			return str;
		}
		return str;
	}


	@Override
	public Set<String> getVariables(AuditEvent event) {
		return variables;
	}


	@Override
	public String extract(String feature, AuditEvent event) {
		try {
			Map<String,String> scores = extractScores( event );
			String key = feature.toLowerCase().replace( ':', '.' );
			return scores.get( key );
		} catch (Exception e) {
			log.error( "Failed to extract '{}': {}", feature, e.getMessage() );
			return null;
		}
	}
}
