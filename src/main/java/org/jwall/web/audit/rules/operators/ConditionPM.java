/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules.operators;

import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.rules.Condition;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("PM")
public class ConditionPM
extends AbstractCondition
{

	/** The unique class ID */
	private static final long serialVersionUID = 125867770986401362L;

	public ConditionPM(String variable, String value)
	throws SyntaxException
	{
		super(variable, value);
	}

	public String getOperator(){
		return Condition.PM;
	}


	/**
	 * @see org.jwall.web.audit.rules.Condition#matches(java.lang.String, java.lang.String)
	 */
	public boolean matches( String pattern, String input ){
		String[] ps = pattern.split( "\\s+" );

		for( String p : ps )
			if( input.indexOf( p ) >= 0 )
				return true;
		
		return false;
	}
}