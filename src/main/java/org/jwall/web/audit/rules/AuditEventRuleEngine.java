package org.jwall.web.audit.rules;

import java.util.Map;

import org.jwall.audit.rules.EventRule;
import org.jwall.audit.rules.EventRuleEngine;
import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * A rule engine for handling audit-event data.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditEventRuleEngine 
	extends EventRuleEngine<AuditEvent> 
{
	static Logger log = LoggerFactory.getLogger( AuditEventRuleEngine.class );

	/**
	 * @see org.jwall.audit.rules.EventRuleEngine#execute(org.jwall.audit.rules.EventRule, org.jwall.audit.Event)
	 */
	@Override
	protected void execute(EventRule<AuditEvent> rule, AuditEvent event, Map<String,Object> context ) {
		if( rule instanceof AuditEventRule ){
			AuditEventRule r = (AuditEventRule) rule;
			for( EventAction act :  r.getActions() ){
				log.trace( "Executing rule-action {}", act );
				try {
					act.execute( context, event );
				} catch (Exception e) {
					log.error( "Failed to execute action {} for event {}", act, event );
					log.error( "   Error is: {}", e.getMessage() );
					if( log.isDebugEnabled() )
						e.printStackTrace();
				}
			}
		}
	}
}
