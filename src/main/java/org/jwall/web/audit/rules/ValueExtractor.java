/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jwall.audit.FeatureExtractor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.MessageParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;

/**
 * <p>
 * This class extracts the values for the given variable for an event.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class ValueExtractor implements
		FeatureExtractor<AuditEvent, List<String>> {
	static Logger log = LoggerFactory.getLogger(ValueExtractor.class);

	/** This class is a singleton with only one final static instance */
	final static ValueExtractor extractor = new ValueExtractor();

	/** The set of variables which require more attention (parsing) */
	final Set<String> needsParsing = new HashSet<String>();

	/** The set of variables that this extractor is able to extract */
	final Set<String> variables = new LinkedHashSet<String>();

	final Map<String, FeatureExtractor<AuditEvent, ?>> extractors = new LinkedHashMap<String, FeatureExtractor<AuditEvent, ?>>();

	/**
	 * Create a new extractor and initialize the available variables.
	 * 
	 */
	protected ValueExtractor() {

		// this extractor provides support for all ModSecurity variables
		//
		for (String var : ModSecurity.VARIABLES)
			variables.add(var);

		// and additionally all jwall-audit-event variables
		// TODO: We should extract-check whether FILE, FILE_OFFSET and SIZE are
		// always available?
		//
		for (String var : AuditEvent.VARIABLES)
			variables.add(var);

		//
		// add all variables that require detailed parsing of the audit-event
		//
		needsParsing.add(ModSecurity.RULE_SEV);
		needsParsing.add(ModSecurity.RULE_MSG);
		needsParsing.add(ModSecurity.RULE_TAG);
		needsParsing.add(ModSecurity.RULE_ID);
		needsParsing.add(ModSecurity.RULE_DATA);

		needsParsing.add(ModSecurity.HIGHEST_SEVERITY);
		needsParsing.add(ModSecurity.LOWEST_SEVERITY);

		needsParsing.add(AuditEvent.SEVERITY);
		needsParsing.add(AuditEvent.MESSAGE);
	}

	public void registerExtractor(String name,
			FeatureExtractor<AuditEvent, ?> extr) {
		if (extractors.containsKey(name)) {
			log.warn("Overriding existing feature-extractor '{}'",
					extractors.get(name));
		}

		log.debug("Adding extractor '{}' for feature {}", extr, name);
		extractors.put(name, extr);
	}

	/**
	 * @see org.jwall.audit.FeatureExtractor#getVariables(org.jwall.audit.Event)
	 */
	@Override
	public Set<String> getVariables(AuditEvent event) {
		return Collections.unmodifiableSet(variables);
	}

	/**
	 * @see org.jwall.audit.FeatureExtractor#extract(java.lang.String,
	 *      org.jwall.audit.Event)
	 */
	@Override
	public List<String> extract(String var, AuditEvent evt) {

		List<String> values = new LinkedList<String>();
		if (var == null || evt == null)
			return values;

		String variable = var;
		boolean extracted = false;

		if (variable.startsWith("&"))
			variable = variable.substring(1);

		if (variable.startsWith("&")) {
			log.debug(
					"Extracting special variable '{}', which refers to value-counting",
					variable);
			Integer count = new Integer(evt.getAll(variable.substring(1))
					.size());
			values.add(count.toString());
			log.debug("  => Result is '{}'", values);
			extracted = true;
		}

		if (AuditEvent.AGE.equals(variable)) {
			//
			// compute the age of the event based on the "now" ;-)
			//
			Date d = evt.getDate();
			if (d == null)
				values.add(Long.toString(System.currentTimeMillis())); // treat
																		// a
																		// null-date
																		// a 0L
			else
				values.add(Long.toString(System.currentTimeMillis()
						- d.getTime()));
			extracted = true;
		}

		if (extractors.containsKey(variable)) {

			FeatureExtractor<AuditEvent, ?> extractor = extractors
					.get(variable);
			Object feature = extractor.extract(variable, evt);

			if (feature instanceof Collection) {
				Collection<?> fs = (Collection<?>) feature;
				for (Object o : fs) {
					values.add(o.toString());
				}
			} else {
				values.add(feature.toString());
			}
			extracted = true;

		}

		if (needsMessageParsing(variable)) {
			log.debug("   need to parse event details");
			try {
				List<AuditEventMessage> msgs = MessageParser.parseMessages(evt);

				for (AuditEventMessage msg : msgs) {
					log.debug("Processing event-message: {}", msg.toString());

					if (ModSecurity.RULE_SEV.equals(variable)
							|| AuditEvent.SEVERITY.equals(variable)) {
						if (msg.getSeverity() != null
								&& msg.getSeverity() >= 0
								&& msg.getSeverity() != ModSecurity.SEVERITY_NOT_SET)
							values.add(msg.getSeverity().toString());
					}

					if (ModSecurity.RULE_MSG.equals(variable)) {
						if (msg.getRuleMsg() != null)
							values.add(msg.getRuleMsg());
					}

					if (ModSecurity.RULE_ID.equals(variable)) {
						if (msg.getRuleId() != null)
							values.add(msg.getRuleId().toString());
					}

					if (ModSecurity.RULE_TAG.equals(variable)) {
						if (msg.getRuleTags() != null)
							values.addAll(msg.getRuleTags());
					}

					if (ModSecurity.RULE_FILE.equals(variable)) {
						if (msg.getFile() != null)
							values.add(msg.getFile());
					}

					if (ModSecurity.RULE_DATA.equals(variable)) {
						if (msg.getRuleData() != null)
							values.add(msg.getRuleData());
					}

					if (AuditEvent.MESSAGE.equals(variable)) {
						if (msg.getText() != null)
							values.add(msg.getText());
					}
				}

				if (AuditEvent.SEVERITY.equals(variable) && values.isEmpty()) {
					values.add("255");
				}
				log.debug("Extracted values for '{}': {}", variable, values);

			} catch (Exception e) {
				e.printStackTrace();
			}
			extracted = true;

		}

		if (!extracted)
			values = evt.getAll(variable);

		log.debug("Extracted values for '{}': {}", variable, values);

		if (var.startsWith("&")) {
			String count = String.valueOf(values.size());
			values.clear();
			values.add(count);
		}

		return values;
	}

	private boolean needsMessageParsing(String variable) {
		return needsParsing.contains(variable);
	}

	public static List<String> extractValues(String variable, AuditEvent evt) {
		return extractor.extract(variable, evt);
	}

	public static List<String> extractValues(String variable, Data item) {
		List<String> vals = new ArrayList<String>();

		Serializable val = item.get(variable);
		if (val == null)
			return vals;

		if (val.getClass().isArray()) {

			int len = Array.getLength(val);
			for (int i = 0; i < len; i++) {
				Object valx = Array.get(val, i);
				if (valx != null) {
					vals.add(valx.toString());
				}
			}

			return vals;
		}

		if (val instanceof Collection) {
			Collection<?> col = (Collection<?>) val;
			for (Object o : col) {
				vals.add(o.toString());
			}
			return vals;
		}

		vals.add(val.toString());
		return vals;
	}
}