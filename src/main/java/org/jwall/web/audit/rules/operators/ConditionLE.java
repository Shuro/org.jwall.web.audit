/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules.operators;

import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.rules.Condition;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("LE")
public class ConditionLE
    extends AbstractCondition
{
    /** The unique class ID */
    private static final long serialVersionUID = -6196215282881485160L;

    public ConditionLE(String variable, String value) throws SyntaxException
    {
        super(variable, value);
    }
    
    public String getOperator(){
        return Condition.LE;
    }
    

    /**
     * @see org.jwall.web.audit.rules.Condition#matches(java.lang.String, java.lang.String)
     */
    public boolean matches( String pattern, String input ){
    	if( isNumeric( pattern ) ){
    		try {
    			return (new Double( pattern ).compareTo( new Double( input ) ) ) <= 0;
    		} catch (Exception e) {
    		}
    	}
    	
    	return pattern.compareTo( input ) >= 0;
    }
}