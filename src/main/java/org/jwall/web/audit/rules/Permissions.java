package org.jwall.web.audit.rules;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is provided by rule-actions and provides a hint to rule-engines
 * which permissions should be available before executing an action.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@Target( { java.lang.annotation.ElementType.TYPE } )
@Retention( RetentionPolicy.RUNTIME )
public @interface Permissions {
	String[] value();
}