/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules.operators;

import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.filter.Operator;
import org.jwall.web.audit.rules.Condition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.annotations.XStreamAlias;


/**
 * <p>
 * Instances of this class check all values for the given variable if they
 * start with a pre-defined substring.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias( "beginsWith" )
public class BeginsWith
extends AbstractCondition
implements Condition
{
    /** The unique class ID */
    private static final long serialVersionUID = 6247657062073419253L;
    static Logger log = LoggerFactory.getLogger( BeginsWith.class );
    
    public BeginsWith( String var, String exp ) throws SyntaxException {
        super( var, exp );
    }

    
    public boolean matches( String pattern, String value ){
    	return value != null && value.startsWith( pattern );
    }
    

    public String getOperator()
    {
        return Operator.BeginsWith.toString();
    }
}