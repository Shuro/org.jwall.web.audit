/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.swing.text.html.HTMLDocument.HTMLReader.TagAction;

import org.jwall.audit.rules.EventRule;
import org.jwall.audit.rules.RuleContext;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.filter.AuditEventMatch;
import org.jwall.web.audit.rules.operators.BeginsWith;
import org.jwall.web.audit.rules.operators.ConditionEQ;
import org.jwall.web.audit.rules.operators.ConditionGE;
import org.jwall.web.audit.rules.operators.ConditionGT;
import org.jwall.web.audit.rules.operators.ConditionLE;
import org.jwall.web.audit.rules.operators.ConditionLT;
import org.jwall.web.audit.rules.operators.ConditionRX;
import org.jwall.web.audit.rules.operators.ConditionSX;
import org.jwall.web.audit.rules.operators.Contains;
import org.jwall.web.audit.rules.operators.EndsWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


/**
 * <p>
 * This class implements a simple rule, which can be applied to an audit-event and may
 * trigger a set of actions. Thus, basically an event-rule is an event-filter plus some
 * event-actions. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
@XStreamAlias("EventRule")
public class AuditEventRule
	implements EventRule<AuditEvent>
{
    /* This constant requires at least one filter to match for this rule to trigger */
    public final static String MATCH_ANY_FILTER = "any";

    /* This constant requries ALL filters to match for this rule to trigger */
    public final static String MATCH_ALL_FILTERS = "all";

    
    public final static Class<?>[] CLASSES = {
        AuditEventRule.class,
        AuditEventMatch.class,

        ConditionEQ.class,
        ConditionLT.class,
        ConditionLE.class,
        ConditionGT.class,
        ConditionGE.class,
        ConditionRX.class,
        ConditionSX.class,
        BeginsWith.class,
        EndsWith.class,
        Contains.class,
        

        TagAction.class,
    };
    
    static Logger log = LoggerFactory.getLogger( AuditEventRule.class );

    @XStreamAlias("enabled")
    @XStreamAsAttribute
    Boolean enabled = true;
    
    /* describes the mode for matching events */
    @XStreamAsAttribute
    @XStreamAlias("match")
    String match = null;
    
    @XStreamAlias( "Comment" )
    String comment = null;
    
    /* The list of filters that must match an event for this rule to trigger the actions */
    @XStreamAlias("Conditions")
    LinkedList<Condition> conditions = new LinkedList<Condition>();

    /* This is the list of events that will be triggered if this rule matches */
    @XStreamAlias("Actions")
    LinkedList<EventAction> actions = new LinkedList<EventAction>();

    public AuditEventRule(){
        match = MATCH_ALL_FILTERS;
    }

    /**
     * This method checks whether the rule triggers for the given event.
     * 
     * @param evt The event to test the rule on.
     * @return <code>true</code> if the rule matches the given event, <code>false</code> otherwise.
     */
    public boolean matches( AuditEvent evt, RuleContext ctx ){
        
        log.debug( "checking rule-match on event '" + evt.getEventId() + "'" );
        
        //
        // MATCH_ANY_FILTER means we are finished as soon as the first filter matches
        //
        if( MATCH_ANY_FILTER.equalsIgnoreCase( match ) ){

            log.debug( "ScriptEvent needs to match at least one filter (rule contains " + conditions.size()  + ")" );

            for( Condition condition : conditions ){
                if( condition.matches( evt ) ){
                    log.debug( "filter matched event! Rule needs to trigger!" );
                    return true;
                }
            }

            //
            // if no filter matched we don't trigger...
            //
            log.debug( "No matching filter found! Rule does not fire!" );
            return false;

        } else {
            //
            // otherwise we return false of ANY filter does NOT match
            //
            log.debug( "ScriptEvent needs to match all filters (rule has " + conditions.size() + " filters)" );

            for( Condition condition : conditions )
                if( ! condition.matches( evt ) ){
                    log.debug( "Found non-matching filter! Rule will not fire!" );
                    return false;
                }

            //
            // if no filter mis-matched, the rule should trigger...
            //
            log.debug( "All filters matched! rule needs to trigger its actions!" );
            return true;
        }
    }




    /**
     * @return the enabled
     */
    public boolean isEnabled()
    {
        if( enabled == null )
            return enabled = true;
        return enabled;
    }



    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }



    /**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
     * @return the match
     */
    public String getMatch()
    {
        return match;
    }



    /**
     * @param match the match to set
     */
    public void setMatch(String match)
    {
        if( MATCH_ALL_FILTERS.equals(match) || MATCH_ANY_FILTER.equals( match ) )
            this.match = match;
    }

    
    public boolean isMatchAll(){
    	return MATCH_ALL_FILTERS.equals( match );
    }

    
    public void setMatchAll( boolean b ){
    	if( b )
    		match = MATCH_ALL_FILTERS;
    	else
    		match = MATCH_ANY_FILTER;
    }
    

    /**
     * @return the filters
     */
    public List<Condition> getConditions()
    {
        if( conditions == null )
            conditions = new LinkedList<Condition>();
        
        log.debug( "Returning " + conditions.size() + " conditions." );
        return conditions;
    }


    /**
     * @param filters the filters to set
     */
    public void setConditions(List<Condition> filters)
    {
        if( conditions == null )
            this.conditions = new LinkedList<Condition>();
        else
            this.conditions = new LinkedList<Condition>( filters );
    }


    public boolean add( Condition filter ){
        log.debug( "Adding condition ");
        
        if( conditions == null )
            conditions = new LinkedList<Condition>();

        if( ! conditions.contains( filter ) ){
            conditions.add( filter );
            return true;
        } else
            return false;
    }

    public boolean remove( Condition filter ){
        if( conditions == null )
            return false;

        Object o = conditions.remove( filter );
        return o != null;
    }

    public boolean removeCondition( int i ){
        if( conditions != null & i >= 0 && i < conditions.size() ){
            conditions.remove( i );
            return true;
        }

        return false;
    }


    public boolean add( EventAction action ){
        if( actions == null )
            actions = new LinkedList<EventAction>();

        if( ! actions.contains( action ) ){
            actions.add( action );
            return true;
        }
        
        return false;
    }


    public boolean remove( EventAction action ){
        if( actions == null )
            return false;

        Object o = actions.remove( action );
        return o != null;
    }

    public boolean removeAction( int i ){
        if( actions != null && i >= 0 && i < actions.size() ){
            actions.remove( i );
            return true;
        }

        return false;
    }

    /**
     * @return the actions
     */
     public List<EventAction> getActions()
     {
         return actions;
     }


     /**
      * @param actions the actions to set
      */
     public void setActions(Set<EventAction> actions)
     {
         if( actions == null )
             actions = new LinkedHashSet<EventAction>();
         else
             actions = new LinkedHashSet<EventAction>( actions );
     }


     public boolean equals( Object o ){
         if( this == o )
             return true;

         if( o instanceof AuditEventRule ){
             AuditEventRule rule = (AuditEventRule) o;
             return this.toString().equals(  rule.toString() );
         }

         return false;
     }


     public int hashCode(){
         return toString().hashCode();
     }


     public Object readResolve(){
         if( this.enabled == null )
             this.enabled = new Boolean( true );
         return this;
     }
     
     public String toString(){
         StringBuffer s = new StringBuffer();

         s.append( "{ IF\n" );

         Iterator<Condition> it = conditions.iterator();
         while( it.hasNext() ){
             Condition filter = it.next();
             s.append( "\t" + filter + "\n" );

             if( it.hasNext() ){
                 if( MATCH_ALL_FILTERS.equalsIgnoreCase( match ) )
                     s.append( " AND " );
                 else
                     s.append( "  OR " );
             }
         }

         s.append( "\nTHEN\n" );

         Iterator<EventAction> i = actions.iterator();
         while( i.hasNext() ){

             EventAction act = i.next();
             s.append( "\t" + act + "\n" );
             if( i.hasNext() )
                 s.append( "\t" );
         }
         s.append( "}" );
         return s.toString();
     }
}
