/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.rules.operators;

import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.filter.Operator;
import org.jwall.web.audit.rules.Condition;
import org.jwall.web.audit.rules.ValueExtractor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;


/**
 * <p>
 * The abstract stuff of a condition instance.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public abstract class AbstractCondition
    implements Condition
{
    /** The unique class ID */
    private static final long serialVersionUID = 1950679429132740380L;

    /* */
    static Logger log = LoggerFactory.getLogger( AbstractCondition.class );
    
    @XStreamAlias("variable")
    @XStreamAsAttribute
    String variable;
    
    @XStreamAlias("value")
    @XStreamAsAttribute
    String value;
    
    
    public AbstractCondition( String variable, String value )
        throws SyntaxException
    {
        this.variable = variable;
        this.value = value;
        
        if( this.variable == null )
            throw new SyntaxException( "Variable must not be null!" );
        
        if( this.value == null )
            throw new SyntaxException( "Value for condition must not be null!" );
    }

    
    /**
     * @see org.jwall.web.audit.rules.Condition#extractValues(org.jwall.web.audit.AuditEvent)
     */
    public List<String> extractValues( AuditEvent evt ){
        List<String> values = ValueExtractor.extractValues( this.getVariable(), evt ); //. new LinkedList<String>();
        return values;
    }

    
    /**
     * 
     * @deprecated
     */
    public final boolean matches( AuditEvent evt ){
        List<String> values = extractValues( evt );
        return matches( values );
    }
    
    
    public final boolean matches( List<String> values ){
    	for( String val : values ){
    		if( !isNegated() && matches( val ) )
    			return true;
    	}
    	return false;
    }
    
    public final boolean matches( String val ){
    	if( isNegated() )
    		return ! matches( getPureValue(), val );
    	
    	return matches( getPureValue(), val );
    }
    
    
    /**
     * @return the variable
     */
    public String getVariable()
    {
        return variable;
    }

    /**
     * @param variable the variable to set
     */
    public void setVariable(String variable)
    {
        this.variable = variable;
    }


    /**
     * @return the value
     */
    public String getValue()
    {
        return value;
    }


    /**
     * @param value the value to set
     */
    public void setValue(String value)
    {
        this.value = value;
    }
    
    
    public String toString(){
        return getVariable() + " " + getOperator() + " " + getValue();
    }
    
    
    public final String getPureValue(){
    	if( value != null && value.startsWith( "!" ) )
    		return value.substring( 1 );
    	return value;
    }
    
    
    public final boolean isNegated(){
    	return this.value != null && this.value.startsWith( "!" );
    }
    
    
    public static boolean isNumeric( String s ){
    	try {
    		String str = s;
    		if( str.startsWith( "!" ) )
    			str = str.substring( 1 );
    		Double.parseDouble( str );
    		return true;
    	} catch (Exception e) {
    		return false;
    	}
    }
    
    
    public abstract String getOperator();
    

    public static Condition createCondition( Operator oper, String variable, String value ) throws SyntaxException {
    	
    	switch( oper ){
    		case RX: return new ConditionRX( variable, value );
    		case EQ: return new ConditionEQ( variable, value );
    		case LT: return new ConditionLT( variable, value );
    		case LE: return new ConditionLE( variable, value );
    		case GT: return new ConditionGT( variable, value );
    		case GE: return new ConditionGE( variable, value );
    		case PM: return new ConditionPM( variable, value );
    		case SX: return new ConditionSX( variable, value );
    		case BeginsWith: return new BeginsWith( variable, value );
    		case EndsWith: return new EndsWith( variable, value );
    		case Contains: return new Contains( variable, value );
    		case IN: return new ConditionIN( variable, value );
    	}
        
        throw new SyntaxException( "Operator not supported!" );
    }
}
