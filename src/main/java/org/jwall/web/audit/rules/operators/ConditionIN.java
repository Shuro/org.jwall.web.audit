package org.jwall.web.audit.rules.operators;

import org.jwall.web.audit.SyntaxException;
import org.jwall.web.audit.filter.Operator;


/**
 * <p>
 * This class implements a condition called <code>@in</code>, which basically matches,
 * if one of a set of SX operators matches.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ConditionIN extends ConditionSX {

	/** The unique class ID */
	private static final long serialVersionUID = 2437965899355955280L;

	public ConditionIN(String var, String exp) throws SyntaxException {
		super(var, exp);
	}

	/**
	 * @see org.jwall.web.audit.rules.operators.ConditionSX#getOperator()
	 */
	@Override
	public String getOperator() {
		return Operator.IN.toString();
	}

	/**
	 * @see org.jwall.web.audit.rules.operators.ConditionSX#matches(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean matches(String pattern, String input) {
		if( pattern.indexOf( "," ) >= 0 ){
			String[] patterns = pattern.split( "," );
			for( String p : patterns ){
				if( super.matches( p, input ) )
					return true;
			}
			return false;
		} else
			return super.matches(pattern, input);
	}
}