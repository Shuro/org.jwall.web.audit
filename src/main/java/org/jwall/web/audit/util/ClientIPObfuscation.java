/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.jwall.web.audit.ModSecurity;


/**
 * <p>
 * This class implements a simple client IP address obfuscation. It will check the audit-log header
 * for the client IP and replace this with a <code>x.x.x.x</code>-string in all subsequent sections
 * as well. 
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ClientIPObfuscation
    implements Obfuscator
{
    
    public final static String IPv4_ADDRESS_PATTERN = "\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}";
    public final static String IPv6_ADDRESS_PATTERN = "[a-f0-9]{0,4}:[a-f0-9]{1,4}";
    
    
    static String IPv4_REPLACEMENT = System.getProperty( "ipv4.replacement" );
    static String IPv6_REPLACEMENT = System.getProperty( "ipv6.replacement" );
    
    static Pattern ipv6address = Pattern.compile( IPv6_ADDRESS_PATTERN );
    static Pattern ipv4address = Pattern.compile( IPv4_ADDRESS_PATTERN );

    
    List<Integer> sections = new LinkedList<Integer>();
    
    String lastClientIP = null;
    String lastClientPort = null;
    String lastServerIP = null;
    String lastServerPort = null;
    Map<String,String> replacements = new HashMap<String,String>();
    
    
    /**
     * 
     */
    public ClientIPObfuscation(){
        sections.add( ModSecurity.SECTION_AUDIT_LOG_HEADER );
        sections.add( ModSecurity.SECTION_REQUEST_HEADER );
        sections.add( ModSecurity.SECTION_AUDIT_TRAILER );
        sections.add( ModSecurity.SECTION_RULE_LOG );
        
        if( IPv4_REPLACEMENT == null )
            IPv4_REPLACEMENT = "x.x.x.x";
        
        if( IPv6_REPLACEMENT == null )
            IPv6_REPLACEMENT = "ffff::f";
    }
    
    public List<Integer> getSections(){
        return sections;
    }

    
    public String obfuscate( Integer id, String sectionData ) throws IOException {
        
        if( ! sections.contains( id ) )
            return sectionData;
        
        if( id == ModSecurity.SECTION_AUDIT_LOG_HEADER ){
            
            
            //
            // extract remote-addr, remote-port, server-addr, server-port from AuditLog-Header
            //            0    1    2      3          4           5           6   
            //    s[] = [date, TZ], ID, Client-IP, Client-Port, Server-IP, Server-Port
            String[] s = sectionData.trim().split(" ");
            
            lastClientIP = s[3];
            if( lastClientIP.indexOf( ":" ) >= 0 )
                replacements.put( lastClientIP, IPv6_REPLACEMENT );
            else
                replacements.put( lastClientIP, IPv4_REPLACEMENT );
            
            lastClientPort = s[4];
            lastServerIP = s[5];
            lastServerPort = s[6];
            
            String obfuscated = sectionData.replace( lastClientIP, getReplacement() ); // sectionData.replaceFirst( getExpression(), getReplacement() );
            return obfuscated;
        }
        
        
        if( id == ModSecurity.SECTION_REQUEST_HEADER || id == ModSecurity.SECTION_RULE_LOG || id == ModSecurity.SECTION_AUDIT_TRAILER ){
            
            StringBuffer obfuscated = new StringBuffer();
            BufferedReader r = new BufferedReader( new StringReader( sectionData ) );
            String line = r.readLine();
            while( line != null ){
                
                if( line.toLowerCase().startsWith( "x-forwarded-for:" ) ){
                    line = line.replaceAll( IPv4_ADDRESS_PATTERN, IPv4_REPLACEMENT );
                }
                
                if( lastClientIP != null && line.indexOf( lastClientIP ) >= 0 ){
                    line = line.replace( lastClientIP, replacements.get( lastClientIP ) );
                } 
                
                obfuscated.append( line );
                obfuscated.append( "\r\n" );
                line = r.readLine();
            }
            
            return obfuscated.toString();
        }
        
        return sectionData;
    }
    

    public String getExpression()
    {
        return IPv4_ADDRESS_PATTERN;
    }

    public String getReplacement()
    {
        return "x.x.x.x";
    }

    public String getVariable()
    {
        return ModSecurity.REMOTE_ADDR;
    }
    
    
    /**
     * @see org.jwall.web.audit.util.Obfuscator#done()
     */
    public void done(){
        this.lastClientIP = null;
    }
}
