/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.IOException;
import java.util.List;


/**
 * 
 * This is the interface of all obfuscator implementations. Usually an obfuscator provides
 * a list of sections in which it will replace stuff. The main method for obfucation is
 * {@link obfuscate(Integer,String)}.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface Obfuscator
{
    
    
    /**
     * 
     * 
     * @return
     */
    public List<Integer> getSections();
    
    
    /**
     * This method does the obfuscation on the given section data. 
     * 
     * @param sectionId The ID of the section data, given as additional parameter.
     * @param sectionData The section data.
     * @return The obfuscated section data.
     * @throws IOException In case an I/O error occurs.
     */
    public String obfuscate( Integer sectionId, String sectionData ) throws IOException;
    
    
    /**
     * This method is called once all sections have been given to the obfuscator. Usually
     * this will be called one the Z-section has been reached. It may be used to release
     * all context-sensitive stuff being hold within the obfuscator instance.
     */
    public void done();
}
