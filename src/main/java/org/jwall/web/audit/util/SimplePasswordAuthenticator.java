/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;


/**
 * 
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class SimplePasswordAuthenticator
    implements Authenticator
{
    protected Properties users;
    protected Properties groups;
    protected MessageDigest md = null;
    
    
    /**
     * This creates a new authenticator which reads users from the given file <code>usersFile</code>.
     * The group information is read from <code>groupFile</code>.
     * 
     * The user file is expected to have one line of the format
     * <pre>
     *     user=password
     * </pre>
     * 
     * for each user. The password is supposed to be in plaintext. The group file is supposed to have a 
     * line for each group, i.e.
     * <pre>
     *     group1=user1,user2,user3
     * </pre>
     * 
     * Similar to the standard unix password/group format.
     * 
     * @param usersFile
     * @param groupFile
     * @throws IOException
     */
    public SimplePasswordAuthenticator( Properties usersFile, Properties groupFile ) {
        users = usersFile;
        groups = groupFile;
    }

    
    /**
     * This creates a new authenticator in the same manner as the above constructor does. It additionally
     * allows for the specification of a message digest algorithm, which is used to compute a digest of
     * the password before comparing it during authentication.
     * 
     * @param usersFile
     * @param groupFile
     * @param messageDigest
     * @throws IOException
     */
    public SimplePasswordAuthenticator( Properties usersFile, Properties groupFile, MessageDigest messageDigest ) {
        this( usersFile, groupFile );
        md = messageDigest;
    }
    
    
    /**
     * This method checks the given login and password to the internal user-information. If a message digest
     * was specified during the creation of this authenticator, the given digest of the given password is
     * computed before comparison.
     * 
     * @see org.jwall.web.audit.util.Authenticator#authenticate(java.lang.String, java.lang.String)
     */
    public boolean authenticate(String login, String password)
    {
        String pass = password;
        if( md != null )
            pass = new String( md.digest( password.getBytes() ) );
        
        return users.get( login ) != null && users.get( login ).equals( pass );
    }


    /**
     * @see org.jwall.web.audit.util.Authenticator#getRoles(java.lang.String)
     */
    public List<String> getRoles(String user)
    {
        if( user == null || groups.get( user )  == null )
            return new LinkedList<String>();
        
        List<String> roles = new LinkedList<String>();

        String[] s = groups.get( user ).toString().split(",");
        
        for( String role : s )
            roles.add( role.trim() );
        
        return roles;
    }

    
    /**
     * @see org.jwall.web.audit.util.Authenticator#isUserInRole(java.lang.String, java.lang.String)
     */
    public boolean isUserInRole(String user, String role)
    {
        return getRoles( user ).contains( role );
    }
}
