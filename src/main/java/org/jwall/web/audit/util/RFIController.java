/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;
import java.util.Properties;


/**
 * 
 * This class is a simple controller which reads commands from standard input
 * and allows sending commands to running rfi-collectors as well as querying
 * statistics, stopping them and creating new ones.
 * 
 * 
 * 
 * @author chris@jwall.org
 *
 */
public class RFIController
{
    BufferedReader reader;
    Registry registry;
    boolean run = true;
    RFICollectorService server;
    Properties env = new Properties();
    
    public RFIController() throws Exception {
        reader = new BufferedReader( new InputStreamReader( System.in ) );
        
        System.out.println("Obtaining registry...");
        registry = LocateRegistry.getRegistry( "127.0.0.1", 1099 );
        System.out.println("Registry is " + registry );
        
        server = (RFICollectorService) registry.lookup( RFICollectorService.SERVICE_NAME );
        System.out.println("server = " + server );
        
        env.setProperty( RFICollector.PROPERTY_DOWNLOAD, "false" );
        env.setProperty( RFICollector.PROPERTY_DATA_DIR, "/tmp" );
        env.setProperty( RFICollector.PROPERTY_SITE_DIRS, "false" );
        env.setProperty( RFICollector.PROPERTY_FILES_ONLY, "true" );
    }
    
    
    
    
    public String eval( String line ) throws Exception {
        
        if( line.startsWith( "list" ) ){
            StringBuffer s =new StringBuffer();
            
            List<String> c = server.listCollectors();
            for( String name : c )
                s.append( name + "\n" );
            
            return s.toString();
        }
        
        if( line.startsWith( "drop" ) ){
            
            String[] args = line.split("\\s+");
            if( args.length == 2 ){
                
                server.unregisterCollector( args[1] );
                return "";
            }
        }
        
        if( line.startsWith( "create" ) ){
            
            String[] args = line.split("\\s+");
            if( args.length == 2 ){
                
                Properties p = new Properties();
                for( Object o : env.keySet() )
                    p.setProperty( o.toString(), env.getProperty( o.toString() ) );
                
                p.setProperty( RFICollector.PROPERTY_LOG_FILE, args[1] );
                
                System.out.println("Creating collector for file " + p.getProperty( RFICollector.PROPERTY_LOG_FILE ) );
                
                server.registerCollector( args[1], p );
            } else {
                System.out.println( "The \"create\" command needs exactly 2 parameters:");
                System.out.println( "\taccess-log  -  the file to read from" );
                System.out.println( "\tproperties  -  a file to read the properties from.");
            }
        }
        
        if( line.startsWith("set") ){
            
            String[] args = line.split("\\s+");
            
            if( args.length == 1 ){
                
                StringBuffer s = new StringBuffer("Current environment:\n");
                for( Object o : env.keySet() )
                    s.append("\t" + o.toString() + " = " + env.get( o ) + "\n" );
                
                return s.toString();
            }
            
            if( args.length != 3 ){
                
                return "Error: The \"set\" commands needs at least a variable-name and a value!";
                
            } else {
                
                String name = args[1];
                if( ! name.startsWith( "org.jwall." ) )
                    name = "org.jwall." + name;
                
                env.setProperty( name, args[2] );
                
            }
        }
        
        if( line.startsWith( "exit" ) || line.startsWith( "quit" ) ){
            run = false;
        }
        
        return "";
    }

    public String readLine() throws Exception {
        return reader.readLine();
    }
    
    public void run(){

        try {
            String line = null;
            
            while( run ){
                System.out.print("#> ");
                
                line = readLine();
                if( line != null ){
                    System.out.print( eval( line ) );
                }
            }
        
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        try {
            RFIController ctrl = new RFIController();
            ctrl.run();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
