/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.Hashtable;

/**
 * 
 * This class provides a set of static methods that are useful for parsing strings,
 * messages et. al.
 * 
 * It also encapsulates handling/parsing of mime-messages.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ParserUtils
{
    /**
     * Splits a string which might contain quoted substrings.
     * This is a simple wrapper around <code>splitQuotedString(input, &quot;)</code>.
     * 
     * @param input The string to split
     * @return An array of tokens of type string.
     */
    public static String[] splitQuotedString(String input){
        return splitQuotedString(input, String.valueOf('"'), " ");
    }    
    
    public static String[] splitQuotedString( String input, String pattern ){
        return splitQuotedString(input, String.valueOf('"'), pattern );
    }

    /**
     * This method splits a possible quoted String into tokens, respecting the quotations.
     * The returned array of strings does not contain the limiting quotations.
     * 
     * @param input A string, possibly containing quotes.
     * @return An array of strings which are tokens from the splitted input-string.
     */
    public static String[] splitQuotedString(String input, String quoteChar, String pattern){
        //
        // TODO: this needs more tuning
        // 

        StringBuffer line = new StringBuffer(input);

        String[] tok = new String[255];
        for(int i = 0; i < tok.length; i++)
            tok[i] = "";

        int i = 0;
        int k = 0;
        do {
            i = line.indexOf("\"", i);
            if(i >= 0){
                int end = line.indexOf("\"", i+1) + 1;
                if(end < 0)
                    end = line.length();
                tok[k] = line.substring(i, end);
                line.replace(i, end, "$"+k);
                k++;
                i = end;
            }
        } while (i > 0 && i < line.length());

        String[] parts = line.toString().split( pattern );
        for(int n = 0; n < parts.length; n++){
            if( parts[n].matches("\\$\\d") ){
                String id = parts[n].replaceAll("\\$", "");
                int idx = Integer.parseInt( id );
                parts[n] = tok[idx];
            }

        }

        return parts;
    }



    /**
     * Parses the QueryString and returns a hash of (param,value)-pairs. The 
     * parameters-pairs are split using <code>&amp;</code>, parameters and values
     * will be split using <code>=</code>.
     * 
     * A possible trailing string starting with <code>#</code> will be ignored. 
     * 
     * @param qs The query-string that will be parsed. 
     */
    public static Hashtable<String,String> parseQueryString(String qs)
    {
        Hashtable<String,String> params = new Hashtable<String,String>();
        int l = qs.indexOf("#");
        String[] pairs;
        if(l > 0)
            pairs = qs.substring(0,l).split("\\&");
        else
            pairs = qs.split("\\&");

        for(int i = 0; i < pairs.length; i++){
            String s[] = pairs[i].split("=", 2);
            String value = "";
            if(s.length > 1)
                value = s[1];

            if(! "".equals(s[0]))
                params.put(s[0], value);
        }

        return params;
    }

    /**
     * This method extracts multipart/form-data from a http-request-body.
     * 
     * @param contentType The content-type.
     * @param content The content that is to be decoded.
     * @return A string containing the form-data or an empty string with no request-body is available.
     * @throws Exception
     */
    public static String extractFormData(String contentType, String content)
    throws Exception
    {
        StringBuffer data = new StringBuffer();

        try {

            if(contentType == null)
                return "";

            int idx = contentType.indexOf("boundary=") + 9;
            if( idx < 0 || idx > contentType.length() )
                throw new Exception("");

            String boundary = contentType.substring(idx); 

            BufferedReader r = new BufferedReader(new StringReader( content ));

            String line = r.readLine();  // line is now the seperator

            do {
                line = r.readLine();  // empty line
                if(line == null)
                    return data.toString();

                while(line != null && !line.startsWith("Content-Disposition:"))
                    line = r.readLine();

                //
                // now line says content-disposition....
                //
                String param = "";
                if(line != null && line.startsWith("Content-Disposition: form-data")){
                    //
                    // ok, that's our turn !
                    //

                    int i = line.indexOf("name")+6;
                    int j = line.indexOf("\"", i);

                    param = line.substring(i, j);
                }
                line = r.readLine();  // the separating 2 crlf

                do {
                    line = r.readLine();
                    if(line == null)
                        return data.toString();

                    if(! line.startsWith("--"+boundary)){
                        if(data.length() > 0)
                            data.append("&");
                        data.append(param);
                        data.append("=");
                        data.append(line);
                        //data.append(URLEncoder.encode(line, "UTF-8"));
                    }
                } while(! ( line.startsWith("--"+boundary) || "".equals(line) ));

            } while(! line.equals("--"+boundary+"--"));


        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return data.toString();
    }
    
    /**
     * 
     * This method does a simple path-normalization. Currently it
     * simply replaces &quot;//&quot; by &quot;/&quot;.
     * 
     * @param path The path that is to be normalized.
     * @return The <i>normalized</i> path.
     */
    public static String normalizePath(String path){
    	
    	// TODO: This has to be enhanced to feature a fully rfc-compliant
    	//       normalization
    	
        String ret = path;
        do {
            ret = ret.replaceAll("//", "/");
            ret = ret.replaceAll("/./", "/");
        } while (ret.indexOf("//") >= 0 && ret.indexOf("/./") >= 0);
        
        return ret;
    }
}
