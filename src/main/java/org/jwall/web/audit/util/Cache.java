package org.jwall.web.audit.util;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Cache<V> {

	int size;
	Map<String,V> data = new HashMap<String,V>();
	LinkedList<String> keys = new LinkedList<String>();
	long hits = 0L;
	long misses = 0L;
	
	public Cache( int size ){
		this.size = size;
		this.keys = new LinkedList<String>();
	}
	
	
	public void put( String key, V object ){
		data.put( key, object );
		keys.add( key );
		if( keys.size() > size )
			prune();
	}
	
	public boolean containsKey( String key ){
		boolean b = data.containsKey( key );
		if( b )
			hits++;
		else
			misses++;
		return b;
	}
	
	public V get( String key ){
		V dat = data.get( key );
		return dat;
	}
	
	public void prune(){
		while( keys.size() > size ){
			String key = keys.removeFirst();
			data.remove( key );
		}
	}
	
	public int size(){
		return keys.size();
	}
	
	public int maximumSize(){
		return size;
	}
	
	
	public boolean isFull(){
		return keys.size() >= size;
	}
	
	
	public Long getHits(){
		return hits;
	}
	
	public Long getMisses(){
		return misses;
	}
	
	
	public Double getUse(){
		return ( new Double( keys.size() ) / new Double( size ) );
	}
}