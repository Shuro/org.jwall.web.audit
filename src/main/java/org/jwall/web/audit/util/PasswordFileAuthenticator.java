/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.util.Properties;


/**
 * This extension of the SimplePasswordAuthenticator class simply reads users and
 * groups from files which are given to the constructor at instantiation time.
 * The user file is expected to have one line of the format
 * 
 * <pre>
 *     user=password
 * </pre>
 * 
 * for each user. The password is supposed to be in plaintext. The group file is supposed to have a 
 * line for each group, i.e.
 * 
 * <pre>
 *     group1=user1,user2,user3
 * </pre>
 * 
 * Similar to the standard unix password/group format.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class PasswordFileAuthenticator
    extends SimplePasswordAuthenticator
{
    /**
     * This creates a new authenticator which reads users from the given file <code>usersFile</code>.
     * The group information is read from <code>groupFile</code>.
     * 
     * 
     * @param usersFile
     * @param groupFile
     * @throws IOException
     */
    public PasswordFileAuthenticator( File usersFile, File groupFile ) throws IOException {
        super( new Properties(), new Properties() );
        users.load( new FileInputStream( usersFile ) );
        groups = new Properties();
        
        if( groupFile.exists() && groupFile.canRead() )
            groups.load( new FileInputStream( groupFile ) );
    }

    
    /**
     * This creates a new authenticator in the same manner as the above constructor does. It additionally
     * allows for the specification of a message digest algorithm, which is used to compute a digest of
     * the password before comparing it during authentication.
     * 
     * @param usersFile
     * @param groupFile
     * @param messageDigest
     * @throws IOException
     */
    public PasswordFileAuthenticator( File usersFile, File groupFile, MessageDigest messageDigest ) throws IOException {
        this( usersFile, groupFile );
        md = messageDigest;
    }
}
