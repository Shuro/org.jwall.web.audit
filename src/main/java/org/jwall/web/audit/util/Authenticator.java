/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.util.List;


/**
 * 
 * This interface defines a simple authentication and group-authorization
 * module. It provides methods to authenticate a user using a simple
 * login and password string. It also allows for checking group-membership
 * of a given username.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface Authenticator
{

    /**
     * This method is used to authenticate the given user (<code>login</code>) with
     * the specified password.
     * 
     * @param login The login of the user to authenticate.
     * @param password The password used for authentication.
     * @return <code>true</code>, if the user exists and the password equals the one 
     *         stored within the authenticator.
     */
    public boolean authenticate( String login, String password );
    
    
    /**
     * This method returns if the given user (<code>user</code>) is a member of the
     * specified group (<code>role</code>). 
     *  
     * @param user The user to check.
     * @param role The group to check.
     * @return <code>true</code>, if the users is a member of the given group.
     */
    public boolean isUserInRole( String user, String role );
    

    /**
     * This method returns the list of groups which the user sepcified by <code>login</code>
     * belongs to.
     * 
     * @param login The user's login.
     * @return The list of group names, which the user is a member of.
     */
    public List<String> getRoles( String login );
}
