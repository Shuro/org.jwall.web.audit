/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.LinkedBlockingQueue;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AccessLogAuditReader;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.io.ModSecurityAuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * <p>
 * This class scans audit-events for remote-file references in parameters and downloads
 * all references into a local repository. The aim is to create collections of scripts
 * which are used to check for RFI-vulnerabilities.
 * </p>
 * <p>
 * The class basically implements the worker-thread part to handle the scanning and
 * download given an audit-event or access-log source (usually a file). Though the class
 * provides a <code>main</code>-method for running in a standalone-manner, it is primarily
 * used within an rfi-collector-server, which handles a bunch of rfi-collectors and is
 * controllable via a simple RMI-interface.
 * </p>
 * 
 * <b>TODO:<b>
 * <ul>
 *   <li>Provide logging of downloaded references to some sort of index/DB</li>
 *   <li>More documentation, Getting-Started/Usage documents</li>
 * </ul>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class RFICollector
extends Thread
implements AuditEventListener
{
    public final static String VERSION = "0.0.1";

    private static Logger log = LoggerFactory.getLogger( "org.jwall.web.audit.util.RFICollector" );

    public static final String PROPERTY_TAIL = "org.jwall.rfi.tail";
    public static final String PROPERTY_FOLLOW_FILES = "org.jwall.rfi.follow-files";
    public static final String PROPERTY_FILES_ONLY = "org.jwall.rfi.files-only";
    public static final String PROPERTY_DOWNLOAD = "org.jwall.rfi.download";
    public static final String PROPERTY_SITE_DIRS = "org.jwall.rfi.site.dirs";
    public static final String PROPERTY_DATA_DIR = "org.jwall.rfi.data-dir";
    public static final String PROPERTY_LOG_FILE = "org.jwall.rfi.log-file";
    public static final String PROPERTY_PRESERVE_URL = "org.jwall.rfi.preserve-url"; 

    File dataDir;
    AuditEventReader reader;
    Properties props = new Properties();
    SortedSet<String> references = new TreeSet<String>();
    LinkedBlockingQueue<URL> remoteReferences = new LinkedBlockingQueue<URL>();
    boolean finished = false;
    Integer cnt = 0;
    Long check = 0L;


    /**
     * 
     * @param p
     * @throws Exception
     */
    public RFICollector( Properties p ) throws Exception {

        this.props = new Properties();
        for( Object k : p.keySet() )
            props.put( k, new String( p.getProperty( k.toString() ) ) );

        boolean tail = "true".equals( props.getProperty( PROPERTY_TAIL ) );

        reader = createReader( this.props.getProperty( PROPERTY_LOG_FILE ), tail );
        dataDir = new File( this.props.getProperty( PROPERTY_DATA_DIR ) );
    }


    /**
     * 
     * @param source
     * @param data
     */
    public RFICollector( AuditEventReader source, Properties p  ) throws Exception {
        this.props = new Properties();
        for( Object k : p.keySet() )
            props.put( k, new String( p.getProperty( k.toString() ) ) );
        reader = source;
        dataDir = new File( this.props.getProperty( PROPERTY_DATA_DIR ) );
    }



    public void run(){
        try {
            AuditEvent evt = null;

            while( !finished ){

                try {
                    evt = reader.readNext();
                } catch (Exception ex){}

                if( evt == null ){
                    if( "true".equalsIgnoreCase( System.getProperty(PROPERTY_TAIL) ) ){
                        StringBuffer s = new StringBuffer();
                        for( String ref : references )
                            s.append( ref + "\n" );

                        log.info("Analyzed " + cnt + " events, found " + references.size() + " remote references:\n" + s.toString() );
                        return;
                    }
                    try {
                        Thread.sleep( 1000 );
                    } catch (Exception e2){};
                } else {
                    cnt++;
                    handleEvent( evt );
                }
            }            

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCount(){
        return cnt;
    }



    /**
     * 
     * This method extracts all remote-file references from the request associated
     * with this audit event.
     * 
     * @param evt The audit event which was recorded during the request.
     * @return List of remote file references.
     */
    public List<String> extractRemoteReferences( AuditEvent evt ){
        List<String> rfr = new LinkedList<String>();

        //System.out.println("Analyzing request to " + evt.getRequestURL() );
        for( String param : evt.getAll( ModSecurity.ARGS_GET ) ){
            List<String> vals = evt.getAll( ModSecurity.ARGS_GET + ":" + param );

            for( String val : vals ){
                if( val != null && val.startsWith( "http://" ) ){
                    rfr.add( val );
                    log.debug("Found remote file reference: " + val );
                    System.out.println( "Found remote file reference: " + val );
                }
            }
        }

        if( "POST".equalsIgnoreCase( evt.get( ModSecurity.REQUEST_METHOD ) ) ){

            for( String param : evt.getAll( ModSecurity.ARGS_POST ) ){

                for( String val : evt.getAll( ModSecurity.ARGS_POST + ":" + param ) ) {
                    if( val != null && val.startsWith( "http://" ) ){
                        rfr.add( val );
                        log.debug("Found remote file reference: " + val );
                        System.out.println( "Found remote file reference: " + val );
                    }
                }
            }
        }
        return rfr;
    }

    public void eventArrived( AuditEvent e ){
        handleEvent( e );
    }

    public void eventsArrived( Collection<AuditEvent> events ){
        for( AuditEvent evt: events )
            eventArrived( evt );
    }



    /**
     * This is called for all events.
     * 
     * @param evt
     */
    public void handleEvent( AuditEvent evt ){

        // extract all remote-file references 
        //
        List<String> rfr = extractRemoteReferences( evt );


        // now, download all remote-references to local files and
        // simultaneously write meta-data to disk as well...
        //
        for( String rf : rfr ){

            try {
                URL url = new URL( rf );

                StringBuffer meta = new StringBuffer();
                meta.append( "ScriptEvent-ID: " + evt.getEventId() + "\n" );
                meta.append( "Date: " + new Date() + "\n" );
                meta.append( "URL: " + url + "\n" );

                if( !references.contains( rf ) ){
                    references.add( rf );
                }

                log.info( "Downloading from URL " + url );
                String data = download( url );
                String id = md5( "" + url );
                File out = new File( dataDir + "/" + id + ".dat" );

                log.info(" output-file : " + out.getAbsolutePath() );

                if( "true".equalsIgnoreCase( System.getProperty( PROPERTY_PRESERVE_URL ) ) ){
                    id = url.getPath().replaceAll("/", "_");
                    out = new File( dataDir + "/" + url.getHost() + "/" + url.getPath().replaceAll( "/", "_") );

                    if( out.getParentFile() != null )
                        out.getParentFile().mkdirs();
                }

                if( out.exists() ){

                    meta.append( "Msg: output-file exists!\n" );

                } else {

                    FileWriter fos = new FileWriter( out );
                    fos.write( data );
                    fos.close();

                    meta.append( "File: " + out.getAbsolutePath() + "\n" );

                }

                FileWriter info = new FileWriter( new File( dataDir + "/" + id + ".txt" ) );
                info.write( meta.toString() );
                info.close();

            } catch (Exception e) {
                log.warn( "Exception: " + e.getMessage() );
            }
        }

        if( cnt == 0 )
            check = System.currentTimeMillis();

        Integer mod = 5000;
        if( cnt > 0 && cnt % mod == 0 ){

            double d = mod.doubleValue();
            Long dur = ( System.currentTimeMillis() - check );
            double sec = dur.doubleValue() / 1000;
            //System.out.println( "Batch of " + mod + " events processed in " + (sec) + "s" );
            check = System.currentTimeMillis();



            System.out.println( "Processed " + cnt + " events (" + ( d / (sec)) + " evts/s)" );
        }
    }


    /**
     * 
     * @param url
     * @return
     * @throws Exception
     */
    public String download( URL url ) throws Exception {

        StringBuffer data = new StringBuffer();

        if( "true".equalsIgnoreCase( props.getProperty( PROPERTY_DOWNLOAD ) ) ){

            log.info( "Starting to download \"" + url + "\"" );

            URLConnection con = url.openConnection();
            con.setRequestProperty( "user-agent", "" );
            con.setConnectTimeout( 2000 );
            con.setReadTimeout( 2000 );
            con.connect();
            InputStream in = con.getInputStream();
            int b = -1;
            int bytes = 0;
            do {

                b = in.read();
                if( b != -1 ){
                    data.append( (char) b );
                    bytes++;
                }

            } while( b != -1 );
            log.info( bytes + " bytes downloaded." );
        } else {
            log.info( "download-option " + PROPERTY_DOWNLOAD + " not enabled!" );
        }
        return data.toString();
    }


    public void finish(){
        finished = true;
    }



    public String md5( String data ) throws Exception {
        MessageDigest md = java.security.MessageDigest.getInstance( "MD5" );
        md.update( data.getBytes() );

        BigInteger bi = new BigInteger( md.digest() ).abs();
        return bi.toString( 16 );
    }

    public static AuditEventReader createReader( String f, boolean tail ) throws Exception {

        System.out.println("Creating AuditEventReader from " + f );
        AuditEventReader reader = null;

        File logFile = new File( f );
        int fmt = AuditFormat.guessFormat( logFile );

        if( fmt == AuditFormat.APACHE_ACCESS_LOG )
            reader = new AccessLogAuditReader( logFile, tail );

        if( fmt == AuditFormat.MOD_SECURITY_1_X_SERIAL_LOG )
            reader = new ModSecurityAuditReader( logFile, tail );

        if( fmt == AuditFormat.MOD_SECURITY_2_X_SERIAL_LOG )
            reader = new ModSecurity2AuditReader( logFile, tail );

        if( reader == null )
            throw new Exception( "The log-file format is not supported!" );

        return reader;
    }



    public static void loadProperties( File file ) throws IOException {
        if( file.exists() ){
            System.out.println( "Reading properties from \"" + file.getAbsolutePath() + "\"..." );
            Properties p = new Properties();
            p.load( new FileInputStream( file ) );
            for( Object key : p.keySet() )
                System.setProperty( key.toString(), p.getProperty( key.toString() ) );
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args)
    {

        try {

            if( System.getProperty( PROPERTY_DOWNLOAD) == null )
                System.setProperty( PROPERTY_DOWNLOAD, "false" );

            if( System.getProperty( PROPERTY_TAIL ) == null )
                System.setProperty( PROPERTY_TAIL, "false" );

            if( System.getProperty( PROPERTY_SITE_DIRS) == null )
                System.setProperty( PROPERTY_SITE_DIRS, "true" );

            if( System.getProperty( PROPERTY_FILES_ONLY) == null )
                System.setProperty( PROPERTY_FILES_ONLY, "false" );

            if( System.getProperty( PROPERTY_DATA_DIR ) == null )
                System.setProperty( RFICollector.PROPERTY_DATA_DIR, "." );

            if( args.length == 0 ){
                System.out.println( "This is the RFI-Collector for web-server logs, version " + VERSION );
                System.out.println( "Usage:\n\tjava org.jwall.web.audit.util.RFICollector [options] log-file\n\n" );
                System.exit( 0 );
            }


            File properties = new File( System.getProperty("user.home") + "/" + ".rfi-collector.properties" );
            if( properties.exists() )
                loadProperties( properties );



            File dataDir = new File( "./" );
            // parsing options
            //
            for( int i = 0; i + 1 < args.length; i++ ){
                if( "-c".equals( args[i] ) || "--config".equals( args[i] ) ){
                    File cfg = new File( args[ i + 1 ] );
                    if( cfg.exists() && cfg.canRead() ){
                        loadProperties( cfg );
                    } else {
                        System.out.println( "Unable to read properties from \""+ args[ i + 1 ] + "\"!" );
                    }
                }

                if( "-o".equals( args[i] ) || "--output".equals( args[ i ] ) ){

                    dataDir = new File( args[ i + 1 ] );
                    if( !dataDir.exists() )
                        dataDir.mkdirs();

                    if( !dataDir.isDirectory() )
                        throw new Exception("Output location \"" + dataDir.getAbsolutePath() + "\" is not a directory!" );
                }

            }


            System.out.println("Config:");
            for( Object key : System.getProperties().keySet() ){
                if( key.toString().startsWith( "org.jwall" ) )
                    System.out.println( ""  + key.toString() + "=" + System.getProperty( key.toString() ) );
            }

            if( System.getProperty( PROPERTY_LOG_FILE ) == null )
                System.setProperty( PROPERTY_LOG_FILE, ( new File( args[ args.length - 1 ] ) ).getAbsolutePath() );



            File logFile = new File( System.getProperty( PROPERTY_LOG_FILE ) );
            AuditEventReader reader = AuditFormat.createReader( logFile.getAbsolutePath(), false );

            if( reader == null )
                throw new Exception( "The log-file format is not supported!" );

            RFICollector col = new RFICollector( System.getProperties() );
            col.run();
        } catch (Exception e) {
            System.out.println( "Error: " + e.getMessage() );
            e.printStackTrace();
            System.exit( -1 );
        }
    }
}
