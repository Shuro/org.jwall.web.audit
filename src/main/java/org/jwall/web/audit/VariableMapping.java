/**
 * 
 */
package org.jwall.web.audit;

/**
 * @author chris
 * 
 */
public class VariableMapping {

	public static String[] CASE_INSENSITIVE_PREFIXES = new String[] {
			"REQUEST_HEADERS:", "RESPONSE_HEADERS:" };

	public String map(String variable) {
		return variable;
	}

	public static boolean isCaseInsensitive(String var) {

		for (String prefix : CASE_INSENSITIVE_PREFIXES) {
			if (var.toUpperCase().startsWith(prefix)) {
				return true;
			}
		}

		return false;
	}
}
