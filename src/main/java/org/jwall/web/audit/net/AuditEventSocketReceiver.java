package org.jwall.web.audit.net;

import java.net.ServerSocket;
import java.net.Socket;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class AuditEventSocketReceiver extends Thread {

	static Logger log = LoggerFactory.getLogger( AuditEventSocketReceiver.class );
	ServerSocket socket;
	AuditEventListener listener;

	public AuditEventSocketReceiver( int port ) throws Exception {
		socket = new ServerSocket( port );
	}

	public void run(){
		while( true ){
			try {
				Socket client = socket.accept();
				AuditEventReaderThread handler = new AuditEventReaderThread( client );
				log.info( "Starting new handler for incoming connection from {}:{}", client.getInetAddress().getHostAddress(), client.getPort() );
				handler.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}


	public class AuditEventReaderThread extends Thread {

		AuditEventReader reader;
		Socket socket;

		public AuditEventReaderThread( Socket sock ) throws Exception {
			this.socket = sock;
			reader = new ModSecurity2AuditReader( sock.getInputStream() );
		}

		public void run(){
			try {
				AuditEvent evt = reader.readNext();
				while( evt != null ){
					if( listener != null )
						listener.eventArrived( evt );
					evt = reader.readNext();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			log.info( "Handler for connection {}:{} exiting...", socket.getInetAddress(), socket.getPort() );
		}
	}
}
