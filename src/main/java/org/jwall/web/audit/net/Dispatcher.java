/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.AuditServer;
import org.jwall.web.audit.AuditEvent;

/**
 * This nested class simply implements a client-handling thread which is used
 * to asynchronously push the events to all registered clients.
 */
class Dispatcher
extends Thread 
{
    private static Logger log = LoggerFactory.getLogger("Dispatcher");
    
    int clientPollTime = 0;
    boolean finished = false;
    BlockingQueue<AuditEvent> events;
    Queue<NetworkClientWorkerThread> worker;
    
    public Dispatcher( int clientIdleTime ){
        clientPollTime = clientIdleTime;
        events = new LinkedBlockingQueue<AuditEvent>();
        worker = new LinkedBlockingQueue<NetworkClientWorkerThread>();
    }
    
    public void run(){
        while( ! isFinished() ){
            
            try {
                while(events.isEmpty()){
                    log.debug("NetworkEventServer.Dispatcher: Sleeping until events arrive...");
                    Thread.sleep( clientPollTime );
                }
                
                while(! events.isEmpty()){
                    AuditEvent evt = events.poll();
                    
                    Iterator<NetworkClientWorkerThread> it = worker.iterator();
                    while(it.hasNext()){
                        NetworkClientWorkerThread t = it.next();
                        log.debug("Sending event to client...");
                        
                        if(t.isAlive() && ! t.isClosed()){
                            t.eventArrived( evt );
                        } else {
                            t.closed = true;
                            worker.remove(t);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void enqueueEvents(AuditEvent evt){
        events.add( evt );
    }
    
    public void unregisterClient(NetworkClientWorkerThread t){
        if(worker.contains( t ))
            worker.remove( t );
    }
    
    public void registerClient(NetworkClientWorkerThread t){
        log.debug("NetworkEventServer.Dispatcher: registering new client...");
        AuditServer.log( "Client connected: " + t );
        
        worker.add( t );
    }

    
    public boolean isFinished(){
        return this.finished;
    }
    
    public void finish(){
        finished = true;
    }
}
