/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyStore;
import java.util.Collection;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLException;
import javax.net.ssl.SSLServerSocketFactory;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.util.Authenticator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements a small event-server thread. It will listen on
 * a socket and wait for a <code>NetworkAuditEventSource</code> to connect
 * and push all events to the connected event-sources.
 * 
 * <b>This class is not well-tested!</b>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class NetworkEventServer
extends Thread
implements AuditEventListener
{
    public final static int CLIENT_POLL_TIME = 256;
    private static Logger log = LoggerFactory.getLogger( "NetworkEventServer" );
    
    ServerSocket socket;
    Authenticator users = null;
    Dispatcher dispatcher;

    

    
    
    
    public NetworkEventServer(String s, Authenticator auth)
    throws Exception
    {
        this(s, 10001, auth);
    }

    public NetworkEventServer(String serv, int port, Authenticator auth)
    throws Exception
    {
        users = auth;
        dispatcher = new Dispatcher( CLIENT_POLL_TIME );
        socket = new ServerSocket(port, 10, InetAddress.getByName(serv) );
    }

    public NetworkEventServer(int port, Authenticator auth)
    throws Exception
    {
        users = auth;
        dispatcher = new Dispatcher( CLIENT_POLL_TIME );
        //dispatcher.start();
        socket = new ServerSocket(port, 10 );
    }
    
    private NetworkEventServer(ServerSocket ssocket, Authenticator auth){
        users = auth;
        dispatcher = new Dispatcher( CLIENT_POLL_TIME );
        //dispatcher.start();
        socket = ssocket;
    }
    
    
    public void start(){
        super.start();
        dispatcher.start();
    }
    
    public static NetworkEventServer createSSLEventServer(File keyFile, String pass, int port, Authenticator auth)
    throws Exception
    {
        return createSSLEventServer(new FileInputStream(keyFile), pass, port, auth);
    }

    public static NetworkEventServer createSSLEventServer(InputStream keyIn, int port, Authenticator auth)
    throws Exception
    {
        return createSSLEventServer( keyIn, "geheim", port, auth);
    }

    
    /**
     * This method creates a new NetworkEventServer that is based on an SSL-enabled server socket. Compared to
     * the non-SSL case, the additional parameters are the <code>keyStream</code> and the <code>pass</code>, which
     * refer to the stream that is used to read the private/public key pair from. The password is used to decrypt
     * the private key from the <code>keyStream</code>. 
     * 
     * @param keyStream The stream to read the public/private key pair from.
     * @param pass The password to decrypt the key information.
     * @param port The port on which this server will accept connections.
     * @param auth This map contains a set of login/password pairs used to authenticate users.
     * @return The SSL-enabled server instance.
     * @throws Exception In case the server socket could not be created or something went wrong with the key decryption.
     */
    public static NetworkEventServer createSSLEventServer(InputStream keyStream, String pass, int port, Authenticator auth)
    throws Exception
    {
        char[] passphrase = pass.toCharArray(); 
        KeyStore keystore = KeyStore.getInstance("JKS"); 
        keystore.load( keyStream ,passphrase ); 

        // Now we initialize a KeyManagerFactory with the KeyStore 
        KeyManagerFactory kmf = 
            KeyManagerFactory.getInstance("SunX509"); 
        kmf.init(keystore, passphrase); 

        // Now we create an SSLContext and initialize it with 
        // KeyManagers from the KeyManagerFactory 
        SSLContext context = SSLContext.getInstance("TLS"); 
        KeyManager[] keyManagers = kmf.getKeyManagers(); 

        context.init(keyManagers, null, null); 

        // First we need a SocketFactory that will create 
        // SSL server sockets. 
        SSLServerSocketFactory ssf = context.getServerSocketFactory(); 
        ServerSocket ss = ssf.createServerSocket(port); 

        NetworkEventServer srv = new NetworkEventServer(ss, auth);
        return srv;
    }


    public void run(){
        while(true){
            try {
                Socket sock = socket.accept();
                NetworkClientWorkerThread worker = new NetworkClientWorkerThread(sock, users, this);
                dispatcher.registerClient( worker );
                worker.start();
            } catch (SSLException se) {
                log.error( "Cannot establish SSL-socket connection due to ssl-problems: {}", se.getMessage() );
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * 
     * @see org.jwall.web.audit.AuditEventListener#eventArrived(org.jwall.web.audit.AuditEvent)
     */
    public void eventArrived(AuditEvent e){
        dispatcher.enqueueEvents( e );
    }
    
    public void eventsArrived( Collection<AuditEvent> events ){
        for( AuditEvent evt: events )
            eventArrived( evt );
    }


    /**
     * This method is used to unregister one of the client thread from the dispatcher. The call is simply
     * delegated to the dispatcher itself.
     * 
     * @param t The client thread to unregister.
     */
    public void unregisterClient(NetworkClientWorkerThread t){
        dispatcher.unregisterClient( t );
    }
}
