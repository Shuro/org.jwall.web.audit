/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;


import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.jwall.Collector;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.io.ConcurrentAuditWriter;
import org.jwall.web.audit.util.Base64Codec;
import org.jwall.web.audit.util.MD5;


/**
 * 
 * This class implements a simple socket-handler which provides easy
 * injection of audit-events to the modsecurity-console.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class AuditEventURLConnectionSender 
implements AuditEventListener
{
    public final static String CONSOLE_HOST = "org.modsecurity.console.host";
    public final static String CONSOLE_PORT = "org.modsecurity.console.port";
    public final static String CONSOLE_USER = "org.modsecurity.console.user";
    public final static String CONSOLE_PASS = "org.modsecurity.console.password";
    public final static String CONSOLE_CONNECTION_KEEP_ALIVE = "org.modsecurity.collector.keep-alive";


    /** A unique logger for this class */
    private static Logger log = LoggerFactory.getLogger( "AuditEventConsoleSender" );

    /** the uri of the receiver servlet */
    public final static String CONSOLE_URI = "/rpc/auditLogReceiver";

    /** the destination host */
    private String host = "localhost";

    /** the port to which this senders connects */
    private int port = 8888;

    /** the user name for authentication */
    private String user = "";

    /** the password used for authentication */
    private String pass = "";


    private HttpURLConnection connection = null;

    /**
     * 
     * This method creates a new console sender that sends all arriving
     * events to the given host <code>host</code> using <code>login</code>
     * and <code>password</code> for authentifaction.
     * 
     * @param host The host on which the Console is running.
     * @param port The port, at which the Console is listening.
     * @param login User-name for authentication with the console.
     * @param password Password for authentication.
     * 
     */
    public AuditEventURLConnectionSender( String host, int port, String login, String password)
    {
        this.host = host;
        this.port = port;
        this.user = login;
        this.pass = password;

        try {
            log.debug( "Disabling certificate validation..." );
            SSLContext sc = SSLContext.getInstance( "SSL" );
            sc.init( null, new TrustManager[]{ new ZeroTrustManager() }, new java.security.SecureRandom() );
            HttpsURLConnection.setDefaultSSLSocketFactory( sc.getSocketFactory() );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * This method sends the given audit-event to the configured console.
     * 
     * @param evt The event to be sent to the console.
     * @throws Exception In case an error occurs.
     */
    public void sendAuditEvent( AuditEvent evt ) throws Exception {

        byte[] data = evt.toString().getBytes();
        String hash = "md5:"+MD5.md5( data );

        String sum = ConcurrentAuditWriter.createSummary( evt );
        Base64Codec codec = new Base64Codec();
        String cred = new String( codec.encode( ( user + ":" + pass ).getBytes() ) );

        HttpURLConnection con = this.getConnection();

        try {
            //
            // ok, we try to send the event now
            //
            con.setRequestMethod( "PUT" );
            con.setRequestProperty( "Authorization", cred );
            con.setRequestProperty( "X-Content-Hash", hash );
            con.setRequestProperty( "X-ForensicLog-Summary", sum );
            con.setRequestProperty( "User-Agent", "jwall.org/Collector Version " + Collector.VERSION );

            String event = evt.toString();
            con.setFixedLengthStreamingMode( event.length() );
            
            OutputStreamWriter out = new OutputStreamWriter( con.getOutputStream() );
            out.write( event );
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            connection = null;
            return;
        }
        
        log.info("ScriptEvent sent to server.");
        
        if( !con.getDoInput() ){
            connection = null;
            return;
        }
        
        con.getResponseMessage();
        //String resp = con.getResponseMessage();
        
        /*
        System.out.println( "Reading server response..." );
        //
        // reading the response
        //
        try {
            BufferedReader r = new BufferedReader( new InputStreamReader( con.getInputStream() ) );
            System.out.println("Created the bufferedReader, now starting to read...");
            StringBuffer response = new StringBuffer();

            String l = r.readLine();
            while( l != null ){
                System.out.println("line = " + l );
                response.append( l + "\n" );
                l = r.readLine();
            } 
            System.out.println("Finished reading response...");

            if( response.toString().startsWith("HTTP/1.1 200 OK"))
                log.debug( "ScriptEvent " + evt.getEventId() + " sent." );
            else
                log.debug( response.toString() );

            log.debug( response.toString() );

            r.close();
            
            System.out.println("Response is: \n" + response );
        } catch (Exception e) {
            e.printStackTrace();
        }
        connection = null;
         */
    }


    public HttpURLConnection getConnection() throws Exception {

        System.out.println( "Establishing url connection..." );


        /*
        if( false ){ //port == 8888 || port == 8889 ){

            // using secure https connection
            //
            URL url = new URL( "https://" + host + ":" + port + AuditEventURLConnectionSender.CONSOLE_URI );
            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();

            // we skip the hostname verification
            //
            con.setHostnameVerifier( new HostnameVerifier(){
                public boolean verify( String host, SSLSession session ){
                    return true;
                }
            });

            connection = con;

        } else {
         */

            // stickin' to the plain http connection
            //
            URL url = new URL( "http://" + host + ":" + port + AuditEventURLConnectionSender.CONSOLE_URI );
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            connection = con;
        //}
        
        connection.setInstanceFollowRedirects( false );
        connection.setRequestMethod( "PUT" );
        connection.setDoOutput( true );
        connection.setDoInput( true );
        connection.setUseCaches( false );
        connection.setAllowUserInteraction( false );


        return connection;
    }

    /**
     * 
     * Simply send all arriving events to the configured console. 
     * 
     */
    public void eventArrived( AuditEvent evt ){

        try {

            this.sendAuditEvent( evt );

        } catch ( Exception e ) {

            e.printStackTrace();

        }
    }

    public void eventsArrived( Collection<AuditEvent> events ){
        for( AuditEvent evt: events )
            eventArrived( evt );
    }


    public class ZeroTrustManager 
    implements X509TrustManager, TrustManager 
    {
        private Logger log = LoggerFactory.getLogger( "ZeroTrustManager" );

        public void checkClientTrusted(X509Certificate[] chain, String authType)
        throws CertificateException {

            log.debug( "checkClientTrusted: \n");

            for( X509Certificate cert : chain ){
                log.debug("-------------------------------------------------------");
                log.debug( " SubjectDN = "+cert.getSubjectDN() );
                log.debug( " Issuer = " + cert.getIssuerDN() );
            }       
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType)
        throws CertificateException {

            log.debug( "checkServerTrusted: \n");

            for( X509Certificate cert : chain ){
                log.debug("-------------------------------------------------------");
                log.debug( " SubjectDN = "+cert.getSubjectDN() );
                log.debug( " Issuer = " + cert.getIssuerDN() );
            }       
        }

        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
}
