package org.jwall.web.audit.net;

import java.net.Socket;

public interface SocketAuthenticator {

	public String authenticate( Socket socket );
}
