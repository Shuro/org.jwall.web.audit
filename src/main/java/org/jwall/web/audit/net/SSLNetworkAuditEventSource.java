/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.io.InputStream;
import java.net.Socket;
import java.security.KeyStore;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;


/**
 * 
 * This class is an SSL-enabled implementation of the network audit event source. As with the
 * non-SSL version this class resembles the client-side of the event connection.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class SSLNetworkAuditEventSource
extends NetworkAuditEventSource
{
    
    /**
     * This private constructor initializes the basic data structures (socket,
     * streams).
     * 
     * @param s The socket representing the client connection.
     * @throws Exception In case no readers/stream could be opened using the socket.
     */
    private SSLNetworkAuditEventSource(Socket s)
        throws Exception
    {
        super(s);
    }
    
    
    /**
     * This is the factory method to create a new SSL based network audit event source. The
     * most important additional parameter is the key file from which the private and public
     * key are extracted.
     * 
     * @param keyStoreFile The file containing the private/public key pair.
     * @param pass The password in case the key file is encrypted.
     * @param server The server name/address to which this source will be connected.
     * @param port The port of the server to which the source will be connected.
     * @return The SSL enabled source that can be connected to by clients.
     * @throws Exception In case anything went wrong (setting up the server socket, extracting the keys)
     */
    public static SSLNetworkAuditEventSource createSSLSource(InputStream keyStoreFile, String pass, String server, int port)
        throws Exception
    {
        char[] passphrase = pass.toCharArray(); 
        KeyStore keystore = KeyStore.getInstance("JKS"); 
        keystore.load( keyStoreFile, passphrase); 

        TrustManagerFactory tmf = 
            TrustManagerFactory.getInstance("SunX509"); 
        tmf.init(keystore); 

        SSLContext context = SSLContext.getInstance("TLS"); 
        TrustManager[] trustManagers = tmf.getTrustManagers(); 
        context.init(null, trustManagers, null); 

        SSLSocketFactory sf = context.getSocketFactory();  

        return new SSLNetworkAuditEventSource(sf.createSocket(server, port));
    }
}
