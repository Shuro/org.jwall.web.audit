/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.io.EOFException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.AuditEventType;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventIterator;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * This class implements a parser for audit-logfile for the <code>modsecurity2</code>. It
 * also implements the <code>AuditEventSource</code> interface.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class SyslogAuditEventMultiStream extends SyslogAuditEventStream
	implements AuditEventReader, Runnable
{
    /** A unique logger for this class */
    static Logger log = LoggerFactory.getLogger( SyslogAuditEventMultiStream.class );
    
    /** A flag indicating whether this reader should poll new events after hitting EOF */
    boolean tail;
    Integer lines = 0;
    String prefix = null;
    String sensor = "";
    AuditEventListener listener;
    
    boolean base64 = false;
    boolean autoDetection = true;
    
    Map<String,String> pendingIDs = new HashMap<String,String>();
    Map<String,StringBuffer[]> pending = new HashMap<String,StringBuffer[]>();
    PrintStream out;
    
    StringBuffer current = new StringBuffer();
    
    
    /**
     * This constructor creates an audit-event-Reader that reads from the
     * given input stream.
     * 
     * @param in The stream to read from.
     * @throws IOException In case the stream cannot be opened or another IO error occurs.
     */
    public SyslogAuditEventMultiStream( InputStream in, AuditEventListener listener ) throws IOException {
    	super( in, listener, true );
    	
    	File debug = new File( "/tmp/debug-audit.log" );
    	int i = 0;
    	while( debug.exists() ){
    		debug = new File( "/tmp/debug-audit.log-" + i );
    		i++;
    	}
    	log.info( "Writing debug-copy of received data to {}", debug );
    	out = new PrintStream( new FileOutputStream( debug ) );
    }

    
    /**
     * @see org.jwall.web.audit.io.AuditEventReader#readNext()
     */
    public AuditEvent readNext() throws IOException, ParseException, EOFException {
        //log.info( "readEvent " + counter );
        
        counter++;
        
        String[] line = readLine();  // this holds the current line to be worked on
        long offset = (long) getDataRead();
        
        // this array of strings is filled with the appropriate section-strings
        // stringbuffers are needed because the sections are read one line at a time
        StringBuffer[] sections = getBuffers( line[0] );
    
        // ptr always points to the stringbuffer which is to be read
        int ptr = 0;
    
        try {
            // 
            // we skip lines until we found an audit-start-line, which has the form "--[0-9a-f]*-A--"
            // if the reader hits EOF => line is null and an exception will be thrown, catched below and null is returned 
            //
            while((! line[1].matches("--[\\-\\@0-9A-Za-z]*-A--"))){
            	log.info( "Skipping to start of next event..." );
            	//log.info( "Start of new event found: {}", line[1] );
            	//sections = this.initBuffers( line[0] );
            	line = readLine();
                bytesRead += line[1].length() + 1.0d;
            }            
            
            String id = line[1].replaceFirst("--", "").replaceAll("-A--", "");
            pendingIDs.put( line[0], id );
            
            //
            // ok, now line points to the beginning-separator-line of the audit event.
            //
            while(!(line[1].startsWith("--") && line[1].endsWith("-Z--"))){
                //log.info( "line: {}", line ); // this is the section-beginning-marker
                
            	if( line[1].matches("--[\\-\\@0-9A-Za-z]*-A--") ){
                	log.debug( "Start of new event found: {}", line[1] );
                	current.append( line[1] + "\n" );
                	sections = this.initBuffers( line[0] );
                	//line = readLine();
                    bytesRead += line[1].length() + 1.0d;
            	}
            	
                ptr = getSectionIndex(line[1]);

                //
                // read any lines until we found a line with an associated buffer
                //
                do {
                	line = readLine();  // now line is the first line of the sections body
                	current.append( line[1] + "\n" );
                	sections = getBuffers( line[0] );
                } while( sections == null );
                
                bytesRead += line[1].length() + 1.0d;
    
                //
                // if ptr is -1 an invalid section-name was found !
                //
                if(ptr >= 0){
                    //
                    // no we read a section, until a new one begins
                    //
                    sections[ptr] = new StringBuffer();
                    do {
                    	log.debug( "Appending line to buffer for key '{}'", line[0] );
                        sections[ptr].append(line[1]+"\n");
                        line = readLine();
                    	current.append( line[1] + "\n" );
                        sections = getBuffers( line[0] );
                        bytesRead += line[1].length();
                        //log.info("line: {}", line);
                    } while(!( line[1].trim().matches("^--[\\-\\@0-9A-Za-z]*-Z--$") ) );
                } else {
                    //
                    // invalid section-name
                    //
                    log.debug( "Line contains invalid section-name: " + line );
                }
            }
    
            //
            // We need to convert the stringBuffer-array into a string-array
            // as the AuditEvent-constructor expects a string-array
            //
            String tok[] = current.toString().split( "^--.*-[A-Z]--$" );
            String sensorName = tok[ tok.length - 1 ];
            
            String[] data = new String[ModSecurity.SECTIONS.length()];
            for(int i = 0; i < data.length; i++){
                data[i] = sections[i].toString();
            }
    
            //
            // the A-section is empty - it seems that the EOF has been reached
            //
            if(data[0].equals(""))
                return null;
            
            log.debug( "Removing buffers for key '{}'", line[0] );
            pending.remove( line[0] );
            log.debug( "{} buffers currently pending", pending.size() );
            
            
            AuditEvent event = eventFactory.createAuditEvent( pendingIDs.get( line[0] ), data, inputFile, offset, (long) bytesRead - offset, AuditEventType.ModSecurity2 );
            event.set( AuditEvent.SENSOR_NAME, sensorName );
            log.info( "{} lines read", lines );
            return event;
            
        } catch ( EOFException eof ) {
            log.warn( "End-of-file reached!" );
            eofReached = true;
            throw eof;
        } catch ( Exception e ) {
            e.printStackTrace();
            return null;
        }
    }
 
    
    
    private String[] readLine() throws IOException {
    	
    	String line = reader.readLine();
    	if( line == null )
    		throw new EOFException( "End of file reached!");
    	lines++;
    	if( lines % 1000 == 0 )
    		log.info( "{} lines read so far" );
    	
    	String prio = "";
    	int prioEnd = line.indexOf( ">" );
    	if( prioEnd > 0 )
    		prio = line.substring( 0, prioEnd + 1 ) + " ";

    	int idx = line.indexOf( ": " );
    	String pre = line.substring( 0, idx );
    	String data = line.substring( idx + 2 );
    	
    	String[] tok = pre.split( " " );
    	String key = prio + tok[tok.length - 2] + " " + tok[tok.length - 1];
    	//log.debug( "line read: {} | '{}'", key, data );
    	
    	if( out != null )
    		out.println( data );
    	
    	if( base64 ){
    		log.info( "Decoding base64 data '{}'", data );
    		Base64 b64 = new Base64();
    		data = new String( b64.decode( data ) );
    		log.info( "   => {}", data );
    	}
    	
    	return new String[]{ key, data };
    }

    
    public StringBuffer[] getBuffers( String key ){
    	
    	if( this.pending.containsKey( key ) ){
    		return pending.get( key );
    	}
    	
    	log.debug( "No buffers for key '{}' pending", key );
    	return null; //initBuffers( key );
    }
    
    
    public StringBuffer[] initBuffers( String key ){
    	log.debug( "Initializing new buffers for key '{}'", key );
    	StringBuffer[] buf = new StringBuffer[ ModSecurity.SECTIONS.length() ];
        for(int i = 0; i < buf.length; i++)
            buf[i] = new StringBuffer();
        
        if( pending.containsKey( key ) )
        	log.warn( "Partial event found for key '{}'!", key );
        
    	pending.put( key, buf );
    	return buf;
    }
    

    @Override
	public void run() {
    	while( true ){
    		try {
    			AuditEvent event = readNext();
    			listener.eventArrived( event );
    		} catch (Exception e) {
    			e.printStackTrace();
    			return;
    		}
    	}
	}


	@Override
    public Iterator<AuditEvent> iterator()
    {
        try {
            return new AuditEventIterator( this );
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}