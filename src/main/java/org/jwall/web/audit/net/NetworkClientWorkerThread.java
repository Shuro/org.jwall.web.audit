/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.AuditServer;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.util.Authenticator;

/**
 * This class implements a client handler thread. There will be one of these threads
 * for each client. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class NetworkClientWorkerThread
extends Thread
implements AuditEventListener
{
    public final static int MAX_LOGIN_FAILURES = 3;
    private static Logger log = LoggerFactory.getLogger( "NetworkClientWorkerThread" );
    
    ObjectOutputStream out;
    BufferedReader in;
    boolean closed = false;
    BlockingQueue<AuditEvent> events;
    int sent = 0;
    String id = "";
    Socket sock;
    AuditEventFilter filter = null;
    Authenticator users = null;
    static boolean debug = false;
    NetworkEventServer server;




    public NetworkClientWorkerThread(Socket sock, Authenticator users, NetworkEventServer serv)
    throws Exception
    {
        log.debug("Creating new NetworkClientWorkerThread for "+sock);
        server = serv;
        this.users = users;
        id = "Client["+sock.getInetAddress().getHostAddress()+":"+sock.getPort()+"]";
        events = new LinkedBlockingQueue<AuditEvent>();
        this.sock = sock;
        closed = false;
    }


    /**
     * This method tries to authenticate the client. In case the authentication fails for
     * more than a certain number of tries, the connection is discarded. 
     * 
     * @throws Exception In case any error occurs, or the number of tries exceeded.
     */
    private void authenticate() throws Exception {
        AuditServer.log( "Incoming connection: "+this);
        out = new ObjectOutputStream( sock.getOutputStream() );
        in = new BufferedReader( new InputStreamReader( sock.getInputStream()));
        boolean authOk = false;
        String login = "";
        int tries = 0;

        while(!authOk && tries++ < MAX_LOGIN_FAILURES ){
            out.writeObject("LOGIN");
            login = in.readLine();
            out.writeObject("PASSWORD");
            String pass = in.readLine();

            authOk = users.authenticate( login, pass );
            if(! authOk){
                out.writeObject("LOGIN FAILED!");
                AuditServer.log( this + " Login failed for user \"" + login + "\"");
            }
        }

        if( ! authOk ){
            out.writeObject( "ERROR: Maximum number of login attempts exceeded!" );
            throw new Exception("Authentication error: Number of login attempts exceeded!");
        }

        out.writeObject( "LOGIN OK" );
        AuditServer.log( this + " User " + login + " authenticated successfully.");
    }


    public void run(){
        try {

            // first the client needs to authenticate itself. if anything goes wrong here,
            // we simply clean up and discard the connection
            //
            authenticate();

        } catch (Exception e) {
            e.printStackTrace();
            closed = true;
        }

        // in case the user was successfully authenticated we start another thread to listen
        // for commands send by the client 
        //
        Thread t = new Thread(new Runnable(){
            public void run(){
                while(! closed)
                    checkCommand();
            }
        });
        t.start();


        // no this is the main working loop. while the connection is not closed, we 
        // push all incoming events to the client
        //
        // if something uncorrectable happens, we unregister the client and finish this thread
        //
        try {

            while(! closed ){
                this.pushEvents();
            }

            AuditServer.log( this + " client disconnecting.");
            sock.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        server.unregisterClient(this);
    }


    /**
     * This is the main work part. It checks for events that need to be pushed to the client and
     * sleeps otherwise.
     * 
     * @throws Exception In case an uncorrectable error occurs. 
     */
    private void pushEvents() throws Exception {
        try {
            while(events.isEmpty())
                Thread.sleep(128);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(sock.isInputShutdown() || sock.isOutputShutdown() || ! sock.isConnected() || sock.isClosed()){
            sock.close();
            closed = true;
        }

        while(! events.isEmpty() ){ 
            log.debug("NetworkClientWorker: sending event..");
            AuditEvent e = events.take();
            out.writeObject( e );
            sent++;
        }
    }


    /* (non-Javadoc)
     * @see org.jwall.audit.AuditEventListener#eventArrived(org.modsecurity.AuditEvent)
     */
    public void eventArrived(AuditEvent evt)
    {
        if( sock.isOutputShutdown() ){
            events.clear();
            closed = true;
        }

        if(filter == null || filter.matches( evt )){
            events.add( evt );
        }
    }

    public void eventsArrived( Collection<AuditEvent> events ){
        for( AuditEvent evt: events )
            eventArrived( evt );
    }

    
    public boolean isClosed(){
        return closed;
    }

    public String toString(){
        return id;
    }

    public void checkCommand(){
        try {
            String cmd = null;
            while(cmd == null){
                cmd = in.readLine();
                Thread.sleep(128);
            }

            if("DISCONNECT".equals(cmd)){
                closed = true;
            }

            if(cmd != null && cmd.startsWith("SET_FILTER")){

                try {
                    String[] t = cmd.split(" ");
                    if("null".equals(t[1])){
                        filter = null;
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    filter = null;
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    protected void finalize(){
        this.closed = true;
    }
}
