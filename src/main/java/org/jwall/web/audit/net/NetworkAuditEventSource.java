/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit.net;

import java.io.EOFException;
import java.io.ObjectInputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.jwall.web.audit.ModSecurityAuditEvent;
import org.jwall.web.audit.AuditEventListener;
import org.jwall.web.audit.filter.AuditEventFilter;
import org.jwall.web.audit.io.AuditEventSource;


/**
 * 
 * This class implements a network client-thread that will connect to a
 * given host and then wait for audit-events to be read from that connection. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class NetworkAuditEventSource
extends Thread
implements AuditEventSource
{
    private static Logger log = LoggerFactory.getLogger( "NetworkAuditEventSource" );
	Socket socket;
	ObjectInputStream in;
	PrintStream out;
	Queue<ModSecurityAuditEvent> queue;
	List<AuditEventListener> listeners;
	boolean closed = false;

	
	/**
	 * This creates a simple tcp based connection using the given socket.
	 *  
	 * @param s The socket representing the client end-point.
	 * @throws Exception In case no stream could be opened on the socket.
	 */
	protected NetworkAuditEventSource(Socket s)
	throws Exception
	{
		socket = s;
		in = new ObjectInputStream( socket.getInputStream() ); 
		out = new PrintStream( socket.getOutputStream() );
		queue = new LinkedBlockingQueue<ModSecurityAuditEvent>();
		listeners = new LinkedList<AuditEventListener>();
	}

	
	/**
	 * This creates a new instance of this class which connects to the given server
	 * and port. In case <code>ssl</code> is true, the connection will be established
	 * using SSL encryption.
	 * 
	 * @param host The server name to which to connect to.
	 * @param port The port at which the event server is listening.
	 * @param ssl If to connect using ssl. 
	 * @throws Exception In case the connection could not be established.
	 */
	public NetworkAuditEventSource(String host, int port, boolean ssl)
	throws Exception
	{
		this( new Socket(host, port) );
	}

	
	/**
	 * This method adds the given listener to the list of listeners to be notified
	 * of new events.
	 * 
	 * @param l The listener to be added.
	 */
	public void addEventListener(AuditEventListener l){
		listeners.add( l );
	}

	
	/**
	 * This method removes the given listener. 
	 * 
	 * @param l The listener to be removed.
	 */
	public void removeEventListener(AuditEventListener l){
		listeners.remove( l );
	}

	
	/**
	 * @see org.jwall.web.audit.io.AuditEventSource#hasNext()
	 */
	public boolean hasNext()
	{
		return !queue.isEmpty();
	}

	
	/**
	 * @see org.jwall.web.audit.io.AuditEventSource#nextEvent()
	 */
	public ModSecurityAuditEvent nextEvent()
	{
		return queue.poll();
	}

	
	/**
	 * @see org.jwall.web.audit.io.AuditEventSource#setFilter(org.jwall.web.audit.AuditEventFilter)
	 */
	public void setFilter(AuditEventFilter f)
	{
		out.println(f.toXML());
		out.flush();
	}

	
	/**
	 * This method is used to connect this client side source to an audit event server. The connection
	 * is basically established upon instantiation time. This method basically authenticates the user
	 * and registers this source to receive events.
	 * 
	 * @param login The user name used to authenticate.
	 * @param pass The password for authentication.
	 * @return <code>true</code>, if the user was successfully authenticated.
	 * @throws Exception In case an IO error occurs.
	 */
	public boolean connect(String login, String pass)
	throws Exception
	{
		String s = (String) in.readObject();

		if("LOGIN".equals(s))
			out.println(login);

		s = (String) in.readObject();

		if("PASSWORD".equals(s))
			out.println(pass);

		s = (String) in.readObject();

		return "LOGIN OK".equals(s);
	}


	public void run(){
		try {
			try {
				while(! closed){
					try {
						ModSecurityAuditEvent e = (ModSecurityAuditEvent) in.readObject();
						if(e != null){
						    log.debug("received event-object!");
							queue.add(e);
							Iterator<AuditEventListener> l = listeners.iterator();
							while(l.hasNext()){
								l.next().eventArrived(e);
							}
						}
					} catch (EOFException eof) {
					    //
					    // in case we are at the end of the stream, we simply have a little break.
					    //
					    Thread.sleep(256);
					}
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			in.close();
			socket.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	/**
	 * This method tries to cleanly unregister from the server and close the tcp connection.
	 */
	public void close()
	{
		try {
			this.interrupt();
			out.println("DISCONNECT");
			out.flush();
			listeners.clear();
		} catch (Exception e) {
			e.printStackTrace();
		}
		closed = true;
	}
}
