package org.jwall.web.audit;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class IronBeeEventMessage implements AuditEventMessage {

	public final static String EVENT_ID = AuditEvent.EVENT_ID;
	public final static String RULE_ID = ModSecurity.RULE_ID;
	public final static String TAGS = AuditEvent.TAGS;
	public final static String FIELDS = "FIELDS";
	public final static String CONFIDENCE = "CONFIDENCE";
	public final static String REC_ACTION = "REC_ACTION";

	final Map<String, Serializable> map = new LinkedHashMap<String, Serializable>();

	@Override
	public String getTxId() {
		return get(ModSecurity.TX_ID) + "";
	}

	@Override
	public Date getDate() {
		return null;
	}

	@Override
	public String getFile() {
		return "";
	}

	@Override
	public Integer getLine() {
		return -1;
	}

	@Override
	public Integer getSeverity() {
		try {
			return new Integer(get("SEVERITY") + "");
		} catch (Exception e) {
			return -1;
		}
	}

	@Override
	public String getText() {
		return get("MSG") + "";
	}

	@Override
	public List<String> getRuleTags() {
		ArrayList<String> list = new ArrayList<String>();
		try {
			String[] tags = (String[]) get("TAGS");
			if (tags == null)
				return list;

			for (String tag : tags) {
				list.add(tag);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@Override
	public String getRuleMsg() {
		return getText();
	}

	@Override
	public String getRuleId() {
		return get(ModSecurity.RULE_ID) + "";
	}

	@Override
	public String getRuleData() {
		return get(ModSecurity.RULE_DATA) + "";
	}

	public void set(String key, Serializable value) {
		if (value == null) {
			map.remove(map(key));
			return;
		}
		map.put(map(key), value);
	}

	public Serializable get(String key) {
		return map.get(map(key));
	}

	private String map(String key) {
		return key.toUpperCase().replaceAll("-", "_");
	}

	public String toString() {
		StringBuffer s = new StringBuffer();

		Iterator<String> it = map.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			s.append(key);
			s.append("=");

			Serializable val = map.get(key);
			if (val.getClass().isArray()) {
				s.append("[");
				int len = Array.getLength(val);
				for (int i = 0; i < len; i++) {
					Object obj = Array.get(val, i);
					s.append(obj);
					if (i + 1 < len)
						s.append(",");
				}
				s.append("]");
			} else {
				s.append(val.toString());
			}
			if (it.hasNext())
				s.append(", ");
		}
		return s.toString();
	}
}