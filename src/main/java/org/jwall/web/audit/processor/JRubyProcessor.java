package org.jwall.web.audit.processor;

import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;

public class JRubyProcessor implements EventProcessor<AuditEvent> {

	String script;
	
	
	public JRubyProcessor(){
	}
	
	
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context) throws Exception {
		
		if( script == null )
			return event;
		
		return event;
	}
}