/**
 * 
 */
package org.jwall.web.audit.processor;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;

/**
 * @author chris
 * 
 */
public class HttpHeaderParser extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(HttpHeaderParser.class);
	String section;
	String firstLine;
	String collection;

	public HttpHeaderParser() {
		this("HTTP_REQUEST_HEADERS", "REQUEST_HEADERS", "REQUEST_LINE");
	}

	public HttpHeaderParser(String section, String collectionName,
			String nameOfFirstLine) {
		this.section = section;
		this.firstLine = nameOfFirstLine;
		this.collection = collectionName;
	}

	/**
	 * @return the collection
	 */
	public String getCollection() {
		return collection;
	}

	/**
	 * @param collection
	 *            the collection to set
	 */
	public void setCollection(String collection) {
		this.collection = collection;
	}

	/**
	 * @see stream.data.DataProcessor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data data) {

		if (data.get(section) != null) {
			String headerData = data.get(section).toString();
			Map<String, String> headers = parseHeader(headerData);
			data.putAll(headers);
		}

		return data;
	}

	protected Map<String, String> parseHeader(String header) {
		BufferedReader reader = null;
		Map<String, String> headers = new LinkedHashMap<String, String>();
		try {
			reader = new BufferedReader(new StringReader(header));
			String first = reader.readLine();
			if (first == null) {
				return headers;
			}
			if (firstLine != null)
				headers.put(firstLine, first);

			String line = reader.readLine();
			while (line != null) {
				int idx = line.indexOf(": ");
				if (idx > 0) {
					String key = line.substring(0, idx);
					String val = line.substring(idx + 2);
					log.info("Adding {}={}", collection + ":" + key, val);
					headers.put(collection + ":" + key, val);
				}
				line = reader.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return headers;
	}
}
