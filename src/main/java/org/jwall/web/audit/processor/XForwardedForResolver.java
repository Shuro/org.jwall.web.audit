package org.jwall.web.audit.processor;

import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * This class implements a simple resolver for X-Forwarded-For headers. It will
 * replace the REMOTE_ADDR variable with the X-Forwarded-For address, if such a
 * header is present.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class XForwardedForResolver implements EventProcessor<AuditEvent> {
	/* The logger for this class */
	static Logger log = LoggerFactory.getLogger(XForwardedForResolver.class);

	/**
	 * Header variables are treated case-insensitive, we make this all
	 * upper-case
	 */
	public static String X_FORWARDED_FOR = ModSecurity.REQUEST_HEADERS
			+ ":X-Forwarded-For".toUpperCase();

	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event,
	 *      java.util.Map)
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		log.debug("Processing event '{}'", event.get(ModSecurity.TX_ID));
		String forwardedFor = event.get(X_FORWARDED_FOR);
		log.debug("  value for X-Forwarded-For is: {}", forwardedFor);
		if (forwardedFor != null && !forwardedFor.trim().equals("")) {
			int idx = forwardedFor.indexOf(",");
			if (idx > 0) {
				forwardedFor = forwardedFor.substring(0, idx).trim();
			}
			log.debug("Found X-Forwarded-For header, value: '{}'",
					forwardedFor.trim());
			event.set(ModSecurity.REMOTE_ADDR, forwardedFor.trim());
		} else {
			log.debug("No X-Forwarded-For present in event '{}'",
					event.get(ModSecurity.TX_ID));
		}

		return event;
	}
}
