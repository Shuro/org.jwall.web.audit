package org.jwall.web.audit.processor;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.rules.PersistentCollectionExtractor;

import stream.Data;
import stream.Processor;
import stream.annotations.Parameter;

/**
 * This processor extracts collections from the rule-log (i.e. section 'K' of
 * ModSecurity events). The output of the extraction is pushed back into the
 * event (or Data item).
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
public class PersistentCollectionProcessor extends
		PersistentCollectionExtractor implements Processor,
		EventProcessor<AuditEvent> {
	String key = ModSecurity.SECTION_NAMES[ModSecurity.SECTION_RULE_LOG];

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	@Parameter
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @see stream.data.DataProcessor#process(stream.data.Data)
	 */
	@Override
	public Data process(Data data) {

		if (data == null)
			return data;

		Serializable ruleLog = data.get(key);
		if (ruleLog != null) {
			Map<String, String> coll = processRuleLog(ruleLog.toString(),
					new HashMap<String, String>());
			data.putAll(coll);
		}

		return data;
	}

	/**
	 * 
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {

		Map<String, String> coll = new HashMap<String, String>();
		for (String key : context.keySet()) {
			coll.put(key, context.get(key).toString());
		}

		coll = extractScores(event, coll);
		for (String key : coll.keySet()) {
			event.set(key, coll.get(key));
		}

		return event;
	}
}
