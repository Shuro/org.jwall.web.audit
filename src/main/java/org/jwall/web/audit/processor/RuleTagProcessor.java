package org.jwall.web.audit.processor;

import java.util.List;
import java.util.Map;

import org.jwall.audit.EventProcessor;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.annotations.Description;

/**
 * This process simply parses the EventMessages and extracts all RULE_TAGS to
 * add them to the TAGS attribute.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 */
@Description(group = "AuditEvent.Processing", text = "This processor adds all rule-tags as tags for the event.")
public class RuleTagProcessor implements EventProcessor<AuditEvent> {
	static Logger log = LoggerFactory.getLogger(RuleTagProcessor.class);

	String separator = "|";

	String user = "system";

	/**
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * @param separator
	 *            the separator to set
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	/**
	 * @return the user
	 */
	public String getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @see org.jwall.audit.EventProcessor#processEvent(org.jwall.audit.Event,
	 *      java.util.Map)
	 */
	@Override
	public AuditEvent processEvent(AuditEvent event, Map<String, Object> context)
			throws Exception {
		log.debug("adding RuleTags to event {}", event.get(ModSecurity.TX_ID));

		// the tag-string
		//
		StringBuilder s = new StringBuilder();
		String existingTags = event.get(AuditEvent.TAGS);
		if (existingTags != null) {
			s.append(existingTags);
		}

		int added = 0;
		AuditEventMessage[] msgs = event.getEventMessages();
		log.debug("EventMessages: {}", msgs);
		if (msgs == null) {
			return event;
		}

		for (AuditEventMessage msg : msgs) {
			log.debug("Processing message: {}", msg);

			//
			// iterate over the rule-tags
			//
			List<String> tags = msg.getRuleTags();
			if (tags == null) {
				continue;
			}

			for (String tag : tags) {

				if (s.length() > 0) {
					s.append(separator);
				}

				s.append(user);
				s.append(":");
				s.append(tag);
				added++;
			}
		}

		if (added > 0) {
			String tagString = s.toString();
			log.debug("Adding final tag-string: '{}'", tagString);
			event.set(AuditEvent.TAGS, tagString);
		}

		return event;
	}
}