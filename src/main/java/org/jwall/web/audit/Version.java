/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;


/**
 * 
 * This class implements some version information of this java library. It
 * can be invoked to print out the version to the standard output.
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 * 
 * <h3>Latest Changes</h3>
 * <ul> 
 *   <li>Fixed a bug when accessing an empty collection element</li>
 *   <li>Added maven-plugin for javadocs and sources in deployment.</li>
 *   <li>Added a test for ARGS extraction in POST messages, test-file</li>
 *   <li>Fixed a bug in cookie-extraction</li>
 *   <li>Changed logging to slf4j</li>
 * </ul>  
 *   
 * <h3>Changes in 0.4.0</h3>
 * <ul>
 *   <li>Changed consistent ordering of parameters within parser</li>
 *   <li>Added ConcurrentDirectoryReader as new reader implementation</li>
 *   <li>Fixed a parser bug for the query string.</li>
 *   <li>Added instance counting for the AuditEventMessage implementation</li>
 *   <li>Introduced constant SEVERITY_UNKNOWN</li>
 * </ul>
 *
 */
public class Version
{
	
	public final static org.jwall.Version VER = org.jwall.Version.getVersion( "org.jwall", "org.jwall.web.audit" );
	public final static String REVISION = VER.getRevision();
    public final static String VERSION = VER.getVersion() +  (REVISION.isEmpty() ? "" : " " + REVISION);

    static {
        System.setProperty( "org.jwall.web.audit.version", VER.getVersion() );
    }
    
    /**
     * Return the version number as string.
     * @return
     */
    public static String getVersion(){
        return VERSION;
    }
    
    
    
    
    /**
     * Return the name of the component to which this version
     * information belongs.
     * 
     * @return The name of the software component.
     */
    public final static String getComponentName(){
        return "org.jwall.web.audit";
    }
    
    /**
     * @param args
     */
    public static void main(String[] args)
    {
        
        for( String arg : args ){
            if( arg.equalsIgnoreCase( "-v" ) || arg.equalsIgnoreCase( "--version" ) ){
                System.out.print( getComponentName() + " " );
                break;
            }
        }
        
        System.out.print( getVersion() );
    }
}
