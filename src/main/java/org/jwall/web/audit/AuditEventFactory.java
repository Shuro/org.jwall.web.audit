/*
 *  Copyright (C) 2007-2010 Christian Bockermann <chris@jwall.org>
 *
 *  This file is part of the  web-audit  library.
 *
 *  web-audit library is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The  web-audit  library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
package org.jwall.web.audit;

import java.io.File;

import org.jwall.web.audit.io.ParseException;


/**
 * <p>
 * This interface defines the methods that need to be provided by a factory class
 * that creates audit-event instances. The use of different implementations of this
 * factory interface may improve performance.
 * </p>
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public interface AuditEventFactory
{
    /** This is the system property to define the class which shall be used by readers */
    public final static String AUDIT_EVENT_FACTORY_CLASS = "org.jwall.web.audit.AuditEventFactory";
    

    /**
     * Creates a new audit-event instance from the given section strings. The
     * string correspond to the sections found within an audit-log file.
     * 
     * @param sectionData The data strings of the sections.
     * @return An instance corresponding to the given sections.
     * @throws In case an error occurs while creating/parsing the sections.
     */
    public AuditEvent createAuditEvent( String[] sectionData, AuditEventType type ) throws ParseException;
    
    
    public AuditEvent createAuditEvent( String id, String[] data, File f, long off, long size, AuditEventType type ) throws ParseException;
}
