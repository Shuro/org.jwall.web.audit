package org.jwall.web.audit;

import java.io.Serializable;

/**
 * This enumeration enlists the event formats supported by the storage
 * engine. Currently only support for ModSecurity2 is implemented. 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public enum AuditEventType
	implements Serializable
{

	ModSecurity1 ( "ModSecurity-1.x" ),
	ModSecurity2 ( "ModSecurity-2.x" ),
	IronBee ( "IronBee" );
	
	
	private String name;
	
	AuditEventType( String name ){
		this.name = name;
	}
	
	
	public String toString(){
		return name;
	}
}