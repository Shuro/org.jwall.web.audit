package org.jwall;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jwall.web.audit.util.Base64Codec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenerateLargeEvent {
	
	static Logger log = LoggerFactory.getLogger( GenerateLargeEvent.class );
	
	
	public static void writeRandomData( PrintWriter writer, long size ){
		Base64Codec b64 = new Base64Codec();
		Random rnd = new Random();
		long written = 0;
		
		final byte[] buf = new byte[16 * 1024];
		rnd.nextBytes( buf );
		
		char[] newline = new char[]{ '\r', '\n' };
		
		while( written < size ){
			Long remain = size - written;
			int write = Math.min( buf.length, remain.intValue() );
			byte[] encoded = b64.encode( buf );
			for( int i = 0; i < write && i < buf.length; i++ ){
				writer.write( (int) encoded[i] );
				written++;
				if( i > 0 && written % 80 == 0 ){
					writer.write( newline );
					i += 2;
					written += 2;
				}
			}
			writer.flush();
		}
		log.debug( "Wrote {} random bytes in Base64..." );
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) throws Exception {
		
		long start = System.currentTimeMillis();
		Pattern p = Pattern.compile( "^--[a-zA-z0-9]*-([A-Z])--$" );
		URL url = GenerateLargeEvent.class.getResource( "/audit.log" );

		PrintWriter writer = new PrintWriter( new FileWriter( new File( "/Volumes/RamDisk/big-event.audit-log" ) ) );
		long size = 16 * 10 * 1024 * 1024L;
		char section = 'A';
		
		BufferedReader reader = new BufferedReader( new InputStreamReader( url.openStream() ) );
		String line = reader.readLine();
		while( line != null ){
			
			Matcher m = p.matcher( line );
			if( m.matches() ){
				log.info( "Found separator: {}", line );
				String grp = m.group( 1 );
				log.info( "   caught: '{}'", grp );
				section = grp.charAt( 0 );
			}

			if( section == 'B' && line.startsWith( "Content-Length: " ) ){
				log.info( "injecting new content-length..." );
				writer.println( "Content-Length: " + size );
				line = reader.readLine();
				continue;
			} 
			
			if( section == 'C' && line.startsWith( "TEST_DATA" ) ){
				log.info( "Writing {} bytes of random data...", size );
				writeRandomData( writer, size );
				writer.println();
				line = reader.readLine();
				continue;
			}
			writer.println( line );
			
			line = reader.readLine();
		}

		reader.close();
		writer.close();
		long end = System.currentTimeMillis();
		log.info( "Creating event required {} ms", (end-start) );
	}
}