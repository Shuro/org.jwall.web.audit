package org.jwall.event.processor;

import java.net.URL;
import java.util.HashMap;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.processor.XForwardedForResolver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class XForwardedForResolverTest {

	static Logger log = LoggerFactory.getLogger( XForwardedForResolverTest.class );
	XForwardedForResolver resolver = new XForwardedForResolver();
	AuditEvent event;

	@Test
	public void testProcessEvent() {

		log.info( "Running resolver-test..." );
		try {
			URL url = XForwardedForResolverTest.class.getResource( "/test-audit.log" );
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader( url.openStream() );
			event = reader.readNext();
		} catch (Exception e) {
			Assert.fail( e.getMessage() );
		}
		
		Assert.assertNotNull( event );
		
		//String remoteAddr = event.get( ModSecurity.REMOTE_ADDR );
		String fwd = event.get( ModSecurity.REQUEST_HEADERS + ":X-Forwarded-For".toUpperCase() );
		
		try {
			AuditEvent processed = resolver.processEvent( event, new HashMap<String,Object>() );
			
			String remote = processed.get( ModSecurity.REMOTE_ADDR );
			Assert.assertEquals( remote, fwd );
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail( e.getMessage() );
		}
	}
}