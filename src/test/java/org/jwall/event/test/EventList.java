package org.jwall.event.test;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.ModSecurity2AuditReader;

public class EventList {
	
	
	
	

	public static List<AuditEvent> getAuditEvents( String resource, int num ){
		List<AuditEvent> list = new ArrayList<AuditEvent>();
		
		try {
			int i = 0;
			URL url = EventList.class.getResource( resource );
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader( url.openStream() );
			AuditEvent event = reader.readNext();
			while( event != null && i < num ){
				list.add( event );
				event = reader.readNext();
			}

			
		} catch (Exception e) {
			
		}
		
		return list;
	}
	

	public static List<AuditEvent> getAuditEvents( int num ){
		List<AuditEvent> list = new ArrayList<AuditEvent>();
		
		try {
			int i = 0;
			URL url = EventList.class.getResource( "/events.audit-log" );
			ModSecurity2AuditReader reader = new ModSecurity2AuditReader( url.openStream() );
			AuditEvent event = reader.readNext();
			while( event != null && i < num ){
				list.add( event );
				event = reader.readNext();
			}

			
		} catch (Exception e) {
			
		}
		
		return list;
	}
}
