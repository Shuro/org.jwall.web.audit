package org.jwall.web.audit;


import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.filter.FilterExpression;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.test.ParserBugConsole8Test;
import org.jwall.web.audit.test.RegressionTester;
import org.jwall.web.audit.test.TestParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestSectionTest
{
    static Logger log = LoggerFactory.getLogger( ParserBugConsole8Test.class );
    AuditEventReader reader;

    @Before
    public void setUp() throws Exception
    {
        URL url = ParserBugConsole8Test.class.getResource( "/TEST_SECTION-audit.log" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void test() {
 
        try {
            AuditEvent event = reader.readNext();

            String testSection = event.getSection( ModSecurity.SECTION_TEST );
            
            List<FilterExpression> tests = TestParser.parse( testSection );
            Double total = 2.0d;
            Assert.assertEquals( total.intValue(), tests.size() );
            
            Double passed = 0.0d;
            Double failed = 0.0d;
            
            for( FilterExpression test : tests ){
            	if( test.matches( event ) ){
            		log.info( "Test '{}' passed", test );
            		passed += 1.0d;
            	} else {
            		log.info( "Test '{}' failed", test );
            		failed += 1.0d;
            	}
            }
            
            log.info( "{}% of tests failed on event {}", 100 * (failed/(failed + passed)), event.getEventId() );

            
            List<String> errors = RegressionTester.test( event, testSection );
            log.info( "RegressionTest OK? {}", errors.isEmpty() );
            log.info( "   Errors: {}", errors );

            Assert.assertEquals( new Double(1.0), passed );
            Assert.assertEquals( new Double(1.0d), failed );
            
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail( e.getMessage() );
        }
    }
}
