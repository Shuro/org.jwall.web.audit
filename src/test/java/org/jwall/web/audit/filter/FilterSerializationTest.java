package org.jwall.web.audit.filter;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterSerializationTest {
	
	static Logger log = LoggerFactory.getLogger( FilterSerializationTest.class );
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSerialization() throws Exception {

		FilterExpression f = FilterCompiler.parse( "REMOTE_ADDR @eq 127.0.0.1" );
		String str = f.toString();
		log.info( "Filter before serialization is: {}", str );
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream( baos );
		oos.writeObject( f );
		oos.close();
		baos.close();
		
		ByteArrayInputStream bais = new ByteArrayInputStream( baos.toByteArray() );
		ObjectInputStream ois = new ObjectInputStream( bais );
		FilterExpression ex = (FilterExpression) ois.readObject();
		ois.close();
		
		String str2 = ex.toString();
		log.info( "De-serialized filter is: {}" + str2 );
		Assert.assertEquals( str, str2 );
	}
}
