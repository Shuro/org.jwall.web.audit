package org.jwall.web.audit.filter;


import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FilterTest {
	static Logger log = LoggerFactory.getLogger( FilterTest.class );
	AuditEvent event;
	
	@Before
	public void setUp() throws Exception {
        URL url = AuditEventVariableMatchTest.class.getResource( "/test-audit.log" );
        log.info( "Test-events: " + url );
        AuditEventReader reader = AuditFormat.createReader( url.getFile(), false );
        event = reader.readNext();
	}
	
	@Test
	public void testExp1() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @eq www.jwall.org" ).matches( event );
		Assert.assertTrue( b );
	}
	
	@Test
	public void testExp2() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @eq secure.jwall.org" ).matches( event );
		Assert.assertFalse( b );
	}
	
	@Test
	public void testExp3() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @sx *jwall.org" ).matches( event );
		Assert.assertTrue( b );
	}
	
	@Test
	public void testExp4() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_HEADERS:Host @sx *jwall.org AND REQUEST_URI !@eq '/robots.txt'" ).matches( event );
		Assert.assertFalse( b );
	}
	
	@Test
	public void testExp5() throws FilterException {
		boolean b = ExpressionReader.parse( "REQUEST_URI @eq !'/robots.txt' and REQUEST_HEADERS:Host @sx *jwall.org AND REQUEST_METHOD @rx '!(GET|POST)'" ).matches( event );
		Assert.assertFalse( b );
	}
}