package org.jwall.web.audit;


import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.rules.ValueExtractor;
import org.jwall.web.audit.test.ParserBugConsole8Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UnknownSeverityTest
{
    static Logger log = LoggerFactory.getLogger( UnknownSeverityTest.class );
    AuditEventReader reader;

    @Before
    public void setUp() throws Exception
    {
        URL url = ParserBugConsole8Test.class.getResource( "/unknown-severity-audit.log" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void test() {
 
        try {
            AuditEvent event = reader.readNext();
	    log.info( "Testing severity of event {}", event.getEventId() );
            List<String> vals = ValueExtractor.extractValues( AuditEvent.SEVERITY, event );
	    log.info( "  SEVERITY: {} ", vals );

	    Assert.assertEquals( 1, vals.size() );
	    Assert.assertEquals( "255", vals.get(0));
            
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail( e.getMessage() );
        }
    }
}
