package org.jwall.web.audit;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.zip.GZIPInputStream;

import org.jwall.audit.EventView;
import org.jwall.web.audit.filter.FilterExpression;
import org.jwall.web.audit.filter.Operator;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventDatabaseMock implements EventView<AuditEvent> {
	static Logger log = LoggerFactory.getLogger(AuditEventDatabaseMock.class);

	List<AuditEvent> events = new ArrayList<AuditEvent>();
	Set<String> variables = new TreeSet<String>();

	public AuditEventDatabaseMock() {
		try {
			URL url = AuditEventDatabaseMock.class
					.getResource("/sink-audit.log.gz");
			log.info("Creating DatabaseMock from URL {}", url);
			InputStream in = new GZIPInputStream(url.openStream());
			AuditEventReader reader = new ModSecurity2AuditReader(in);
			AuditEvent e = reader.readNext();
			while (e != null) {
				events.add(e);
				variables.addAll(e.getVariables());
				e = reader.readNext();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		log.info("DatabaseMock has {} events", events.size());
	}

	@Override
	public Set<String> getVariables() {
		return variables;
	}

	@Override
	public Set<String> getIndexedVariables() {
		return variables;
	}

	@Override
	public List<String> getCompletions(String variable) {
		return new ArrayList<String>();
	}

	@Override
	public Long count(FilterExpression filter) throws Exception {

		Long cnt = 0L;

		for (AuditEvent e : events) {
			if (filter.matches(e))
				cnt++;
		}

		return cnt;
	}

	@Override
	public Map<String, Long> count(String variable, FilterExpression filter)
			throws Exception {
		Map<String, Long> result = new LinkedHashMap<String, Long>();

		for (AuditEvent e : events) {

			if (filter.matches(e)) {
				List<String> vals = e.getAll(variable);
				for (String val : vals) {
					Long cnt = result.get(val);
					if (cnt == null) {
						cnt = 1L;
					} else {
						cnt = cnt + 1;
					}
					result.put(val, cnt);
				}
			}
		}

		return result;
	}

	@Override
	public List<AuditEvent> list(FilterExpression filter, int offset, int limit)
			throws Exception {
		log.info("Creating list for filter {}", filter);
		log.info("    offset: {}, limit: {}", offset, limit);
		List<AuditEvent> evts = new ArrayList<AuditEvent>();
		for (AuditEvent e : events) {
			if (filter.matches(e))
				evts.add(e);
			/*
			 * if( i >= offset && i < offset + limit && filter.matches( e ) ){
			 * evts.add( e ); i++; }
			 */
		}

		return evts.subList(offset, offset + limit);

		// return evts;
	}

	@Override
	public AuditEvent get(String id) throws Exception {
		for (AuditEvent e : events) {
			if (id != null && id.equals(e.get(ModSecurity.TX_ID)))
				return e;
		}
		return null;
	}

	@Override
	public boolean delete(String id) throws Exception {
		log.debug("Deleting event by ID: {}", id);
		boolean deleted = false;
		Iterator<AuditEvent> it = events.iterator();
		while (it.hasNext()) {
			AuditEvent evt = it.next();
			if (id != null && id.equals(evt.get(ModSecurity.TX_ID))) {
				it.remove();
				deleted = true;
			}
		}

		log.debug("   event(s) deleted: {}", deleted);
		/*
		 * 
		 * AuditEvent evt = get( id ); if( evt != null ) return events.remove(
		 * evt );
		 */
		return deleted;
	}

	public List<AuditEvent> getRealList() {
		return events;
	}

	@Override
	public void tag(FilterExpression filter, String tag) throws Exception {
	}

	@Override
	public void untag(FilterExpression filter, String tag) throws Exception {
	}

	@Override
	public boolean delete(FilterExpression filter) throws Exception {
		Iterator<AuditEvent> it = events.iterator();
		while (it.hasNext()) {
			AuditEvent e = it.next();
			if (filter.matches(e))
				it.remove();
		}
		return true;
	}

	@Override
	public Set<Operator> getSupportedOperators() {
		Set<Operator> ops = new LinkedHashSet<Operator>();
		for (Operator op : Operator.values()) {
			ops.add(op);
		}
		return ops;
	}
}
