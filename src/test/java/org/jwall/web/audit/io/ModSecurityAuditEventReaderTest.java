package org.jwall.web.audit.io;

import static org.junit.Assert.fail;

import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventMessage;
import org.jwall.web.audit.test.ParserBugConsole8Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ModSecurityAuditEventReaderTest {

	static Logger log = LoggerFactory
			.getLogger(ModSecurityAuditEventReaderTest.class);
	AuditEventReader reader;

	@Before
	public void setUp() throws Exception {
		URL url = ParserBugConsole8Test.class
				.getResource("/CONSOLE-58-audit.log");
		log.info("Audit-ScriptEvent-Log: {}", url);
		reader = new ModSecurity2AuditReader(url.openStream());
	}

	@Test
	public void test() {

		try {
			AuditEvent event = reader.readNext();
			AuditEventMessage[] msgs = event.getEventMessages();
			log.info("Messages: {}", msgs);
			Assert.assertEquals(2, msgs.length);

		} catch (Exception e) {
			fail("Error: " + e.getMessage());
		}
	}
}
