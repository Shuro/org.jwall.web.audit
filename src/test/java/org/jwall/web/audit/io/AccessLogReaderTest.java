package org.jwall.web.audit.io;

import java.io.StringReader;
import java.util.Date;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.log.LogMessage;
import org.jwall.log.io.AccessLogReader;
import org.jwall.web.audit.ModSecurity;

public class AccessLogReaderTest {

	String line = "66.249.65.43 - - [22/Nov/2009:12:41:43 +0100] \"GET /web/policy/editor.jsp HTTP/1.1\" 200 6065 \"-\" \"Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)\"";
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testParse() throws Exception {
		AccessLogReader r = new AccessLogReader( new StringReader( line ) );
		LogMessage msg = r.readNext();
		Assert.assertEquals( "66.249.65.43", msg.get( ModSecurity.REMOTE_ADDR ) );
		Assert.assertEquals( "/web/policy/editor.jsp", msg.get( ModSecurity.REQUEST_URI ) );
		System.out.println( "Date: " + new Date(msg.getTimestamp()));
	}
}