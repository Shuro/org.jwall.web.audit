package org.jwall.web.audit.io;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditEventParserTest
{
    static Logger log = LoggerFactory.getLogger( AuditEventParserTest.class );

    @Before
    public void setUp() throws Exception {
    }

    
    /**
     * This is a test case for the GMT-date bug, making ModSecurity producing wrong dates
     * in timezones with negative GMT offset.
     */
    @Test
    public void testWebAuditBug5()
    {
        String dateString = "[14/Feb/2011:14:16:05 --0500] LchpWlV30w4AAD-aA1IAAAAA 10.0.0.102 60320 10.0.0.4 80";

        log.info( "################# WEB-AUDIT-5 BUG ##################" );
        log.info( "#" );
        Date date = AuditEventParser.parseDate( dateString );
        log.info( "Date: {}", date );
        SimpleDateFormat fmt = new SimpleDateFormat( "yyyyMMdd-HHmmss" );
        //
        // date string in event is:    14/Feb/2011:14:16:05 --0500
        //
        // if you run this test in germany, the hour will show "20" i.e. 8pm, which is due to
        // the GMT-5 of the event and germany being GMT+1  =>   14 + 6 hours =>  20
        //
        log.info( "   date: {}", fmt.format(date) );
        log.info( "   expecting: {}", "20110214-201605" );
        Assert.assertEquals( "20110214-201605", fmt.format( date ) );
        log.info( "#" );
        log.info( "################# WEB-AUDIT-5 BUG ##################" );
    }
}