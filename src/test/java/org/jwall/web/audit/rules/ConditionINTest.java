package org.jwall.web.audit.rules;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.rules.operators.ConditionIN;

public class ConditionINTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetOperator() throws Exception {
		ConditionIN in = new ConditionIN( AuditEvent.SEVERITY, "3,4" );
		Assert.assertEquals( "@in", in.getOperator() );
	}

	@Test
	public void testMatchesStringString() throws Exception {
		ConditionIN in = new ConditionIN( AuditEvent.SEVERITY, "3,4" );
		Assert.assertTrue( in.matches( "3,*jwall.org", "www.jwall.org" ) );
	}


	@Test
	public void testInvertedMatche() throws Exception {
		ConditionIN in = new ConditionIN( AuditEvent.SEVERITY, "3,4" );
		Boolean b =  in.matches( "3,!*jwall.org", "www.jwall.org" );
		Assert.assertFalse( b );
	}
}