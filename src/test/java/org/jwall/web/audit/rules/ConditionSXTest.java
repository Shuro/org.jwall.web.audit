package org.jwall.web.audit.rules;

import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.rules.operators.ConditionSX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConditionSXTest
{
    static Logger log = LoggerFactory.getLogger( ConditionSXTest.class );
    static LinkedList<String> values = new LinkedList<String>();

    ConditionSX condition;
    
    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
        values.add( "www.jwall.org" );
        values.add( "secure.jwall.org" );
        values.add( "www.modsecurity.org" );
        values.add( "www.modsecurity.com" );
    }

    @Before
    public void setUp() throws Exception
    {
        condition = new ConditionSX( "REQUEST_HEADERS:Host", "*.jwall.org" );
    }

    @Test
    public void testMatchesListOfString()
    {
        log.info( "Testing list-matches... " );
        boolean b = condition.matches( values );
        Assert.assertTrue( "", b );
    }

    
    @Test
    public void testGetOperator()
    {
        log.info( "Testing operator-name... " );
        Assert.assertEquals( Condition.SX, condition.getOperator() );
    }

    
    @Test
    public void testMatchesStringString()
    {
        log.info( "Testing some matches..." );
        Assert.assertTrue( condition.matches( "*.jwall.org", "www.jwall.org" ) );
        Assert.assertFalse( condition.matches( "*.jwall.com", "www.jwall.org" ) );
        Assert.assertTrue( condition.matches( "*.*.org", "secure.jwall.org" ) );
        Assert.assertTrue( condition.matches( "*.*.org", "*.modsecurity.org" ) );
        Assert.assertTrue( condition.matches( "www.jwall.org", "www.jwall.org" ) );
    }
    
    @Test
    public void testNegateMatch() throws Exception {
        log.info( "Testing negation... " );
        ConditionSX neg = new ConditionSX( "", "!*.org" );
        ConditionSX neg2 = new ConditionSX( "", "!*.org" );
        Assert.assertFalse( neg.matches( "secure.jwall.org" ) );
        Assert.assertTrue( neg2.matches( "secure.jwall.com" ) );
    }

    
    @Test
    public void testNegateMatchNoWildcard() throws Exception {
        log.info( "Testing wildcard-negation..." );
        ConditionSX sx = new ConditionSX( "", "!secure.jwall.org" );
        Assert.assertFalse( sx.matches( "secure.jwall.org" ) );
        Assert.assertTrue( sx.matches( "www.jwall.org" ) );
    }
}