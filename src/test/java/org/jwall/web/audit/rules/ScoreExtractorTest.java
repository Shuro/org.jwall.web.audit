package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScoreExtractorTest {

	static Logger log = LoggerFactory.getLogger( ScoreExtractorTest.class );

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testExtract() {

		List<AuditEvent> events = EventList.getAuditEvents( "/default-request-host-audit.log", 10 );

		try {

			PersistentCollectionExtractor ex = new PersistentCollectionExtractor();
			String variable = "TX:ANOMALY_SCORE";

			for( AuditEvent evt : events ){
				log.info( "Extracting {} from event {}", variable, evt.getEventId() );

				//log.info( "Event is:\n{}", evt );

				Map<String,String> env = PersistentCollectionExtractor.extractScores( evt );
				for( String key : env.keySet() ){
					log.debug( "   {} = {}", key, env.get(key) );
				}
				
				String value = ex.extract( "TX:ANOMALY_SCORE", evt );
				log.info( "Extracted value is {}", value );
				
				Assert.assertEquals( "5", ex.extract( "TX:ANOMALY_SCORE", evt ) );
				Assert.assertEquals( "5", ex.extract( "tx.anomaly_score", evt ) );
				Assert.assertEquals( "5", ex.extract( "tx.inbound_anomaly_score", evt ) );
				//waitForReturn();
			}			
		} catch (Exception e) {
			fail("Not yet implemented");
		}
	}
	
	public static void waitForReturn(){
		try {
			System.out.print( "Press return to continue..." );
			System.in.read();
		} catch (Exception e) {
		}
	}
}
