package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.io.File;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.rules.operators.ConditionEQ;
import org.jwall.web.audit.rules.operators.ConditionGE;
import org.jwall.web.audit.rules.operators.ConditionGT;
import org.jwall.web.audit.rules.operators.ConditionLE;
import org.jwall.web.audit.rules.operators.ConditionLT;
import org.jwall.web.audit.rules.operators.ConditionPM;
import org.jwall.web.audit.rules.operators.ConditionRX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventRuleTest
{
    static AuditEvent event;
    static Logger log = LoggerFactory.getLogger( EventRuleTest.class );

    @BeforeClass
    public static void setUpBeforeClass() throws Exception
    {
    	File resourceDir = new File( "src/test/resources" );
    	File auditLog = new File( resourceDir.getAbsolutePath() + File.separator + "test-audit.log" );
    	log.info( "Reading audit-event from file '" + auditLog.getAbsolutePath() + "'" );
    	AuditEventReader reader = AuditFormat.createReader( auditLog.getAbsolutePath(), false );
    	event = reader.readNext();
    }

    @Before
    public void setUp() throws Exception
    {
    }

    @Test
    public void testMatches()
    {
        try {
            AuditEventRule rule = new AuditEventRule();
            rule.add( new ConditionRX( "SEVERITY", "^4$" ) );

            boolean matches = rule.matches( event, null );
            log.info( " rule matches??: " + matches );

        } catch (Exception e) {
            e.printStackTrace();
            fail("Not yet implemented");
        }
    }


    @Test
    public void testCountOperator()
    {
        try {
            Condition c = new ConditionEQ( "&REQUEST_HEADERS:Host", "1" );
            if( ! c.matches( event ) )
                fail( "Count-operator failed!" );

        } catch (Exception e) {
            e.printStackTrace();
            fail( "Error: '" + e.getMessage() + "'" );
        }
    }

    @Test
    public void testConditionEQ()
    {
        try {
            String sev = event.get( "REQUEST_HEADERS:Host" );
            log.info( "  event.get('REQUEST_HEADERS:Host') = " + sev );

            log.info( "Testing  REQUEST_HEADERS:Host @eq www.jwall.org" );
            Condition c = new ConditionEQ( "REQUEST_HEADERS:Host", sev );
            if( !c.matches( event ) )
                fail( "Failed to match ConditionEQ on REQUEST_HEADERS:Host with value = '" + sev + "' !" );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }

    @Test
    public void testConditionLT()
    {
        try {
            log.info( "Testing  SEVERITY @lt 4" );
            log.info( "   event SEVERITY: " + ValueExtractor.extractValues( AuditEvent.SEVERITY, event ) );
            Condition c = new ConditionLT( "SEVERITY", "4" );
            if( ! c.matches( event ) )
                fail( "Failed to match ConditionLT on SEVERITY with value '4' !" );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }
    
    
    @Test
    public void testConditionLE()
    {
        try {
            log.info( "Testing  SEVERITY @le 4" );
            log.info( "   event SEVERITY: " + ValueExtractor.extractValues( AuditEvent.SEVERITY, event ) );
            Condition c = new ConditionLE( "SEVERITY", "2" );
            if( ! c.matches( event ) )
                fail( "Failed to match ConditionLE on SEVERITY with value '2' !" );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }
    
    
    @Test
    public void testConditionGT()
    {
        try {
            log.info( "Testing  SEVERITY @gt 4" );
            log.info( "   event SEVERITY: " + ValueExtractor.extractValues( AuditEvent.SEVERITY, event ) ); //event.getAll( AuditEvent.SEVERITY ) );
            Condition c = new ConditionGT( "SEVERITY", "4" );
            if( c.matches( event ) )
                fail( "Failed to match ConditionGT on SEVERITY with value '4' !" );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }


    @Test
    public void testConditionGE()
    {
        try {
            log.info( "Testing  SEVERITY @ge 2" );
            log.info( "   event SEVERITY: " + ValueExtractor.extractValues( AuditEvent.SEVERITY, event ) );
            Condition c = new ConditionGE( "SEVERITY", "2" );
            if( ! c.matches( event ) )
                fail( "Failed to match ConditionGE on SEVERITY with value '2' !" );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }


    @Test
    public void testConditionRX()
    {
        try {
            log.info( "Testing  SEVERITY @rx ^\\d$" );
            String val = event.get( "SEVERITY" );
            log.info( "  event.get('SEVERITY') = " + val );
            Condition c = new ConditionRX( "SEVERITY", "^\\d$" );
            if( ! c.matches( event ) )
                fail( "Failed to match ConditionRX on SEVERITY with value '^\\d$' !" );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }


    @Test
    public void testConditionPM()
    {
        try {
            log.info( "Testing  REQUEST_HEADERS:User-Agent \"@pm test alexa\"" );
            String val = event.get( "REQUEST_HEADERS:User-Agent" );
            log.info( "  event.get('REQUEST_HEADERS:User-Agent') = " + val );
            Condition c = new ConditionPM( "REQUEST_HEADERS:User-Agent", "test alexa" );
            long start = System.nanoTime();
            if( ! c.matches( event ) )
                fail( "Failed to match ConditionPM on REQUEST_HEADERS:User-Agent with value 'test alexa' !" );
            log.info( "PM-Match took " + (System.nanoTime() - start ) + " ns." );
        } catch (Exception e) {
            fail( "Error: " + e.getMessage() );
        }
    }
}