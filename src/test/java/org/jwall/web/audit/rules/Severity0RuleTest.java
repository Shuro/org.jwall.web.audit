package org.jwall.web.audit.rules;

import java.io.File;
import java.net.URL;

import junit.framework.Assert;

import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

public class Severity0RuleTest {

	static AuditEvent event;
	static Logger log = LoggerFactory.getLogger(Severity0RuleTest.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		File resourceDir = new File("src/test/resources");
		File auditLog = new File(resourceDir.getAbsolutePath() + File.separator
				+ "audit-event-severity-0.log");
		log.info("Reading audit-event from file '" + auditLog.getAbsolutePath()
				+ "'");
		AuditEventReader reader = AuditFormat.createReader(
				auditLog.getAbsolutePath(), false);
		event = reader.readNext();
	}

	@Test
	public void test() {
		URL url = EventRuleConditionTests.class
				.getResource("/audit-event-severity-0-rule.xml");

		XStream xs = new XStream();
		for (Class<?> clazz : AuditEventRule.CLASSES) {
			xs.processAnnotations(clazz);
		}

		AuditEventRule rule = (AuditEventRule) xs.fromXML(url);

		boolean match = rule.matches(event, null);
		Assert.assertTrue(match);
	}
}
