package org.jwall.web.audit.rules;


import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleCountRuleIDsTest
{
    static Logger log = LoggerFactory.getLogger( RuleCountRuleIDsTest.class );
    AuditEventReader reader;

    @Before
    public void setUp() throws Exception
    {
        URL url = RuleCountRuleIDsTest.class.getResource( "/rule-sev-extraction-bug.dat" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void test() {
 
        try {
            AuditEvent event = reader.readNext();

            
            List<String> values = ValueExtractor.extractValues( "&RULE_ID", event );
            log.info( "Extracted: {}", values );
            
            Assert.assertTrue( values.size() == 1 );
            
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail( e.getMessage() );
        }
    }
}