package org.jwall.web.audit.rules;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jwall.web.audit.AuditEvent;

public class MockAction implements EventAction {

	@Override
	public void execute(Map<String, Object> ctx, AuditEvent evt) {
		System.out.println( "Firing action on event: " + evt );
	}

	@Override
	public String getLabel() {
		return "FAKE_ACTION";
	}

	@Override
	public String getName() {
		return "FAKE_ACTION";
	}

	@Override
	public Map<String, String> getParameters() {
		return new LinkedHashMap<String,String>();
	}

	@Override
	public List<String> getValues() {
		return new ArrayList<String>();
	}
}