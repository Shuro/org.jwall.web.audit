package org.jwall.web.audit.rules;

import static org.junit.Assert.fail;

import java.io.File;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.AuditFormat;
import org.jwall.web.audit.rules.operators.ConditionEQ;
import org.jwall.web.audit.rules.operators.ConditionRX;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thoughtworks.xstream.XStream;

public class EventRuleConditionTests {
	AuditEvent event;
	static Logger log = LoggerFactory.getLogger(EventRuleConditionTests.class);

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		File resourceDir = new File("src/test/resources");
		File auditLog = new File(resourceDir.getAbsolutePath() + File.separator
				+ "test-audit.log");
		log.info("Reading audit-event from file '" + auditLog.getAbsolutePath()
				+ "'");
		AuditEventReader reader = AuditFormat.createReader(
				auditLog.getAbsolutePath(), false);
		event = reader.readNext();
	}

	@Test
	public void testMatches() {
		try {
			AuditEventRule rule = new AuditEventRule();
			rule.add(new ConditionRX("SEVERITY", "^4$"));

			boolean matches = rule.matches(event, null);
			log.info(" rule matches??: " + matches);

		} catch (Exception e) {
			e.printStackTrace();
			fail("Not yet implemented");
		}
	}

	@Test
	public void testCountOperator() {
		try {
			AuditEventRule rule = new AuditEventRule();

			Condition condition = new ConditionEQ("&REQUEST_HEADERS:Host", "1");
			rule.add(condition);

			boolean matches = rule.matches(event, null);

			if (!matches)
				fail("Count-operator failed!");

		} catch (Exception e) {
			e.printStackTrace();
			fail("Error: '" + e.getMessage() + "'");
		}

	}

	@Test
	public void testSeverityEq() {

		String severity = event.get(ModSecurity.RULE_SEV);
		severity = ValueExtractor.extractValues(ModSecurity.RULE_SEV, event)
				.get(0);
		log.info("Severity: {}", severity);
	}

	@Test
	public void testRuleSeverityEq2() throws Exception {

		URL url = EventRuleConditionTests.class.getResource("/event-rule.xml");

		XStream xs = new XStream();
		for (Class<?> clazz : AuditEventRule.CLASSES) {
			xs.processAnnotations(clazz);
		}

		AuditEventRule rule = (AuditEventRule) xs.fromXML(url);

		boolean match = rule.matches(event, null);
		Assert.assertTrue(match);
	}

	@Test
	public void testRuleSeverityEq3() throws Exception {

		URL url = EventRuleConditionTests.class
				.getResource("/event-rule-severity-eq-3.xml");

		XStream xs = new XStream();
		for (Class<?> clazz : AuditEventRule.CLASSES) {
			xs.processAnnotations(clazz);
		}

		AuditEventRule rule = (AuditEventRule) xs.fromXML(url);
		boolean match = rule.matches(event, null);
		Assert.assertFalse(match);
	}

}