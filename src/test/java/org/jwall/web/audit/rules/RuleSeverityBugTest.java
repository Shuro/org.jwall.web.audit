package org.jwall.web.audit.rules;


import java.net.URL;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.rules.operators.ConditionLT;
import org.jwall.web.audit.test.ParserBugConsole8Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleSeverityBugTest
{
    static Logger log = LoggerFactory.getLogger( ParserBugConsole8Test.class );
    AuditEventReader reader;

    @Before
    public void setUp() throws Exception
    {
        URL url = ParserBugConsole8Test.class.getResource( "/rule-sev-extraction-bug.dat" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void test() {
 
        try {
            AuditEvent event = reader.readNext();

            ConditionLT lt = new ConditionLT( ModSecurity.RULE_SEV, "3" );
            
            List<String> values = ValueExtractor.extractValues( ModSecurity.RULE_SEV, event );
            boolean matches = lt.matches( values );
            Assert.assertFalse( matches );
            
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail( e.getMessage() );
        }
    }
}