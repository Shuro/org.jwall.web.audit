package org.jwall.web.audit.rules;

import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.rules.operators.ConditionEQ;

public class ConditionEQTest {

	List<String> positive = new ArrayList<String>();
	List<String> negative = new ArrayList<String>();
	
	@Before
	public void setUp() throws Exception {
		positive.add( "A" );
		positive.add( "B" );
	}

	
	@Test
	public void testMatchesListOfString() throws Exception {
		ConditionEQ eq = new ConditionEQ( "", "B" );
		boolean rc = eq.matches( positive );
		Assert.assertTrue( rc );
	}
	
	@Test
	public void testNegated() throws Exception {
		ConditionEQ eq = new ConditionEQ( "", "!B" );
		boolean rc = eq.matches( positive );
		Assert.assertFalse( rc );
	}
}