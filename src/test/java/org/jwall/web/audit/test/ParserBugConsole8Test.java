package org.jwall.web.audit.test;

import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This test is simply to confirm valid parsing of events which have been received by Apache,
 * but have not been fully processed, since they are invalid requests - e.g. they have been
 * rejected.
 * </p>
 * <p>
 * See <a href="http://bugs.jwall.org/browse/CONSOLE-8">http://bugs.jwall.org/browse/CONSOLE-8</a> for details.
 * </p>
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ParserBugConsole8Test
{
    static Logger log = LoggerFactory.getLogger( ParserBugConsole8Test.class );
    AuditEventReader reader;

    @Before
    public void setUp() throws Exception
    {
        URL url = ParserBugConsole8Test.class.getResource( "/CONSOLE-8-bug-audit.log" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void testReadNext()
    {
        try {
            AuditEvent evt = reader.readNext();
            log.info( "ScriptEvent: {}" + evt.getEventId() );
            Assert.assertNotNull( evt );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}