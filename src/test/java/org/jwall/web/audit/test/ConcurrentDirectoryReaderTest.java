package org.jwall.web.audit.test;

import java.io.File;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.io.ConcurrentDirectoryReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConcurrentDirectoryReaderTest
{
    static Logger log = LoggerFactory.getLogger( ConcurrentDirectoryReaderTest.class );
    
    ConcurrentDirectoryReader reader;

    Long totalFiles = 0L;
    Long eventsRead = 0L;
    
    @Before
    public void setUp() throws Exception
    {
        URL url = ConcurrentDirectoryReader.class.getResource( "/data-dir-3" );
        log.info( "Creating test-reader from directory '{}'", url );
        reader = new ConcurrentDirectoryReader( new File( url.toURI() ) );
        totalFiles = reader.bytesAvailable();
        log.info( "Found {} files.", totalFiles );
        eventsRead = 0L;
    }

    @Test
    public void testReadNext()
    {
        try {
            
            AuditEvent evt = reader.readNext();
            while( evt != null ){
                eventsRead++;

                log.info( "Completed {}%", 100 * ( eventsRead.doubleValue() / totalFiles.doubleValue() ) );
                try {
                    evt = reader.readNext();
                } catch (Exception e) {
                }
            }
            
            Assert.assertEquals( 3, eventsRead.intValue() );
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}