package org.jwall.web.audit.test;

import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * <p>
 * This test is simply to confirm valid parsing of events which have been received by Apache,
 * but have not been fully processed, since they are invalid requests - e.g. they have been
 * rejected.
 * </p>
 * <p>
 * See <a href="http://bugs.jwall.org/browse/CONSOLE-8">http://bugs.jwall.org/browse/CONSOLE-8</a> for details.
 * </p>
 * 
 * 
 * @author Christian Bockermann &lt;chris@jwall.org&gt;
 *
 */
public class ParserArgsTest
{
    static Logger log = LoggerFactory.getLogger( ParserArgsTest.class );
    AuditEventReader reader;

    List<String> expectedArgs = new LinkedList<String>();
    
    @Before
    public void setUp() throws Exception
    {
        URL url = ParserArgsTest.class.getResource( "/parse-args-audit.log" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
        reader = new ModSecurity2AuditReader( url.openStream() );
    }

    @Test
    public void testReadNext()
    {
        try {

            AuditEvent evt = reader.readNext();
            log.info( "ScriptEvent: {}", evt.getEventId() );
            
            Assert.assertNotNull( evt );

            log.info( "ARGS_NAMES = {}", evt.getAll( ModSecurity.ARGS_NAMES ) );

            expectedArgs.add( "version" );
            expectedArgs.add( "__FORM_TOKEN" );
            expectedArgs.add( "scroll_bar_pos" );
            expectedArgs.add( "action" );
            expectedArgs.add( "preview" );
            expectedArgs.add( "editrows" );
            expectedArgs.add( "text" );
            expectedArgs.add( "comment" );
            
            List<String> args = evt.getAll( ModSecurity.ARGS_NAMES );
            Assert.assertEquals( expectedArgs.size(), args.size() );

            for( int i = 0; i < args.size(); i++ )
                Assert.assertEquals( expectedArgs.get(i), args.get(i) );
            
            for( String arg : evt.getAll( ModSecurity.ARGS_NAMES ) ){
                log.info( "  ARGS:{} = {}", arg, evt.getAll( ModSecurity.ARGS + ":" + arg ) );
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}