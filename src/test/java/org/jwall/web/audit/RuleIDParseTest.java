package org.jwall.web.audit;


import java.io.File;
import java.net.URL;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.web.audit.io.AuditEventReader;
import org.jwall.web.audit.io.ModSecurity2AuditReader;
import org.jwall.web.audit.rules.AuditEventRule;
import org.jwall.web.audit.rules.MockAction;
import org.jwall.web.audit.rules.operators.ConditionEQ;
import org.jwall.web.audit.test.ParserBugConsole8Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RuleIDParseTest {
	
	static Logger log = LoggerFactory.getLogger( RuleIDParseTest.class );
	AuditEvent event;
	
	@Before
	public void setUp() throws Exception {
		URL url = ParserBugConsole8Test.class.getResource( "/CONSOLE-58-audit.log" );
        log.info( "Audit-ScriptEvent-Log: {}", url );
		AuditEventReader reader = new ModSecurity2AuditReader( url.openStream() );
		event = reader.readNext();
		Assert.assertNotNull( event );
	}

	
	@Test
	public void testRuleID() throws Exception {
		AuditEventRule rule = new AuditEventRule();
		rule.add( new ConditionEQ( "RULE_ID", "960015" ) );
		rule.add( new MockAction() );
		Assert.assertTrue( rule.matches( event, null ) );
	}
	
	@Test
	public void testRuleID2() throws Exception {
		
		File test = new File( "/Users/chris/audit/larry-bugs/rule-id-audit.log" );
		if( test.canRead() ){
			AuditEventReader reader = new ModSecurity2AuditReader( test );
			event = reader.readNext();
			log.info( "ScriptEvent is: {}", event.getEventId() );
			AuditEventRule rule = new AuditEventRule();
			rule.add( new ConditionEQ( "RULE_ID", "113" ) );
			rule.add( new MockAction() );
			Assert.assertTrue( rule.matches( event, null ) );
		}		
	}
}
