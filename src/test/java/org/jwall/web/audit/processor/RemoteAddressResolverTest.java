package org.jwall.web.audit.processor;

import static org.junit.Assert.fail;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import junit.framework.Assert;

import org.junit.Test;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RemoteAddressResolverTest {

	static Logger log = LoggerFactory
			.getLogger(RemoteAddressResolverTest.class);

	@Test
	public void test() {

		List<AuditEvent> events = EventList.getAuditEvents(
				"/audit-event-orig-ip.log", 10);

		if (events.isEmpty())
			fail("Failed to load events for testing!");

		Map<String, Object> ctx = new LinkedHashMap<String, Object>();

		RemoteAddressResolver resolver = new RemoteAddressResolver();
		try {
			AuditEvent evt = events.get(0);

			String before = evt.get(ModSecurity.REMOTE_ADDR);
			log.info("Remote address BEFORE processing is: {}", before);
			Assert.assertEquals("172.16.0.1", before);

			log.info("Applying event-processor...");
			evt = resolver.processEvent(events.get(0), ctx);

			String remoteAddress = evt.get(ModSecurity.REMOTE_ADDR);
			log.info("Remote address after processing is: {}", remoteAddress);

			Assert.assertEquals("1.2.3.4", remoteAddress);

		} catch (Exception e) {
			fail("Test failed: " + e.getMessage());
		}
	}
}
