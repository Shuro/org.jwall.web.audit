package org.jwall.web.audit.processor;

import java.util.List;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.audit.EventProcessorFinder;
import org.jwall.event.test.EventList;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultRequestHostProcessorTest {

	static Logger log = LoggerFactory
			.getLogger(DefaultRequestHostProcessorTest.class);
	AuditEventProcessorPipeline pipeline;
	List<AuditEvent> events;

	@Before
	public void setUp() throws Exception {
		events = EventList
				.getAuditEvents("/default-request-host-audit.log", 10);
		pipeline = new AuditEventProcessorPipeline();
		EventProcessorFinder pf = new EventProcessorFinder();
		pf.deployCustomEventProcessors(DefaultRequestHostProcessorTest.class
				.getResourceAsStream("/default-request-host.xml"), pipeline);
	}

	@Test
	public void test() throws Exception {

		for (AuditEvent evt : events) {

			log.info("Testing default-request-host with event {}",
					evt.get(ModSecurity.TX_ID));
			String host = evt.get(ModSecurity.REQUEST_HEADERS + ":Host");
			log.info("    host = {}", host);

			pipeline.process(evt);

			String after = evt.get(ModSecurity.REQUEST_HEADERS + ":Host");
			log.info("    host of processed event: {}", after);

			if (host == null || "".equals(host.trim())) {
				Assert.assertEquals("", "_unknown_", after);
			} else {
				Assert.assertEquals("", host, after);
			}
		}
	}
}