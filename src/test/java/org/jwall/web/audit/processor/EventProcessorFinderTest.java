package org.jwall.web.audit.processor;

import static org.junit.Assert.fail;

import java.net.URL;

import org.junit.Test;
import org.jwall.audit.EventProcessor;
import org.jwall.audit.processor.AuditEventProcessorFinder;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventProcessorPipeline;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EventProcessorFinderTest {

	static Logger log = LoggerFactory.getLogger(EventProcessorFinderTest.class);

	@Test
	public void test() {

		AuditEventProcessorFinder finder = new AuditEventProcessorFinder();
		AuditEventProcessorPipeline pipeline = new AuditEventProcessorPipeline();

		URL url = EventProcessorFinderTest.class
				.getResource("/event-processors.xml");
		log.info("Adding event-processors from {}", url);

		try {
			finder.deployCustomEventProcessors(url.openStream(), pipeline);

			for (EventProcessor<AuditEvent> p : pipeline.getProcessors()) {
				log.info("Priority: {}, processor: {}",
						pipeline.getPriority(p), p);
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("Not yet implemented");
		}
	}

}
