package org.jwall.audit.script;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.ModSecurity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptListTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger(ScriptListTest.class);

	@Test
	public void testListAll() throws Exception {

		URL rubyScript = ScriptListTest.class.getResource("/list-all.rb");

		StringWriter exp = new StringWriter();
		PrintWriter p = new PrintWriter(exp);

		for (AuditEvent evt : database.getRealList()) {
			p.write(evt.get(ModSecurity.TX_ID) + "\n");
		}

		for (AuditEvent evt : database.getRealList()) {
			if ("500".equals(evt.get(ModSecurity.RESPONSE_STATUS)))
				p.write(evt.get(ModSecurity.TX_ID) + "\n");
		}

		ScriptRunner exec = new ScriptRunner(rubyScript, "ruby");

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream(baos);
		exec.init(out, System.err);
		exec.setAuditEventView(database);
		exec.execute();
		log.debug("Output is:\n{}", new String(baos.toByteArray()));
		Assert.assertEquals(exp.toString(), new String(baos.toByteArray()));
	}
}