package org.jwall.audit.script;

import java.util.ListIterator;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.jwall.audit.script.utils.ListIteratorImpl;
import org.jwall.web.audit.AuditEvent;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.ModSecurity;
import org.jwall.web.audit.filter.FilterCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ListIteratorTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger(ListIteratorTest.class);
	static EventList<AuditEvent> eventList;

	@BeforeClass
	public static void setup() throws Exception {
		try {
			eventList = new EventList<AuditEvent>(database,
					FilterCompiler.parse(""));
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	@Test
	public void testHasNext() throws Exception {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);
		boolean containsEvents = !database.getRealList().isEmpty();
		boolean hasNext = it.hasNext();
		Assert.assertEquals(containsEvents, hasNext);
	}

	@Test
	public void testHasPrevious() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);
		Assert.assertFalse(it.hasPrevious());
		it.next();
		Assert.assertTrue(it.hasPrevious());
	}

	@Test
	public void testNext() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));
	}

	@Test
	public void testNextIndex() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));

		Assert.assertEquals(1, it.nextIndex());
	}

	@Test
	public void testPrevious() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);

		it.next();
		it.previous();
		evt = it.previous();
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));
	}

	@Test
	public void testPreviousIndex() {
		ListIterator<ScriptEvent> it = new ListIteratorImpl(eventList);

		ScriptEvent evt = it.next();
		Assert.assertNotNull(evt);

		AuditEvent first = database.getRealList().get(0);
		Assert.assertEquals(first.get(ModSecurity.TX_ID),
				evt.get(ModSecurity.TX_ID));

		Assert.assertEquals(0, it.previousIndex());
	}
}