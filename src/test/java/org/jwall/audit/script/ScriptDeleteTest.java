package org.jwall.audit.script;

import java.io.ByteArrayOutputStream;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.audit.script.utils.TimePrintStream;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.jwall.web.audit.filter.FilterCompiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptDeleteTest {

	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger( ScriptDeleteTest.class );


	@Test
	public void testExecCount500FromUrl() throws Exception {

		try {
			URL rubyScript = ScriptDeleteTest.class.getResource( "/delete-response500.rb" );

			Long total = database.count( FilterCompiler.parse( "" ) );
			Long error500 = database.count( FilterCompiler.parse( "RESPONSE_STATUS = 500" ) );

			log.info( "database has {} events in total and {} events with status=500", total, error500 );

			ScriptRunner exec = new ScriptRunner( rubyScript, "ruby" );

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			exec.init( new TimePrintStream( baos), System.err );
			exec.setAuditEventView( database );
			exec.execute();

			log.debug( "Output:\n{}", new String( baos.toByteArray() ) );
			
			Long remain = database.count( FilterCompiler.parse( "" ) );
			log.info( "after script-execution, the database contains a total of {} events", remain );
			//Assert.assertEquals( total - error500, remain.intValue() );
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail( "Error: " + e.getMessage() );
		}
	}
}