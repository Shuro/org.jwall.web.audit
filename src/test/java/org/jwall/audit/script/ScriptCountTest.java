package org.jwall.audit.script;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.net.URL;

import org.junit.Assert;
import org.junit.Test;
import org.jwall.web.audit.AuditEventDatabaseMock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ScriptCountTest {
	
	final static AuditEventDatabaseMock database = new AuditEventDatabaseMock();
	static Logger log = LoggerFactory.getLogger( ScriptCountTest.class );
	
	
	@Test
	public void test() throws Exception {
		
		String ruby = "cnt = $events.count( \"\" )\n"
				+ "puts \"Database contains #{cnt} events.\"\n";
		
		String exp = "Database contains " + database.getRealList().size() + " events.\n";
		
		ScriptRunner exec = new ScriptRunner( ruby, "ruby" );
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream( baos );
		
		exec.init( out, System.err );
		exec.setAuditEventView( database );
		exec.execute();

		String output = new String( baos.toByteArray() );
		Assert.assertEquals( exp, output );
	}
	
	
	
	@Test
	public void testExecFromUrl() throws Exception {
		
		URL rubyScript = ScriptCountTest.class.getResource( "/count.rb" );
		
		String exp = "Database contains " + database.getRealList().size() + " events.\n";
		
		ScriptRunner exec = new ScriptRunner( rubyScript, "ruby" );
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream( baos );
		
		exec.init( out, System.err );
		exec.setAuditEventView( database );
		exec.execute();

		String output = new String( baos.toByteArray() );
		log.debug("output: {}", output );
		log.debug("expect: {}", exp );
		Assert.assertEquals( exp, output );
	}

	
	
	
	
	@Test
	public void testExecCount500FromUrl() throws Exception {
		
		URL rubyScript = ScriptCountTest.class.getResource( "/count-response500.rb" );
		
		String exp = "Database contains 6 events.\n";
		
		ScriptRunner exec = new ScriptRunner( rubyScript, "ruby" );
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		PrintStream out = new PrintStream( baos );
		
		exec.init( out, System.err );
		exec.setAuditEventView( database );
		exec.execute();

		String output = new String( baos.toByteArray() );
		Assert.assertEquals( exp, output );
	}
}