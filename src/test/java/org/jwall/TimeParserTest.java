package org.jwall;


import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.jwall.log.io.TimeParser;

public class TimeParserTest {

	@Before
	public void setUp() throws Exception {
	}

	
	@Test
	public void testParseTime() throws Exception {
		String str = "1 hour";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( TimeParser.HOUR, time );
	}
	
	@Test
	public void testParseHourShortened() throws Exception {
		String str = "2 hours";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( 2*TimeParser.HOUR, time.longValue() );
	}
	
	@Test
	public void testParseMix() throws Exception {
		String str = "2 hours 34 minutes 1sec";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( 2*TimeParser.HOUR + 34 * TimeParser.MINUTE + TimeParser.SECOND, time.longValue() );
	}


	
	@Test
	public void testParseMix2() throws Exception {
		String str = "2hours34minutes 1sec";
		TimeParser p = new TimeParser();
		Long time = p.parse( str );
		Assert.assertEquals( 2*TimeParser.HOUR + 34 * TimeParser.MINUTE + TimeParser.SECOND, time.longValue() );
	}
}
